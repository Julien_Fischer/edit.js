/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A found occurrence of a pattern.
 *
 * @param {string} match  the exact match
 * @param {number} offset  the offset at which this occurrence was found
 */
const Occurrence = function(match, offset) {

    this.getMatch = () => { return match; }

    this.getOffset = () => { return offset; }

    this.getLength = () => { return match.length; }

    this.getEndIndex = () => { return offset + this.getLength(); }

    this.toString = () => {
        return `Offset: ${offset}, length: ${this.getLength()}, match: ${StringUtils.quote(match)}`;
    }

    // This method is intended to be used by debugger and should not be invoked by clients
    this.toLiteral = () => {
        return {offset: offset, match: match};
    }

}
