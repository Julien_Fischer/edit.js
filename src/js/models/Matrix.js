/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A two-dimensional array of elements arranged in rows and columns.
 * By default, instances of this class hold only {@code null} elements,
 * so that {@link #isBlank()} will return true if no element is explicitly
 * added to the matrix.
 *
 * @param {number} colCount  the number of columns to initialize the matrix with
 * @param {number} rowCount  the number of rows to initialize the matrix with
 */
const Matrix = function (colCount = 0, rowCount = 0) {

    let data = [];
    const structureChangeListeners = [];
    const cellValueChangeListeners = [];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.of = (arr) => {
        if (arr == null) {
            throw new Error('Cannot initialize matrix from null array');
        } else if (arr.length === 0) {
            this.reset(0, 0);
        }
        const height = arr.length;
        const width = arr[0].length;
        this.reset(width, height);
        arr.forEach((row, y) =>
            row.forEach((cell, x) => {
                this.set(x, y, cell);
            })
        );
        return this;
    };

    this.insertRow = (y = -1, row = null) => {
        if (row != null && row.length !== this.getWidth()) {
            throw new Error('Can not insert row with length ' + row.length + ' in matrix of width ' + this.getWidth());
        }
        rowCount++;
        const rowIndex = computeRowIndex(y);
        ArrayUtils.insert(data, rowIndex, [])
        for (let colIndex = 0; colIndex < this.getWidth(); colIndex++) {
            const content = (row == null) ? null : row[colIndex];
            insertCell(colIndex, rowIndex, content);
        }
        notifyStructureChangeListeners();
    }

    this.insertColumn = (x = -1, column = null) => {
        if (column != null && column.length !== this.getHeight()) {
            throw new Error('Can not insert column with length ' + column.length + ' in matrix of height ' + this.getHeight());
        }
        colCount++;
        for (let y = 0; y < this.getHeight(); y++) {
            const content = (column == null) ? null : column[y];
            insertCell(x, y, content);
        }
        notifyStructureChangeListeners();
    }

    this.removeRow = (y = -1) => {
        validateIndex(y, Axis.Y);
        if (this.getHeight() === 1) {
            return [];
        }
        const removed = this.getRow(y);
        data.splice(y, 1);
        rowCount--;
        notifyStructureChangeListeners();
        return removed;
    };

    this.removeColumn = (x = -1) => {
        validateIndex(x, Axis.X);
        if (this.getWidth() === 1) {
            return [];
        }
        const removed = this.getColumn(x);
        colCount--;
        removed.forEach((cell, y) => {
            data[y].splice(x, 1);
        });
        notifyStructureChangeListeners();
        return removed;
    };

    this.swapRows = (a, b) => {
        validateIndex(a, Axis.Y);
        validateIndex(b, Axis.Y);
        const r1 = computeRowIndex(a);
        const r2 = computeRowIndex(b);
        const tmp = this.getRow(r1);
        for (let i = 0; i < this.getWidth(); i++) {
            data[r1][i] = data[r2][i];
            data[r2][i] = tmp[i];
        }
        notifyStructureChangeListeners();
    }

    this.swapColumns = (a, b) => {
        validateIndex(a, Axis.X);
        validateIndex(b, Axis.X);
        const c1 = computeColIndex(a);
        const c2 = computeColIndex(b);
        const tmp = this.getColumn(c1);
        for (let i = 0; i < this.getHeight(); i++) {
            data[i][c1] = data[i][c2];
            data[i][c2] = tmp[i];
        }
        notifyStructureChangeListeners();
    }

    this.set = (x, y, value) => {
        validateIndex(x, Axis.X);
        validateIndex(y, Axis.Y);
        const colIndex = computeColIndex(x);
        const rowIndex = computeRowIndex(y);
        const oldValue = this.get(colIndex, rowIndex);
        data[rowIndex][colIndex] = value;
        notifyCellValueChangeListeners(new CellUpdate(colIndex, rowIndex, oldValue, value));
    }

    this.get = (x, y) => {
        validateIndex(x, Axis.X);
        validateIndex(y, Axis.Y);
        const rowIndex = computeRowIndex(y);
        const colIndex = computeColIndex(x);
        return data[rowIndex][colIndex];
    }

    this.getRow = (index) => {
        validateIndex(index, Axis.Y);
        const i = computeRowIndex(index);
        return this.toArray()[i];
    }

    this.getColumn = (index) => {
        validateIndex(index, Axis.X);
        const colIndex = computeColIndex(index);
        return MatrixUtils.getColumn(this.toArray(), colIndex);
    }

    this.getHeight = () => {
        return rowCount;
    }

    this.getWidth = () => {
        return colCount;
    }

    this.getDimension = () => {
        return new Vector(rowCount, colCount);
    }

    this.getLength = (axis) => {
        return (axis === Axis.X) ? this.getWidth() : this.getHeight();
    }

    /**
     * Constructs a copy of the backing matrix.
     * This method returns a safe two-dimensional array which can be mutated
     * without affecting the actual table model.
     *
     * @return a two-dimensional array
     */
    this.toArray = () => {
        return MatrixUtils.clone(data);
    };

    /**
     * Clears the table cells content.
     * This method does not modify the structure of the table.
     *
     * @param valueProvider  an optional function producing a default value to
     *                       be used in lieu of an empty cell
     */
    this.clear = function(valueProvider = () => null) {
        this.forEachCoordinate((x, y) => data[y][x] = valueProvider());
        notifyStructureChangeListeners();
    };

    /**
     * Resets the table to its initial state.
     * This method modifies the structure of the table.
     */
    this.reset = function(w = colCount, h = rowCount) {
        colCount = w;
        rowCount = h;
        data = [];
        empty(w, h);
        notifyStructureChangeListeners();
    };

    /**
     * Determines whether this table is blank.
     *
     * @return true if the table contains only null elements; false otherwise
     */
    this.isBlank = () => {
        for (let row of data) {
            for (let cell of row) {
                if ((cell instanceof Cell && !cell.isEmpty()) || !(cell instanceof Cell) && cell != null) {
                    return false;
                }
            }
        }
        return true;
    };

    this.contains = (coordinate) => {
        return this.getRectangle().containsCoordinate(coordinate);
    }

    /**
     * Returns a new matrix containing the elements captured by the specified
     * rectangle area.
     *
     * @param rectangle  the rectangle to use for clipping this matrix
     * @return a new Matrix containing the elements captured by the specified
     * rectangle area.
     */
    this.clip = (rectangle) => {
        const bounds = this.getRectangle();
        if (!bounds.containsRectangle(rectangle)) {
            const dim = bounds.getDimension().toString();
            throw new Error('Can not clip matrix of dimension ' + dim + ' using rectangle ' + rectangle.toString());
        }
        const arr = [];
        const end = rectangle.getEndCoordinate();
        for (let y = rectangle.getY(); y < end.getY(); y++) {
            const row = [];
            for (let x = rectangle.getX(); x < end.getX(); x++) {
                row.push(this.get(x, y));
            }
            arr.push(row);
        }
        return new Matrix().of(arr);
    }

    this.getRectangle = () => {
        return new Rectangle(0, 0, this.getWidth(), this.getHeight());
    }

    this.getColumns = () => {
        return ArrayUtils.range(0, this.getWidth())
            .map(x => this.getColumn(x));
    }

    /**
     * Applies the specified function to every cell in this matrix.
     *
     * @param fn  a function with the following signature: (cell: E, x: int, y: int) => void
     */
    this.forEachCell = (fn) => {
        this.forEachCoordinate(
            (x, y) => fn(this.get(x, y), x, y)
        );
    }

    /**
     * Applies the specified function to every column in this matrix.
     *
     * @param fn  a function with the following signature: (column: E[], colIndex: int) => void
     */
    this.forEachColumn = (fn) => {
        this.getColumns().forEach((col, x) => fn(col, x));
    }

    /**
     * Applies the specified function to every row in this matrix.
     *
     * @param fn  a function with the following signature: (row: E[], rowIndex: int) => void
     */
    this.forEachRow = (fn) => {
        data.forEach((row, y) => fn(row, y));
    }

    /**
     * Applies the specified function to every coordinate in this matrix.
     *
     * @param fn  a function with the following signature: (x: int, y: int) => void
     */
    this.forEachCoordinate = (fn) => {
        for (let y = 0; y < data.length; y++) {
            for (let x = 0; x < data[y].length; x++) {
                fn(x, y);
            }
        }
    }

    /**
     * Returns a new matrix populated with the results of calling the specified function
     * on every element in the calling matrix.
     *
     * @param {function(Cell,number,number): any} fn  the function that is called for
     *                                                every element in this matrix
     * @return {Matrix}
     */
    this.map = (fn) => {
        const clone = this.clone();
        clone.forEachCell((cell, x, y) => clone.set(x, y, fn(cell, x, y)));
        return clone;
    }

    /**
     * Assigns the specified value to every cell that pass the test implemented by
     * this predicate.
     * If no predicate is specified, fills every cell of the matrix.
     *
     * @param {any | function(any, number, number): any} value  the value to fill this matrix with
     * @param {function(any): boolean} [predicate]  the predicate to use for determining which cells are to be
     * filled
     */
    this.fill = (value, predicate) => {
        this.forEachCoordinate((x, y) => {
            const cell = this.get(x, y);
            if (predicate == null || predicate(cell)) {
                const val = isFunction(value) ? value(cell, x, y) : value;
                this.set(x, y, val)
            }
        });
    }

    this.fillColumn = (index, value) => {
        validateIndex(index, Axis.X);
        for (let y = 0; y < this.getHeight(); y++) {
            this.set(computeColIndex(index), y, value);
        }
    }

    this.fillRow = (index, value) => {
        validateIndex(index, Axis.Y);
        for (let x = 0; x < this.getWidth(); x++) {
            this.set(x, computeColIndex(index), value);
        }
    };

    this.transpose = () => {
        const model = this.toArray();
        const rotated = MatrixUtils.rotate(model);
        const width = this.getHeight();
        const height = this.getWidth();
        this.reset(width, height);
        this.of(rotated);
        notifyStructureChangeListeners();
    };

    this.clone = () => {
        return new Matrix().of(this.toArray());
    }

    this.equals = (matrix) => {
        if (matrix == null) {
            return false;
        }
        let equal = true;
        this.forEachCell((cell, x, y) => {
           if (cell !== matrix.get(x, y)) {
               equal = false;
           }
        });
        return equal;
    }

    this.addStructureChangeListener = (listener) => {
        structureChangeListeners.push(listener);
    };

    this.addCellValueChangeListener = (listener) => {
        cellValueChangeListeners.push(listener);
    };

    this.toString = () => {
        return '[Matrix]\n' + data.map(row => row.join(', ')).join('\n');
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = (cols, rows) => {
        empty(cols, rows);
        notifyStructureChangeListeners();
    };

    const notifyStructureChangeListeners = () => {
        structureChangeListeners.forEach(l => l.onTableStructureChange(this.toArray()));
    }

    const notifyCellValueChangeListeners = (change) => {
        cellValueChangeListeners.forEach(l => l.onCellValueChange(change));
    }

    const empty = (cols, rows) => {
        for (let y = 0; y < rows; y++) {
            data.push([]);
            for (let x = 0; x < cols; x++) {
                insertCell(x, y);
            }
        }
    };

    const computeRowIndex = (index) => {
        return index < 0 ? this.getHeight() + index : index;
    }

    const computeColIndex = (index) => {
        return index < 0 ? this.getWidth() + index : index;
    }

    const insertCell = (x, y, content = null) => {
        const colIndex = x;
        const rowIndex = computeRowIndex(y);
        ArrayUtils.insert(data[rowIndex], colIndex, content);
    };

    const validateIndex = (index, axis) => {
        const size = this.getLength(axis);
        if (Math.abs(index) > size - 1) {
            throw new Error('Out of bounds: ' + index + ' is out of bounds [0, ' + size + '] for axis: ' + axis);
        }
    }

    init(colCount, rowCount);

};
