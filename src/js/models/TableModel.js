/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Instances of this class hold the state of a {@link EditorTable} object.
 *
 * @param {Alignment[]} alignments  an array of {@link Alignment} constants
 * @param {string[][]} data  a two-dimensional array
 */
const TableModel = function(alignments, data) {

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getAlignments = () => {
        return alignments;
    }

    this.getAlignment = (index) => {
        return alignments[index];
    }

    this.getData = () => {
        return data;
    }

    this.getDimension = () => {
        return MatrixUtils.getDimension(data);
    }

    this.isEmpty = () => {
        return MatrixUtils.isBlank(data);
    }

    this.toString = () => {
        return '[TableModel] Alignments: ' + StringUtils.prettyPrint(alignments) + '\nData: ' + data;
    }

}
