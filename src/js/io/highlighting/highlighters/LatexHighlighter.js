/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for LaTeX tables.
 *
 * @see Format.LATEX
 */
const LatexHighlighter = function() {

    Highlighter.call(this);

    const EOL = '\\\\';
    const CONSTANT_OPEN  = '{';
    const CONSTANT_CLOSE = '}';

    const VALUE_SEPARATOR = StringUtils.escapeHTMLChars('&');
    const DELIMITERS = [CONSTANT_OPEN, CONSTANT_CLOSE, '[', ']'];
    const KEYWORDS = ['begin', 'centering', 'caption', 'hline', 'end', 'label'];
    const CONSTANTS = ['table', 'tabular'];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        const tokens = arr.map(line => highlightLine(line));
        const dataTokens = parseData(arr);
        dataTokens.forEach(line => tokens[line.index].push(...line.tokens));
        return tokens;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        if (startIndex < 0) {
            return [];
        }
        for (let i = startIndex; i < endIndex; i++) {
            const keyword = getKeyword(subject, i);
            if (keyword !== null) {
                const match = '\\' + keyword.getMatch();
                const offset = keyword.getOffset() - 1;
                this.save(tokens, match, offset, TokenType.KEYWORD);
                if (keyword.getLength() > 1) {
                    i += keyword.getLength();
                }
                continue;
            }
            const delimiter = getDelimiter(subject, i);
            if (delimiter !== null) {
                const match = delimiter.getMatch();
                const offset = delimiter.getOffset();
                this.save(tokens, match, offset, TokenType.DELIMITER);
                if (delimiter.getLength() > 1) {
                    i += delimiter.getLength() - 1;
                }
                if (match === CONSTANT_OPEN) {
                    for (let x = i; x < subject.length; x++) {
                        const char = subject.charAt(x);
                        if (char === CONSTANT_CLOSE) {
                            const offset = i + 1;
                            const substr = subject.substring(offset, x);
                            if (ArrayUtils.contains(CONSTANTS, substr)) {
                                this.save(tokens, substr, offset, TokenType.ATTRIBUTE);
                            }
                            i = x - 1;
                            break;
                        }
                    }
                }
            }
        }
        return tokens;
    }

    const parseData = (arr) => {
        const lines = [];
        const startIndex = firstIndexOfData(arr);
        const endIndex = lastIndexOfData(arr);
        for (let i = startIndex; i <= endIndex; i++) {
            const dataTokens = parseDataLine(arr[i]);
            lines.push({index: i, tokens: dataTokens});
        }
        return lines;
    }

    const parseDataLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = endOfLine(subject);
        if (startIndex < 0) {
            return [];
        }
        let valueStartIndex = startIndex;
        for (let i = startIndex; i <= endIndex; i++) {
            if (isSeparator(subject, i)) {
                const dataString = subject.substring(valueStartIndex, i);
                this.save(tokens, dataString, valueStartIndex, TokenType.VALUE);
                this.save(tokens, VALUE_SEPARATOR, i, TokenType.SEPARATOR);
                i += VALUE_SEPARATOR.length;
                valueStartIndex = i;
            }
        }
        const lastDataString = subject.substring(valueStartIndex, endIndex);
        this.save(tokens, lastDataString, valueStartIndex, TokenType.VALUE);
        return tokens;
    }

    const endOfLine = (subject) => {
        return subject.indexOf(EOL);
    }

    const firstIndexOfData = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            const line = arr[i];
            if (line.match(VALUE_SEPARATOR)) {
                return i;
            }
        }
    }

    const lastIndexOfData = (arr) => {
        for (let i = arr.length - 1; i >= 0; i--) {
            const line = arr[i];
            if (line.match(VALUE_SEPARATOR)) {
                return i;
            }
        }
    }

    const isSeparator = (subject, index) => {
        const c = subject.charAt(index);
        if (c !== '&') {
            return null;
        }
        const substr = subject.substr(index, VALUE_SEPARATOR.length);
        return (substr === VALUE_SEPARATOR);
    }

    const getDelimiter = (subject, index) => {
        const c = subject.charAt(index);
        return ArrayUtils.contains(DELIMITERS, c) ? new Occurrence(c, index) : null;
    }

    const getKeyword = (subject, index) => {
        const c = subject.charAt(index);
        if (c !== '\\') {
            return null;
        }
        const occurrence = StringUtils.match(subject, /\w+/, index);
        if (occurrence == null) {
            return null;
        }
        return ArrayUtils.contains(KEYWORDS, occurrence.getMatch()) ? occurrence : null;
    }

}
