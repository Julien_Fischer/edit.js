/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for MediaWiki tables.
 *
 * @param format  a {@link Format} constant
 *
 * @see Format.MEDIAWIKI
 * @see Format.ASCII_DOC
 */
const MediaWikiHighlighter = function(format) {

    Highlighter.call(this);

    const DELIMITERS = ['|', '!'];
    const ROW_DELIMITER = '-';
    const QUOTE = HtmlEntities.QUOTE_DOUBLE.escaped;
    const QUOTE_LENGTH = QUOTE.length;

    const isMediaWiki = (format === Format.MEDIAWIKI);
    const isAsciiDoc = (format === Format.ASCII_DOC);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        if (startIndex < 0) {
            return [];
        }
        for (let i = startIndex; i <= endIndex; i++) {
            const c = subject.charAt(i);
            if (isAsciiDoc && isAsciiDocMetadata(subject)) {
                this.save(tokens, c, i, TokenType.DELIMITER);
                const metadataStartIndex = StringUtils.indexOf(subject, QUOTE, i);
                const metadataEndIndex = StringUtils.indexOf(subject, QUOTE, metadataStartIndex + QUOTE_LENGTH) + QUOTE_LENGTH;
                const metadataString = subject.substring(metadataStartIndex, metadataEndIndex);
                this.save(tokens, metadataString, metadataStartIndex, TokenType.ATTRIBUTE);
                const closingDelimiterIndex = this.getIndexOfNextCharacter(subject, metadataEndIndex, ']');
                this.save(tokens, ']', closingDelimiterIndex, TokenType.DELIMITER);
                break;
            }
            const tableOpening = isTableOpening(subject);
            const tableClosing = isTableClosing(subject);
            if (tableOpening || tableClosing) {
                // table opening or closing delimiter
                const openingTagEndIndex = isMediaWiki ? i + 2 : subject.length;
                const substr = subject.substring(i, openingTagEndIndex);
                this.save(tokens, substr, i, TokenType.TAG);
                if (isMediaWiki && tableOpening) {
                    // table attributes
                    const attributeStartIndex = this.getIndexOfNextCharacter(subject, i, '&', endIndex);
                    const openingQuoteEndIndex =  attributeStartIndex + QUOTE_LENGTH;
                    const attributeEndIndex = this.getIndexOfNextCharacter(subject, openingQuoteEndIndex, '&', endIndex);
                    const closingQuoteEndIndex =  attributeEndIndex + QUOTE_LENGTH;
                    const attributeString = subject.substring(attributeStartIndex, closingQuoteEndIndex);
                    this.save(tokens, attributeString, attributeStartIndex, TokenType.ATTRIBUTE);
                }
                break;
            } else if (isDelimiter(c)) {
                // value delimiter
                const nextIndex = i + 1;
                const nextChar = subject.charAt(nextIndex);
                // row delimiter
                if (nextChar === ROW_DELIMITER) {
                    const substr = subject.substring(i, nextIndex + 1);
                    this.save(tokens, substr, i, TokenType.TAG);
                    i++;
                    break;
                }
                this.save(tokens, c, i, TokenType.DELIMITER);
            } else {
                // value
                const nextIndex = this.getIndexOfNextNonWhitespaceCharacter(subject, i, endIndex);
                const nextChar = subject.charAt(nextIndex);
                if (nextChar === '') {
                    continue;
                }
                const valueEndIndex = this.getIndexOfNextCharacter(subject, nextIndex, isDelimiter, endIndex, endIndex);
                const isLast = (valueEndIndex === endIndex);
                const r = isLast ? valueEndIndex : valueEndIndex - 1;
                const valueString = subject.substring(nextIndex, r);
                if (!StringUtils.isBlank(valueString)) {
                    this.save(tokens, valueString, nextIndex, TokenType.VALUE);
                    i = isLast ? valueEndIndex : valueEndIndex - 1;
                }
            }
        }

        return tokens;
    }

    const isDelimiter = (char) => {
        return ArrayUtils.contains(DELIMITERS, char);
    }

    const isTableOpening = (subject) => {
        return isMediaWiki ? subject.match(/{\|/) : subject.match(/\|===/);
    }

    const isTableClosing = (subject) => {
        return isMediaWiki ? subject.match(/\|}/) : subject.match(/\|===/);
    }

    const isAsciiDocMetadata = (subject) => {
        return subject.match(/\[cols=[^\]]+]/);
    }

}
