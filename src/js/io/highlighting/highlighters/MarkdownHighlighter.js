/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for markdown tables.
 *
 * @param {Format} format  the format of the input to highlight
 *
 * @see Format.CUCUMBER_MARKDOWN
 */
const MarkdownHighlighter = function(format) {

    Highlighter.call(this);

    const isGitHub = (format === Format.GITHUB_MARKDOWN);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map((line, y) => highlightLine(line, y));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject, y) => {
        const tokens = [];
        let contentStartIndex = -1;
        StringUtils.forEach(subject, (c, i) => {
            if (c === '|') {
                this.save(tokens, c, i, TokenType.DELIMITER);
                if (contentStartIndex > -1) {
                    const type = (isGitHub && y === 1) ? TokenType.ATTRIBUTE : TokenType.VALUE;
                    const substr = subject.substring(contentStartIndex, i);
                    this.save(tokens, substr, contentStartIndex, type);
                }
                contentStartIndex = i + 1;
            }
        });
        return tokens;
    }

}
