/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for SQL scripts.
 *
 * @see Format.SQL
 */
const SqlHighlighter = function() {

    Highlighter.call(this);

    const KEYWORDS = [
        'CREATE',
        'TABLE',
        'VARCHAR',
        'INSERT',
        'INTO',
        'VALUES'
    ];

    const DELIMITERS = [',', ';', '(', ')'];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        const tokens = [];
        const startIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const endIndex = StringUtils.indexOfFirstNonWhitespaceCharacter(subject, true);
        for (let i = startIndex; i <= endIndex; i++) {
            let skip = true;
            let newIndex = i;
            const c = subject.charAt(i);
            const occurrence = StringUtils.match(subject, /\w+/, i);
            if (occurrence != null) {
                // word-like sequence matched: it might be a keyword, delimiter, or an escaped quote
                const word = occurrence.getMatch();
                if (c === '&') {
                    const entity = this.getEscapedHtmlEntity(subject, i);
                    if (this.isEscapedQuoteEntity(entity)) {
                        // escaped quote
                        const openingQuoteIndex = i;
                        let closingQuoteIndex = -1;
                        for (let x = i + 1; x < subject.length; x++) {
                            const char = subject.charAt(x);
                            if (char === '&') {
                                const closingQuoteEntity = this.getEscapedHtmlEntity(subject, x);
                                if (this.isEscapedQuoteEntity(closingQuoteEntity) && !this.isBackSlashed(subject, x)) {
                                    closingQuoteIndex = x;
                                    skip = false;
                                    break;
                                }
                            }
                            // No matching quote was found, skip to next iteration
                        }
                        if (skip) {
                            i = openingQuoteIndex + entity.length;
                            continue;
                        }
                        const closingQuoteEndIndex = closingQuoteIndex + entity.length;
                        const substr = subject.substring(openingQuoteIndex, closingQuoteEndIndex);
                        this.save(tokens, substr, i, TokenType.STRING);
                        newIndex = closingQuoteEndIndex - 1;
                    } else {
                        // other escaped html entities
                        i = i + entity.length - 1;
                        continue;
                    }
                } else {
                    if (isSqlKeyword(word)) {
                        this.save(tokens, word, occurrence.getOffset(), TokenType.KEYWORD);
                        newIndex = occurrence.getEndIndex() - 1;
                    } else if (isUnescapedDelimiter(c)) {
                        this.save(tokens, c, i, TokenType.DELIMITER);
                    } else if (isNumberLiteral(subject, occurrence)) {
                        this.save(tokens, word, i, TokenType.NUMBER);
                        newIndex = i + word.length - 1;
                    }
                }
                i = newIndex;
            } else {
                if (isUnescapedDelimiter(c)) {
                    this.save(tokens, c, i, TokenType.DELIMITER);
                }
            }
        }
        return tokens;
    }

    const isSqlKeyword = (word) => {
        return ArrayUtils.contains(KEYWORDS, word);
    }

    const isUnescapedDelimiter = (char) => {
        return ArrayUtils.contains(DELIMITERS, char);
    }

    const isNumberLiteral = (subject, occurrence) => {
        const prevCharacter = subject.charAt(occurrence.getOffset() - 1);
        return (prevCharacter !== '#' && StringUtils.isNumeric(occurrence.getMatch()));
    }

}
