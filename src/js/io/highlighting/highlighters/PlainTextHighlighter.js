/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * This {@link Highlighter} provides support for Plain Text and Unicode tables.
 *
 * @param {Format} format  the format of the input to highlight
 *
 * @see Format.PLAIN_ASCII
 * @see Format.UNICODE
 * @see Format.UNICODE_BOLD
 */
const PlainTextHighlighter = function(format) {

    Highlighter.call(this);

    const borders = BorderFactory.of(format);
    const leftBorders = [borders.getTopLeft(), borders.getMiddleLeft(), borders.getBottomLeft()];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.parseTokens = (arr) => {
        return arr.map(line => highlightLine(line));
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const highlightLine = (subject) => {
        let prev = -1;
        const tokens = [];
        const horizontalBorderStartIndex = isHorizontalBorder(subject);
        if (horizontalBorderStartIndex > -1) {
            const substr = subject.substring(horizontalBorderStartIndex);
            this.save(tokens, substr, horizontalBorderStartIndex, TokenType.DELIMITER);
        } else {
            StringUtils.forEach(subject, (c, i) => {
                if (isVerticalBorder(c)) {
                    this.save(tokens, c, i, TokenType.DELIMITER);
                    if (prev > -1) {
                        const substr = subject.substring(prev, i - 1)
                        this.save(tokens, substr, prev, TokenType.VALUE);
                    }
                    prev = i + 2;
                }
            });
        }
        return tokens;
    }

    const isVerticalBorder = (char) => {
        return Values.either(char, borders.getLeft(), borders.getRight());
    }

    const isHorizontalBorder = (subject) => {
        const index = StringUtils.indexOfFirstNonWhitespaceCharacter(subject);
        const firstChar = subject.charAt(index);
        return ArrayUtils.contains(leftBorders, firstChar) ? index : -1;
    }

}
