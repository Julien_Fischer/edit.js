/*
* Copyright (c) 2021. Julien Fischer
*
*  Licensed under the Apache License, Version 2.0 (the "License");
*  you may not use this file except in compliance with the License.
*  You may obtain a copy of the License at
*
*         http://www.apache.org/licenses/LICENSE-2.0
*
*  Unless required by applicable law or agreed to in writing, software
*  distributed under the License is distributed on an "AS IS" BASIS,
*  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
*  See the License for the specific language governing permissions and
*  limitations under the License.
*/

/**
 * The base class for highlighting an input string of any known {@link Format}.
 * The only method that a subclass must implement is {@code parseTokens}.
 * The main method, {@code highlight}, can be overridden to provide a different
 * highlighting mechanism.
 */
const Highlighter = function() {

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Returns an highlighted version of this array of strings.
     *
     * @param {string[]} arr  the array of strings to highlight
     * @return {string[]}
     */
    this.highlight = (arr) => {
        return this.parseTokens(arr)
            .map((tokenList, i) => this.colorize(arr[i], tokenList));
    }

    /**
     * Highlights every token in this subject string from the specified list of tokens.
     *
     * @param {string} subject  the subject string to process
     * @param {Token[]} tokens  the tokens to highlight
     * @return {string} a syntax-highlighted version of this string
     */
    this.colorize = (subject, tokens) => {
        let template = subject;
        tokens
            .sort((a, b) => b.getOffset() - a.getOffset())
            .forEach(occurrence => {
                const before = template.substring(0, occurrence.getOffset());
                const colorized = occurrence.toHtmlString();
                const after = template.substring(occurrence.getEndIndex());
                template = before + colorized + after;
            });
        return template;
    };

    //////////////////////////////////////////////////////////////////
    // Protected
    //////////////////////////////////////////////////////////////////

    /**
     * Parses the specified array of strings as a 2d array of {@link Token tokens}.
     *
     * This virtual method must be implemented by subtypes.
     *
     * @param {string[]} arr  the array of strings to parse
     * @return {Token[][]}
     */
    this.parseTokens = (arr) => { throw new Error('Not implemented yet'); }

    /**
     * Returns the escaped html entity starting at the specified index in this
     * subject string if any is found; null otherwise.
     *
     * @param {string} subject  the subject string to process
     * @param {number} index  the index of the first character of this escaped
     * html entity
     * @return {null|string}
     */
    this.getEscapedHtmlEntity = (subject, index) => {
        let endIndex = -1;
        for (let i = index; i < index + 20; i++) {
            const char = subject.charAt(i);
            if (char === ';') {
                endIndex = i;
                break;
            }
        }
        return (endIndex < 0) ? null : subject.substring(index, endIndex + 1);
    };

    /**
     * Returns true if this html entity is an opening or closing chevron.
     *
     * @param {string} entity  the escaped html entity to process
     * @return {boolean}
     */
    this.isEscapedHtmlDelimiterEntity = (entity) => {
        return Values.either(entity, HtmlEntities.GREATER_THAN.escaped, HtmlEntities.LESS_THAN.escaped);
    };

    /**
     * Returns true if this html entity is single or double quote.
     *
     * @param {string} entity  the escaped html entity to process
     * @return {boolean}
     */
    this.isEscapedQuoteEntity = (entity) => {
        return Values.either(entity, HtmlEntities.QUOTE_DOUBLE.escaped, HtmlEntities.QUOTE_SINGLE.escaped);
    };

    /**
     * Returns true if the character at index i in this subject string is escaped with
     * a backslash character; false otherwise.
     *
     * @param {string} subject  the subject string to process
     * @param {number} i  the index of the character to look up
     * @return {boolean}
     */
    this.isBackSlashed = (subject, i) => {
        return (i > 0 && subject.charAt(i - 1) === '\\');
    };

    /**
     * Returns the index of the first non-whitespace character starting from the
     * specified offset if any is found; -1 or default value otherwise.
     *
     * @param {string} subject  the subject string to process
     * @param {number} offset  the start offset
     * @param {number=} [endIndex]  (optional) the index of the last character to process
     * @param {number=} [defaultValue]  (optional) the value to return if no whitespace character
     * is found after the specified offset. By default, -1.
     * @return {number}
     */
    this.getIndexOfNextNonWhitespaceCharacter = (subject, offset, endIndex, defaultValue) => {
        return this.getIndexOfNextCharacter(subject, offset, c => c !== ' ', endIndex, defaultValue);
    }

    /**
     * Returns the index of the first occurrence of the specified character, or
     * predicate matching a character, starting from the specified offset if any
     * is found; -1 or default value otherwise.
     *
     * @param {string} subject  the subject string to process
     * @param {number} offset  the start offset
     * @param {string|function(string):boolean} arg  a character literal or a predicate
     * @param {number=} [endIndex]  (optional) the index of the last character to process
     * @param {number=} [defaultValue]  (optional) the value to return if no whitespace character
     * is found after the specified offset. By default, -1.
     * @return {number}
     */
    this.getIndexOfNextCharacter = (subject, offset, arg, endIndex = subject.length, defaultValue = -1) => {
        const predicate = (isFunction(arg) ? arg : c => c === arg);
        for (let x = offset; x < endIndex; x++) {
            const char = subject.charAt(x);
            if (predicate(char)) {
                return x;
            }
        }
        return defaultValue;
    }

    this.save = (tokens, substr, offset, type) => {
        const token = new Token(substr, offset, type);
        tokens.push(token);
        return token;
    };

};
