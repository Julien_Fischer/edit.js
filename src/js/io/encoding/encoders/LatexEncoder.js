/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for LaTeX tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.LATEX
 */
const LatexEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const CAPTION_PLACEHOLDER = 'Table Caption';
    const LABEL_PLACEHOLDER = 'Table Label';
    const VALUE_SEPARATOR = HtmlEntities.AMPERSAND.unescaped;
    const SPACE = ' ';
    const INDENT_SIZE = 4;
    const INDENT = StringUtils.repeat(SPACE, INDENT_SIZE);
    const INDENT2 = StringUtils.repeat(INDENT, 2);

    const horizontalPolicy = metadata.getRowBorderPolicy();
    const verticalPolicy = metadata.getColumnBorderPolicy();

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const data = MatrixUtils.nullSafe(tableModel.getData(), metadata.getEmptyCellPlaceholder());
        const longestElements = MatrixUtils.getColumnWidths(data);
        const verticalBorders = generateVerticalBorders(data);
        const result = [];
        result.push(`\\begin{table}[!ht]`);
        result.push(`${INDENT}\\centering`);
        result.push(`${INDENT}\\caption{${CAPTION_PLACEHOLDER}}`);
        result.push(`${INDENT}\\begin{tabular}{${verticalBorders}}`);
        if (!isBorderless()) {
            result.push(`${INDENT}\\hline`);
        }
        data.forEach((row, y) => {
            const horizontalBorder = generateHorizontalBorder(y, data.length);
            let rowStr = INDENT2;
            const formattedRow = row.map((cell, x) => format(cell, x, longestElements[x]));
            rowStr += formattedRow.join(` ${VALUE_SEPARATOR} `);
            rowStr += ` \\\\${horizontalBorder}`;
            result.push(rowStr);
        });
        result.push(`${INDENT}\\end{tabular}`);
        result.push(`${INDENT}\\label{${LABEL_PLACEHOLDER}}`);
        result.push(`\\end{table}`);
        return result;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const format = (cell, x, longest) => {
        if (isMinify()) {
            return cell;
        }
        const diff = longest - cell.length;
        const padding = StringUtils.repeat(SPACE, diff);
        return cell + padding;
    }

    const generateHorizontalBorder = (y, height) => {
        const border = ' \\hline';
        return shouldPrintBorder(horizontalPolicy, y, height - 1) ? border : '';
    }

    const generateVerticalBorders = (data) => {
        const width = data[0].length;
        let tmp = isBorderless() ? '' : '|';
        ArrayUtils.range(0, width)
            .forEach(x => {
                tmp += 'l';
                if (shouldPrintBorder(verticalPolicy, x, width - 1)) {
                    tmp += '|';
                }
            });
        return tmp;
    }

    const shouldPrintBorder = (policy, i, lastIndex) => {
        if (isBorderless()) {
            return false;
        }
        return (
            (i === 0 && policy === BorderPolicy.FIRST) ||
            (policy === BorderPolicy.EVERY) ||
            (i === lastIndex)
        );
    }

    const isBorderless = () => {
        return (verticalPolicy === BorderPolicy.NONE || horizontalPolicy === BorderPolicy.NONE);
    }

    const isMinify = () => {
        return (metadata.getOutputPolicy() === OutputPolicy.MINIFY);
    }

};
