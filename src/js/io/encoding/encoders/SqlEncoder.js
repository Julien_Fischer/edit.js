/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for SQL scripts.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.SQL
 */
const SqlEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const TABLE_NAME = 'table_name';
    const QUOTE = HtmlEntities.QUOTE_SINGLE.unescaped;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = function(tableModel) {
        const data = MatrixUtils.nullSafe(tableModel.getData());
        const columnNames = data.splice(0, 1);
        return generateSqlScript(columnNames, data);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const generateSqlScript = (columnNames, rows) => {
        const indentation = StringUtils.repeat(' ', 4);
        const columnDefinitions = MatrixUtils.rotate(columnNames);
        columnDefinitions.forEach(e => e.push('VARCHAR(255)'));
        return [
            ... generateCreateStatement(columnDefinitions, indentation),
            '',
            ... generateInsertStatement(columnNames, rows, indentation)
        ];
    };

    const generateCreateStatement = (columnDefinitions, indentation) => {
        const result = [`CREATE TABLE ${TABLE_NAME} (`];
        const longestPerColumn = MatrixUtils.getLongestElementPerColumn(columnDefinitions);
        columnDefinitions.forEach((row, y) => {
            let str = indentation;
            row.forEach((elem, x) => {
                const longest = longestPerColumn[x];
                const diff = longest.length - elem.length;
                const spaces = StringUtils.repeat(' ', diff);
                str += escape(elem, false) + spaces;
                if (x < row.length - 1) {
                    str += ' ';
                }
            });
            str += (y < columnDefinitions.length - 1) ? ',' : ';';
            result.push(str);
        });
        result.push(');');
        return result;
    };

    const generateInsertStatement = (columnNames, rows, indentation) => {
        const result = [
            'INSERT INTO ' + TABLE_NAME,
            indentation + '(' + columnNames[0].join(', ') + ')',
            'VALUES'
        ];
        rows.forEach((row, y) => {
            const elements = row.map(cell => QUOTE + escape(cell, true) + QUOTE);
            const line = indentation + '(' + elements.join(', ') + ')';
            const lastCharacter = (y < rows.length - 1) ? ',' : ';'
            result.push(line + lastCharacter);
        });
        return result;
    };

    const escape = (subject, escapeQuote) => {
        const escaped = escapeQuote ? subject.replace(/(\\?['"])/g, match => {
            return (match.charAt(0) === '\\') ? match : '\\' + match;
        }) : subject;
        return escaped;
    }

};
