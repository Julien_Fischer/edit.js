/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for HTML tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.HTML
 */
const HtmlEncoder = function(metadata) {

    Encoder.call(this, metadata);

    let result = [];
    let modifier = 2;
    // Cache
    const prettify = (metadata.getOutputPolicy() === OutputPolicy.PRETTIFY);
    const headerOrientation = metadata.getHeaderOrientation();

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const serialized = (headerOrientation === HeaderOrientation.FIRST_COLUMN) ?
            constructVerticalTable(tableModel, prettify) :
            constructHorizontalTable(tableModel, prettify);
        return prettify ? serialized : [serialized.join('')];
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const constructVerticalTable = (tableModel, indent) => {
        const data = MatrixUtils.nullSafe(tableModel.getData());
        const lv1 = indent ? 1 : 0;
        const lv2 = indent ? 2 : 0;
        add('<table>');
        data.forEach((row, y) => {
            add('<tr>', lv1);
            row.forEach((cell, x) => {
                const header = (x === 0);
                const datum = Decoders.formatHeaderCell(x, y, cell, metadata);
                const tag = header ? 'th' : 'td';
                const cellAlignment = tableModel.getAlignment(x);
                const alignment = header ? getHeaderAlignment(x, y, cellAlignment) : getCellAlignment(x, y, cellAlignment);
                const styles = header ? createHeaderStyle(alignment) : createCellStyle(alignment);
                add('<' + tag + styles + '>' + datum + '</' + tag + '>', lv2);
            });
            add('</tr>', lv1);
        });
        add('</table>');
        return result;
    }

    const constructHorizontalTable = (tableModel, indent) => {
        const data = MatrixUtils.nullSafe(tableModel.getData());
        const lv1 = indent ? 1 : 0;
        const lv2 = indent ? 2 : 0;
        const lv3 = indent ? 3 : 0;

        add('<table>');

        let y = 0;
        if (headerOrientation !== HeaderOrientation.NO_HEADER) {
            const header = data[0];
            add('<thead>', lv1);
            add('<tr>', lv2);
            header.forEach((cell, x) => {
                const datum = Decoders.formatHeaderCell(x, y, cell, metadata);
                const alignment = getHeaderAlignment(x, y, tableModel.getAlignment(x), metadata);
                const styles = createHeaderStyle(alignment);
                add('<th' + styles + '>' + datum + '</th>', lv3);
            });
            add('</tr>', lv2);
            add('</thead>', lv1);
            y = 1;
        }

        add('<tbody>', lv1);
        for (; y < data.length; y++) {
            const row = data[y];
            add('<tr>', lv2);
            row.forEach((cell, x) => {
                const alignment = tableModel.getAlignment(x);
                const styles = createCellStyle(alignment);
                add('<td' + styles + '>' + cell + '</td>', lv3);
            });
            add('</tr>', lv2);
        }
        add('</tbody>', lv1);
        add('</table>');
        return result;
    }
    
    const add = (tag, indentation = 0) => {
        const spaces = StringUtils.repeat(" ", indentation * modifier);
        const indented = spaces + tag;
        result.push(indented);
    }

    const getCellAlignment = (x, y, cellAlignment) => {
        return Decoders.isHeaderCell(x, y, headerOrientation) ? metadata.getHeaderAlignment() || Alignment.LEFT : cellAlignment;
    }

    const getHeaderAlignment = (x, y, cellAlignment) => {
        return Decoders.isHeaderCell(x, y, headerOrientation) ? metadata.getHeaderAlignment() : cellAlignment;
    }

    const createCellStyle = (alignment) => {
        return (alignment === Alignment.LEFT) ? '' : createStyle(alignment);
    }

    const createHeaderStyle = (alignment) => {
        return (alignment === Alignment.CENTER || alignment == null) ? '' : createStyle(alignment);
    }

    const createStyle = (alignment) => {
        return ' style="text-align: ' + alignment.displayName + ';"';
    }

};
