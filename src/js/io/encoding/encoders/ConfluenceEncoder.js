/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Encoder} provides support for Confluence and Cucumber tables.
 *
 * @param {TableMetadata} metadata  the formatting options to use for tuning the
 *                                  serialization process
 * @see Format.CONFLUENCE_MARKDOWN
 * @see Format.CUCUMBER_MARKDOWN
 */
const ConfluenceEncoder = function(metadata) {

    Encoder.call(this, metadata);

    const format = metadata.getFormat();
    const minify = metadata.getOutputPolicy() === OutputPolicy.MINIFY;

    const BORDER_CHARACTER = "|";

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.encode = (tableModel) => {
        const result = [];
        const nullSafeData = MatrixUtils.nullSafe(tableModel.getData());
        const longestElements = MatrixUtils.getLongestElementPerColumn(nullSafeData);

        nullSafeData.forEach((row, y) => {
            const num = doubleHeader(y) ? 2 : 1;
            const border = StringUtils.repeat(BORDER_CHARACTER, num);
            let asciiRow = border;
            row.forEach((cell, x) => {
                let datum = (y === 0) ? StringUtils.formatCase(cell, metadata.getHeaderCase()) : cell;
                if (!minify && isConfluence() && y > 0) {
                    datum = ' ' + datum;
                }
                let diff = computeDiff(longestElements, row, datum, x, y);
                const spaces = StringUtils.repeat(' ', diff);
                let paddingRight = minify ? '' : spaces + '';
                let paddingLeft = minify ? '' : ' ';
                if (!minify && isConfluence()) {
                    paddingRight += ' ';
                }
                asciiRow += paddingLeft + datum + paddingRight + border
            });
            result.push(asciiRow);
        });

        return result;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const computeDiff = (longestElements, row, cell, x, y) => {
        const longest = longestElements[x];
        let diff = longest.length - cell.length;
        if (format === Format.CUCUMBER_MARKDOWN) {
            diff += 1;
        } else if (y > 0) {
            diff += 1;
            if (x === row.length - 1) {
                diff += 1;
            }
        }
        return diff;
    }

    const doubleHeader = (i) => {
        return (i === 0 && isConfluence());
    }

    const isConfluence = () => {
        return (format === Format.CONFLUENCE_MARKDOWN);
    }

};
