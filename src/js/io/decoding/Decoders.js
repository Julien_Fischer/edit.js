/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Decoders = {

    strip: (str) => {
        const split = str.split('\n');
        return Decoders.removeNoise(split);
    },

    removeNoise: (arr) => {
        let tmp = arr;
        tmp = Decoders.removeBlankCells(tmp)
        return Decoders.trimArray(tmp);
    },

    trimArray: (arr) => {
        return arr.map(e => e.trim());
    },

    trimMatrix: (arr2D) => {
        const x1 = indexOfFirstNonEmptyColumn(arr2D);
        const y1 = indexOfFirstNonEmptyRow(arr2D);
        const x2 = indexOfLastNonEmptyColumn(arr2D);
        const y2 = indexOfLastNonEmptyRow(arr2D);
        const width = x2 - x1;
        const height = y2 - y1;
        const rect = new Rectangle(x1, y1, width + 1, height + 1);
        return new Matrix().of(arr2D)
            .clip(rect)
            .toArray();

        function isBlank(arr) {
            return arr.every(e => StringUtils.isBlank(e));
        }
        function indexOfFirstNonEmptyColumn(arr) {
            for (let x = 0; x < arr[0].length; x++) {
                const column = MatrixUtils.getColumn(arr, x);
                if (!isBlank(column)) {
                    return x;
                }
            }
            return 0;
        }
        function indexOfLastNonEmptyColumn(arr) {
            const lastIndex = arr[0].length - 1;
            for (let x = lastIndex; x >= 0; x--) {
                const column = MatrixUtils.getColumn(arr, x);
                if (!isBlank(column)) {
                    return x;
                }
            }
            return lastIndex;
        }
        function indexOfFirstNonEmptyRow(arr) {
            for (let y = 0; y < arr.length; y++) {
                if (!isBlank(arr[y])) {
                    return y;
                }
            }
            return 0;
        }
        function indexOfLastNonEmptyRow(arr) {
            const lastIndex = arr.length - 1;
            for (let y = lastIndex; y >= 0; y--) {
                if (!isBlank(arr[y])) {
                    return y;
                }
            }
            return lastIndex;
        }
    },

    /**
     * Returns a new array where all blank elements matching certain conditions
     * have been removed.
     *
     * @param {string[]} arr  the array to clean up
     * @param {boolean} [clip]  true if only the blank cells before and after the
     *                        first and last non empty cell must be removed
     * @return a new array containing only the elements which were not removed
     * by this function
     */
    removeBlankCells: (arr, clip) => {
        if (!clip) {
            return arr.filter(e => !StringUtils.isBlank(e));
        }
        const a = ArrayUtils.indexOfFirstNonEmptyCell(arr);
        const b = ArrayUtils.indexOfLastNonEmptyCell(arr);
        return arr.slice(a, b + 1);
    },

    removeEmptyCells: (arr) => {
        return arr.filter(e => e !== '');
    },

    extractCellContent: (row, separator, escape = true) => {
        const pattern = escape ? StringUtils.escapeRegexCharacters(separator) : separator;
        const reg = new RegExp(pattern, 'g');
        return row.split(reg);
    },

    /**
     * This method handles the most common case:
     * a table is prettified if each cell of the same column has the same width.
     *
     * @param data  the matrix to process
     * @return a {@link OutputPolicy} constant
     */
    detectOutputPolicy: (data) => {
        const columns = MatrixUtils.rotate(data);
        const lengths = columns.map(column => column.map(cell => cell.length));
        const sameLength = lengths.every(row => row.every(cell => cell === row[0]));
        return sameLength ? OutputPolicy.PRETTIFY : OutputPolicy.MINIFY;
    },

    /**
     * Parses the data from the specified input string.
     *
     * @param input  the string to parse
     * @param separator  the separator to use for parsing data
     * @return a matrix of strings
     */
    parseData: (input, separator) => {
        const stripped = Decoders.strip(input);
        const cells = stripped.map(row => Decoders.extractCellContent(row, separator));
        const data = cells.map(row => Decoders.removeEmptyCells(row));
        const squared = MatrixUtils.square(data);
        return MatrixUtils.nullSafe(squared);
    },

    isHeaderCell: (x, y, orientation) => {
        return (
            y === 0 && orientation === HeaderOrientation.FIRST_ROW ||
            x === 0 && orientation === HeaderOrientation.FIRST_COLUMN
        );
    },

    formatHeaderCell: (x, y, datum, metadata) => {
        Logger.verbose('Parsers', 'x', x, 'y', y, 'cell', datum, 'proceed', Decoders.isHeaderCell(x, y, metadata.getHeaderOrientation()), metadata.toString());
        return Decoders.isHeaderCell(x, y, metadata.getHeaderOrientation()) ? StringUtils.formatCase(datum, metadata.getHeaderCase()) : datum;
    },

    detectHeaderCase: (data, orientation, strict) => {
        if (orientation === HeaderOrientation.NO_HEADER) {
            return null;
        }
        if (strict) {
            const arr = (orientation === HeaderOrientation.FIRST_ROW) ? data[0] : MatrixUtils.getColumn(data, 0);
            const first = StringUtils.getCase(arr[0]);
            return arr
                .map(cell => StringUtils.getCase(cell))
                .every(c => c === first) ? first : null;
        }
        const headers = MatrixUtils.get(data, 0, orientation.toAxis());
        const cases = headers.map(cell => StringUtils.getCase(cell));
        return ArrayUtils.mostFrequent(cases);
    },

    /**
     * Returns the decoder handling the specified {@link Format}.
     *
     * @param {TableMetadata} metadata  a {@link TableMetadata} constant
     * @return {Decoder} a decoder instance
     */
    of: (metadata) => {
        const format = metadata.getFormat();
        switch(format) {
            case Format.PLAIN_ASCII:
            case Format.UNICODE:
            // Fall through
            case Format.UNICODE_BOLD:
                return new PlainTextDecoder(format);
            case Format.GITHUB_MARKDOWN:
                return new GitHubMarkdownDecoder();
            case Format.CONFLUENCE_MARKDOWN:
            // Fall through
            case Format.CUCUMBER_MARKDOWN:
                return new ConfluenceDecoder(format);
            case Format.LATEX:
                return new LatexDecoder(metadata.getEmptyCellPlaceholder());
            case Format.ASCII_DOC:
                return new AsciiDocDecoder();
            case Format.MEDIAWIKI:
                return new MediaWikiDecoder();
            case Format.JSON:
                return new JsonDecoder();
            case Format.HTML:
                return new HtmlDecoder();
            case Format.BBCODE:
                return new BBCodeDecoder();
            case Format.CSV:
                return new CsvDecoder(metadata.getSeparator());
            case Format.SQL:
                return new SqlDecoder();
            case Format.C_SHARP:
            case Format.JAVA:
            case Format.PHP:
            case Format.PYTHON:
                // Fall through
            case Format.JAVASCRIPT:
                return new CTypeDecoder(format);
            default:
                throw new Error('Unsupported format: unable to parse ' + format.displayName);
        }
    }

};
