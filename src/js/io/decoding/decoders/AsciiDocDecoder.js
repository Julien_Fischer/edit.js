/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for AsciiDoc tables.
 *
 * @see Format.ASCII_DOC
 */
const AsciiDocDecoder = function() {

    Decoder.call(this);

    const Symbols = Object.freeze({
        LEFT:   {char: '<', alignment: Alignment.LEFT},
        CENTER: {char: '^', alignment: Alignment.CENTER},
        RIGHT:  {char: '>', alignment: Alignment.RIGHT}
    });

    const Patterns = Object.freeze({
        DIVIDER:            /\|={3,}/,
        COLUMN_SPECIFIERS:  /\[\w+="([^"]*)"]/,
        CELL_SEPARATOR:     /\s*\|\s*/,
        PRETTIFIED_CELL:    /\| *(.*)/
    });

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        let preparsed = Decoders.strip(input);
        const outputPolicy = detectOutputPolicy(preparsed);
        const data = extractData(preparsed, outputPolicy);
        const alignments = detectAlignments(preparsed, MatrixUtils.getDimension(data));
        const headerOrientation = detectHeaderOrientation(input);
        const model = new TableModel(alignments, data);
        const metadata = new TableMetadata()
            .setHeaderOrientation(headerOrientation)
            .setHeaderCase(detectHeaderCase(data, headerOrientation))
            .setOutputPolicy(outputPolicy);
        return new ParserResult(model, metadata);
    }

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        return input.match(Patterns.DIVIDER) ? Format.ASCII_DOC : null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const translate = (symbol) => {
        for (const s in Symbols) {
            if (symbol.match(StringUtils.escapeRegexCharacters(Symbols[s].char))) {
                return Symbols[s].alignment;
            }
        }
        return Alignment.LEFT;
    }

    const detectAlignments = (preparsed, dimension) => {
        const index = ArrayUtils.indexOfMatch(preparsed, Patterns.COLUMN_SPECIFIERS);
        if (index < 0) {
            const width = dimension.getX();
            return ArrayUtils.repeat(Alignment.LEFT, width);
        }
        const columnSpecifiers = preparsed[index].match(Patterns.COLUMN_SPECIFIERS)[1];
        return columnSpecifiers.split(',')
            .map(symbol => translate(symbol));
    }

    const detectHeaderOrientation = (input) => {
        const preparsed = Decoders.trimArray(input.split(/\n/));
        const indexOfFirstRow = getIndexOfFirstRow(preparsed);
        const nextLine = preparsed[indexOfFirstRow + 1];
        if (StringUtils.isBlank(nextLine)) {
            return HeaderOrientation.FIRST_ROW;
        }
        const columnSpecifiers = preparsed.filter(row => row.match(Patterns.COLUMN_SPECIFIERS));
        if (columnSpecifiers.length > 0) {
            const columnSpecifier = columnSpecifiers[0];
            const reg = /cols=".?h/;
            if (columnSpecifier.match(reg)) {
                return HeaderOrientation.FIRST_COLUMN;
            }
        }
        return HeaderOrientation.NO_HEADER;
    }

    const getIndexOfFirstRow = (preparsed) => {
        for (let i = 0; i < preparsed.length; i++) {
            const count = StringUtils.countOccurrences(preparsed[i], '|', true);
            if (count > 1) {
                return i;
            }
        }
        return -1;
    }

    const detectHeaderCase = (data, headerOrientation) => {
        return Decoders.detectHeaderCase(data, headerOrientation);
    }

    const detectOutputPolicy = (preparsed) => {
        const indexOfFirstRow = getIndexOfFirstRow(preparsed);
        const spliced = [...preparsed];
        spliced.splice(indexOfFirstRow, 1);
        const indexOfSecondInlineRow = getIndexOfFirstRow(spliced);
        return (indexOfSecondInlineRow > -1) ? OutputPolicy.MINIFY : OutputPolicy.PRETTIFY;
    }

    const extractData = (preparsed, outputPolicy) => {
        return (outputPolicy === OutputPolicy.MINIFY) ?
            parseMinifiedTable(preparsed) : parsePrettifiedTable(preparsed);
    }

    const parsePrettifiedTable = (preparsed) => {
        let tmp = preparsed.filter(row => !isDivider(row) && !isColumnSpecifiers(row));
        const firstRow = tmp.splice(0, 1)[0];
        let parsedFirstRow = firstRow.split(Patterns.CELL_SEPARATOR);
        parsedFirstRow.splice(0, 1);
        parsedFirstRow = parsedFirstRow.map(cell => '|' + cell);
        const width = parsedFirstRow.length;
        const rawData = [...parsedFirstRow, ...tmp];
        const data = [];
        let row = [];
        rawData.forEach((cell, i) => {
           if (i > 0 && i % width === 0) {
               data.push(row);
               row = [];
           }
           const datum = cell.match(Patterns.PRETTIFIED_CELL)[1];
           row.push(datum);
        });
        data.push(row);
        return data;
    }

    const parseMinifiedTable = (preparsed) => {
        let tmp = preparsed.filter(row => !isDivider(row) && !isColumnSpecifiers(row));
        tmp = tmp.map(row => row.split(Patterns.CELL_SEPARATOR));
        return Decoders.trimMatrix(tmp);
    }

    const isColumnSpecifiers = (line) => {
        return line.match(Patterns.COLUMN_SPECIFIERS);
    }

    const isDivider = (line) => {
        return line.match(Patterns.DIVIDER);
    }

}
