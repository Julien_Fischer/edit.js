/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for Confluence and Cucumber
 * tables.
 *
 * @param {Format} format  the format of the table to parse
 * @see Format.CONFLUENCE_MARKDOWN
 * @see Format.CUCUMBER_MARKDOWN
 */
const ConfluenceDecoder = function(format) {

    Decoder.call(this);

    const BORDER_CHARACTER = "|";

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const data = Decoders.parseData(input, BORDER_CHARACTER);
        const trimmed = data.map(row => row.map(cell => cell.trim()));
        const tableModel = new TableModel([], trimmed);
        const metadata = detectMetadata(data, format);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const rows = Decoders.strip(input);
        const firstCharacter = rows[0][0];
        if (firstCharacter === '|') {
            if (rows[0][0] === '|' && rows[0][1] === '|') {
                return Format.CONFLUENCE_MARKDOWN;
            } else if (rows[1][3] !== '-' && rows[0][2] !== '=') {
                return Format.CUCUMBER_MARKDOWN;
            }
        }
        return null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectMetadata = (data, format) => {
        const outputPolicy = (format === Format.CUCUMBER_MARKDOWN) ? Decoders.detectOutputPolicy(data) : detectOutputPolicy(data);
        return new TableMetadata()
            .setOutputPolicy(outputPolicy)
            .setHeaderCase(Decoders.detectHeaderCase(data, HeaderOrientation.FIRST_ROW));
    };

    const detectOutputPolicy = (data) => {
        const columns = MatrixUtils.rotate(data);
        for (let i = 0; i < columns.length; i++) {
            const column = columns[i];
            const headerLength = column[0].length;
            if (column.length > 1) {
                for (let j = 1; j < column.length; j++) {
                    const cell = column[j];
                    const step = (i < columns.length - 1) ? 1 : 2;
                    if (cell.length !== headerLength + step) {
                        return OutputPolicy.MINIFY
                    }
                }
            }
        }
        return OutputPolicy.PRETTIFY;
    };

};
