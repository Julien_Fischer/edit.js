/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for JSON arrays.
 *
 * @see Format.JSON
 */
const JsonDecoder = function() {

    Decoder.call(this);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const sanitized = removeNoise(input);
        const parsed = JSON.parse(sanitized);
        const firstElement = parsed[0];
        const jsonFormat = detectFormat(firstElement);
        const stringified = stringify(parsed, jsonFormat);
        const data = (jsonFormat === JsonFormat.ARRAY_2D) ?
            stringified : ArrayUtils.convertArrayOfObjectsTo2dArray(stringified);
        const tableModel = new TableModel([], data);
        const metadata = new TableMetadata()
            .setOutputPolicy(detectOutputPolicy(input))
            .setJsonFormat(jsonFormat);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const rows = Decoders.strip(input);
        const reg = /(?:\[[\s\S]+])|(?:{[\s\S]+})/;
        const str = rows.join('');
        const match = str.match(reg);
        if (match == null) {
            return  null;
        }
        try {
            JSON.parse(match[0]);
            return Format.JSON;
        } catch (err) {
            return null;
        }
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const stringify = (iterable, jsonFormat) => {
        return (jsonFormat === JsonFormat.ARRAY_2D) ?
            stringifyArray(iterable) : iterable.map(row => stringifyObject(row));
    }

    const stringifyArray = (arr) => {
        return arr.map(row => row.map(cell => stringifyValue(cell)));
    }

    const stringifyObject = (obj) => {
        return ObjectUtils.map(obj, (key, value) => stringifyValue(value))
    }

    const stringifyValue = (value) => {
        return isNaN(value) ? value : '' + value;
    }

    const removeNoise = (str) => {
        let sanitized = removeNoiseBefore(str);
        return removeNoiseAfter(sanitized);
    }

    const removeNoiseBefore = (str) => {
        return str.replace(/[^\[]*/, '');
    };

    const removeNoiseAfter = (str) => {
        const lastIndex = str.lastIndexOf(']');
        return str.substring(0, lastIndex + 1);
    };

    const detectOutputPolicy = (input) => {
        const stripped = Decoders.strip(input);
        return (stripped.length > 1) ? OutputPolicy.PRETTIFY : OutputPolicy.MINIFY;
    };

    const detectFormat = (element) => {
        return (element.constructor === Array) ?
            JsonFormat.ARRAY_2D :
            JsonFormat.ASSOCIATIVE_2D
    };
    
};
