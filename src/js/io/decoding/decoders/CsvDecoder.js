/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for CSV.
 *
 * @param {string} separator  the separator to use for parsing the CSV
 * @see Format.CSV
 */
const CsvDecoder = function(separator) {

    Decoder.call(this);

    const COMMON_DELIMITERS = [',', ';', '\t', '|'];

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const stripped = Decoders.strip(input);
        const data = stripped.map(row => row.split(separator));
        const tableModel = new TableModel([], data);
        const metadata = new TableMetadata()
            .setSeparator(separator);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        return null;
    };

};
