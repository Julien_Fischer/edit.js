/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for GitHub markdown tables
 *
 * @see Format.GITHUB_MARKDOWN
 */
const GitHubMarkdownDecoder = function() {

    Decoder.call(this);

    const BORDER_CHARACTER = "|";
    const ALIGNMENT_CHARACTER = ':';

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const data = Decoders.parseData(input, BORDER_CHARACTER);
        const trimmed = data.map(row => row.map(cell => cell.trim()));
        const rawAlignments = trimmed.splice(1, 1)[0];
        const alignments = rawAlignments.map(e => this.computeAlignment(e));
        const tableModel = new TableModel(alignments, trimmed);
        const metadata = detectMetadata(data);
        return new ParserResult(tableModel, metadata);
    };

    /**
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        const rows = Decoders.strip(input);
        const firstCharacter = rows[0][0];
        return (
            firstCharacter === '|' &&
            rows[0][1] !== '|' &&
            rows[1][3] === '-'
        ) ? Format.GITHUB_MARKDOWN : null;
    };

    this.computeAlignment = (str) => {
        const left = str.charAt(0) === ALIGNMENT_CHARACTER;
        const right = str.charAt(str.length - 1) === ALIGNMENT_CHARACTER;
        if (left && right) {
            return Alignment.CENTER;
        } else if (right && !left) {
            return Alignment.RIGHT
        }
        return Alignment.LEFT;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const detectMetadata = (data) => {
        const outputPolicy = Decoders.detectOutputPolicy(data);
        return new TableMetadata()
            .setOutputPolicy(outputPolicy);
    };

}
