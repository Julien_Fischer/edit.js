/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Decoder} provides support for C-type languages data structures.
 *
 * @param format  a {@link Format} constant
 * @see Format.JAVA
 * @see Format.C_SHARP
 * @see Format.PHP
 * @see Format.JAVASCRIPT
 * @see Format.PYTHON
 */
const CTypeDecoder = function(format) {

    Decoder.call(this);

    const ARRAY_DATA_PATTERN           = /{(.*[\s\S]*)}/;
    const LIST_DATA_PATTERN            = /\((.*[\s\S]*)\)/;
    const PHP_ARRAY_DATA_PATTERN       = LIST_DATA_PATTERN;
    const JS_ARRAY_DATA_PATTERN        = /\[(.*[\s\S]*)]/;
    const JS_ASSOCIATIVE_DATA_PATTERN  = /{\s*([\s\S]*)}/;

    const languageDescriptor = Languages.getDescriptor(format);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * @inheritDoc
     * @override
     */
    this.decode = (input) => {
        const metadata = new TableMetadata()
            .setOutputPolicy(detectOutputPolicy(input));
        const type = getType(input);
        let data;
        switch (format) {
            case Format.JAVA:
                data = (type === JavaFormat.ARRAY_2D) ? parse2dArray(input) : parse2dList(input);
                metadata.setJavaFormat(type);
                break;
            case Format.C_SHARP:
                data = (type === CSharpFormat.ARRAY_2D) ? parse2dArray(input) : parseJaggedArray(input);
                metadata.setCSharpFormat(type);
                break;
            case Format.PHP:
                data = (type === PhpFormat.ARRAY_2D) ? parsePhp2dArray(input) : parsePhpAssociativeArray(input);
                metadata.setPhpFormat(type);
                break;
            case Format.PYTHON:
                data = (type === PythonFormat.ARRAY_2D) ? parseJs2dArray(input) : parsePythonAssociativeArray(input);
                metadata.setPythonFormat(type);
                break;
            case Format.JAVASCRIPT:
                data = (type === JsonFormat.ARRAY_2D) ? parseJs2dArray(input) : parseJsAssociativeArray(input);
                metadata.setJsonFormat(type);
                break;
            default:
                throw new Error('Unsupported format: ' + format);
        }
        const model = new TableModel([], data);
        return new ParserResult(model, metadata);
    };

    /**
     * @implNote JSON, JavaScript and Python syntax for arrays of strings are so similar
     * that it may be impossible to distinguish between them in most cases.
     *
     * @inheritDoc
     * @override
     */
    this.supports = (input) => {
        for (let descriptor of Languages.getDescriptors()) {
            if (matches(input, descriptor.detection)) {
                return descriptor.language;
            }
        }
        return null;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const parse2dList = (input) => {
        let preparsed = preparse(input, LIST_DATA_PATTERN)
            .map(row => row.match(/List\.of\(([^)]+)\)/)[1]);
        return postparse(preparsed);
    }

    const parse2dArray = (input) => {
        let preparsed = preparse(input, ARRAY_DATA_PATTERN)
            .map(row => row.substring(1, row.length - 1));
        return postparse(preparsed);
    }

    const parseJaggedArray = (input) => {
        let preparsed = preparse(input, ARRAY_DATA_PATTERN)
            .map(row => row.match(/new +\w+ *\[] *{([^)]+)}/)[1]);
        return postparse(preparsed);
    }

    const preparsePhpArray = (input) => {
        return preparse(input, PHP_ARRAY_DATA_PATTERN)
            .map(row => row.match(/array\s*\((.*[\s\S]*)\)/)[1]);
    }

    const parsePhp2dArray = (input) => {
        let preparsed = preparsePhpArray(input);
        return postparse(preparsed);
    }

    const parseJs2dArray = (input) => {
        let preparsed = preparse(input, JS_ARRAY_DATA_PATTERN)
            .map(row => row.match(/\[([^\]]+)]/)[1]);
        return postparse(preparsed);
    }

    const parseJsonLikeAssociativeArray = (input, z) => {
        let preparsed = input.match(JS_ARRAY_DATA_PATTERN)[1]
            .trim()
            .split(/(?<=})\s*,/)
            .map(row => row.match(JS_ASSOCIATIVE_DATA_PATTERN)[1]);
        return postParseAssociative(preparsed, z);
    }

    const parsePythonAssociativeArray = (input) => {
        return parseJsonLikeAssociativeArray(input, PythonFormat.ASSOCIATIVE_2D);
    }

    const parseJsAssociativeArray = (input) => {
        return parseJsonLikeAssociativeArray(input, JsonFormat.ASSOCIATIVE_2D);
    }

    const parsePhpAssociativeArray = (input) => {
        let preparsed = preparsePhpArray(input);
        preparsed = Decoders.trimArray(preparsed);
        return postParseAssociative(preparsed, PhpFormat.ASSOCIATIVE_2D);
    }

    const postParseAssociative = (preparsed, languageSpecificFormat) => {
        let tmp = preparsed.map(row => row.split(/, ?/))
            .map(row => Decoders.trimArray(row));
        const separator = languageDescriptor.getToken(Languages.VALUE_SEPARATOR, languageSpecificFormat);
        const reg = new RegExp(` *${separator} *`);
        const rawSplit = tmp.map(row =>
            row.map(cell => cell.split(reg))
        );
        const firstRow = rawSplit[0].map(cell => cell[0]);
        const data = rawSplit.map(row =>
            row.map(cellSplit => cellSplit[1])
        );
        return trimOneCharacter([firstRow, ...data]);
    }

    const trimOneCharacter = (arr2d) => {
        return arr2d.map(row =>
            row.map(cell => cell.substring(1, cell.length - 1))
        );
    }

    const postparse = (rawData) => {
        const data = rawData.map(row => row.split(/, ?/));
        return trimOneCharacter(data);
    }

    const preparse = (input, pattern) => {
        const match = input.match(pattern)[1];
        const quote = languageDescriptor.quotes.value;
        const split = match.split(new RegExp(`,(?!\\s*${quote})`));
        return Decoders.trimArray(split);
    }

    const detectOutputPolicy = (input) => {
        const strip = Decoders.strip(input);
        return (strip.length < 2) ? OutputPolicy.MINIFY : OutputPolicy.PRETTIFY;
    }

    const getType = (input) => {
        const descriptor = Languages.getDescriptor(format);
        for (let obj of descriptor.detection) {
            if (input.match(obj.pattern)) {
                return obj.specific;
            }
        }
        Logger.error('Could not detect language-specific format from input string: ' + input)
    }

    const matches = (input, patterns) => {
        return patterns.some(obj => input.match(obj.pattern));
    }

};
