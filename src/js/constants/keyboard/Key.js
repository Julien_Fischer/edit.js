/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Lists the keys supported by this software
 *
 * @see Modifier
 */
const Key = Object.freeze({

    // Arrows
    UP:     'ArrowUp',
    LEFT:   'ArrowLeft',
    DOWN:   'ArrowDown',
    RIGHT:  'ArrowRight',

    // Actions
    ESCAPE: 'Escape',
    DELETE: 'Delete',
    ENTER:  'Enter',

    // Alphanumeric
    D:      'd',
    E:      'e',
    I:      'i',
    Q:      'q',
    V:      'v',
    Y:      'y',
    Z:      'z'

});
