/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Icons = Object.freeze({

    ALIGN_LEFT:         'align-left',
    ALIGN_CENTER:       'align-center',
    ALIGN_RIGHT:        'align-right',
    ANGLE_DOWN:         'angle-down',
    BIN:                'moon_bin',
    CODE:               'moon_embed2',
    COPY:               'moon_copy',
    CLEAR:              'moon_cross',
    DUPLICATE:          'clone',
    ERROR:              'ban',
    GLOBAL:             'moon_checkbox-unchecked',
    HELP:               'moon_info1',
    HIGHLIGHTING:       'palette',
    INFO:               'moon_info1',
    LAYOUT:             'moon_insert-template',
    MAXIMIZE:           'expand', // moon_enlarge2
    MINIMIZE:           'compress', // moon_shrink2
    MENU:               'moon_menu',
    MENU3:              'moon_menu3',
    NEW:                'moon_table2',
    MAGIC_WAND:         'wand-magic-sparkles',
    MAGIC_WAND2:        'moon_magic-wand',
    MINUS:              'moon_minus',
    MOVE_UP:            'moon_move-up',
    MOVE_DOWN:          'moon_move-down',
    OK:                 'moon_checkmark',
    OPTIONS:            'moon_cog',
    PASTE:              'moon_paste',
    PLUS:               'plus',
    REDO:               'rotate-right',
    RESET:              'repeat',
    SEARCH:             'moon_search',
    TABLE:              'moon_table',
    TABLE2:             'moon_table2',
    THEME:              'moon_contrast',
    TRANSPOSE:          'retweet',
    ROCKET:             'moon_rocket',
    UNDO:               'rotate-left',
    UPLOAD:             'upload',
    // UPLOAD:             'moon_upload',
    WARNING:            'moon_warning'

});
