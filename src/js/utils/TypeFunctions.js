/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

function isArray(element) {
    return (
        element != null &&
        element.constructor === Array
    );
}

function isObject(element) {
    return (
        typeof element === 'object' &&
        !isArray(element) &&
        element !== null
    );
}

/**
 * Determines whether this element is a JavaScript primitive.
 * Note that isPrimitive(null) will return true.
 *
 * @param element  the element to process
 * @return true if the specified element is a JavaScript primitive; false otherwise
 */
function isPrimitive(element) {
    return (element == null) || (!isArray(element) && !isObject(element));
}

function isFunction(element) {
    return (typeof element === 'function');
}
