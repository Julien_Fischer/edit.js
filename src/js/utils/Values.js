/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Values = {

    /**
     * Returns true if this value is between min (inclusive) and max (inclusive).
     *
     * @param {number} x  the value to process
     * @param {number} min  the lower bound (inclusive)
     * @param {number} max  the higher bound (inclusive)
     * @return {boolean}
     */
    between: (x, min, max) => {
        return (x >= min && x <= max);
    },

    or: (value, fallback) => {
        return (value == null) ? fallback : value;
    },

    orNull: (value) => {
        return Values.or(value, null);
    },

    ifTrue: (bool, value, fallback) => {
        return bool ? value : fallback;
    },

    either: (value, ...elements) => {
        return ArrayUtils.contains([...elements], value);
    },

    /**
     * Guarantees this value is restricted to the range defined by (min, max).
     *
     * @param {number} value  the value to process
     * @param {number} min  the lower bound (inclusive)
     * @param {number} max  the higher bound (inclusive)
     * @return {number}
     */
    restrict: (value, min, max) => {
        if (value < min) {
            return max;
        }
        if (value > max) {
            return min;
        }
        return value;
    },

    /**
     * Generates a pseudo-random integer between minInclusive and maxInclusive.
     *
     * @param {number} minInclusive  the lower bound
     * @param {number} maxInclusive  the higher bound
     * @return {number}
     */
    randInt: (minInclusive, maxInclusive) => {
        return Math.floor(Math.random() * (maxInclusive - minInclusive + 1) + minInclusive)
    },

    /**
     * Generates a pseudo-random boolean.
     * If no bias is specified, this method has an equal probability of returning
     * either true or false.
     * A bias of 0 will always return false, whereas a bias of 100 will always
     * return true.
     *
     * @param {number} bias  an optional integer between 0 and 100
     * @return {boolean}
     */
    randBool: (bias = 50) => {
        const min = 0, max = 100;
        if (!Values.between(bias, min, max)) {
            throw new Error(`Bias (${bias}) out of range: (${min}, ${max})`)
        }
        const r = Values.randInt(min + 10, max - 20);
        return (r < bias);
    },

    /**
     * Returns a pseudo-random element from the specified iterable.
     *
     * @param {[]|string} iterable  the iterable to return an element from
     * @return {*|string}
     */
    randElement: (iterable) => {
        if (iterable.length === 0) {
            throw new Error('Can not get random element from empty iterable');
        }
        const index = Values.randInt(0, iterable.length - 1);
        return isArray(iterable) ? iterable[index] : iterable.charAt(index);
    },

    /**
     * Returns the time difference between the specified date-time and the current
     * date-time in milliseconds.
     *
     * @param {Date} date  the date-time to compare
     * @return {number}
     */
    timeDiff: (date) => {
        return (Date.now() - date);
    }

}
