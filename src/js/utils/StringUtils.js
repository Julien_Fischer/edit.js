/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const StringUtils = {

    /**
     * Generates of string containing {@code n} times the specified character
     * sequence.
     *
     * @param {string} sequence  the sequence to repeat
     * @param {number} n  the number of times the specified sequence must be repeated
     * @return {string} a string containing n times the specified sequence
     */
    repeat: (sequence, n) => {
        let str = '';
        for (let i = 0; i < n; i++) {
            str += sequence;
        }
        return str;
    },

    /**
     * Escapes the characters in the specified input string using HTML entities.
     *
     * @param {string|string[]} input  the input string or array of strings to escape
     * @return {string|string[]}
     */
    escapeHTMLChars: (input) => {
        return (isArray(input) ? input.map(e => escape(e)) : escape(input));
        function escape(input) {
            let str = input;
            ObjectUtils.forEach(HtmlEntities, (key, value) => {
                str = str.replaceAll(value.unescaped, value.escaped);
            });
            return str;
        }
    },

    /**
     * Unescapes the escaped HTML characters in the specified input string.
     *
     * @param {string|string[]} input  the input string or array of strings to unescape
     * @return {string}
     */
    unescapeHTMLChars: (input) => {
        return (isArray(input) ? input.map(e => unescape(e)) : unescape(input));
        function unescape(input) {
            let str = input;
            ObjectUtils.forEach(HtmlEntities, (key, value) => {
                str = str.replaceAll(value.escaped, value.unescaped);
            });
            return str;
        }
    },

    /**
     * This method uses the pattern detailed in the MVN Docs for regular expressions:
     * https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
     *
     * @param {string} subject  the subject string to process
     * @return {string}
     */
    escapeRegexCharacters: (subject) => {
        return subject.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    },

    /**
     * Counts the number of spaces before the first non-white space character
     * in the specified subject string.
     * If the string is blank, this method will return {@code -1}.
     *
     * @param {string} str  the subject string
     * @return {number}
     */
    countSpacesBefore: (str) => {
        return str.search(/\S/);
    },

    countSpacesAfter: (str) => {
        return str.match(/\s*$/)[0].length;
    },

    countOccurrences: (str, pattern, literal) => {
        const p = literal ? StringUtils.escapeRegexCharacters(pattern) : pattern;
        const regex = new RegExp(p, 'g');
        return (str.match(regex) || []).length;
    },

    /**
     * Determines whether the specified string is null, empty, or contains
     * only whitespace characters.
     *
     * @param {string} str  the string to process
     * @return {boolean} true if the string is blank; false otherwise
     */
    isBlank: (str) => {
        return (str == null || str.trim() === '');
    },

    removeFirstAndLastCharacter: (str) => {
        return str.trim().substring(1, str.length - 1);
    },

    /**
     * Returns a human-readable string representation of the specified array
     * of elements.
     *
     * @param {any} element  the element to pretty print
     * @return {string}
     */
    prettyPrint: (element) => {
        if (element == null) return '(' + element + ')';
        if (isArray(element)) {
            return element.map(e => StringUtils.prettyPrint(e));
        } else {
            if (element.displayName !== undefined) return element.displayName;
            if (element.toString !== undefined) return element.toString();
            return element;
        }
    },

    quote: (subject, prefix = "'", suffix = "'") => {
        return prefix + subject + suffix;
    },

    /**
     * Applies the specified {@link TextCase} to this sentence.
     * If the provided {@link TextCase} is null, this method just returns the input
     * sentence.
     *
     * @param {string} sentence  the sentence to transform
     * @param {TextCase} textCase  the {@link TextCase} constant to apply. Can be null
     * @return {string} the result of the specified text case being applied to this sentence
     */
    formatCase: (sentence, textCase) => {
        if (textCase == null) {
            return sentence;
        }
        switch (textCase) {
            case TextCase.LOWER:        return sentence.toLowerCase();
            case TextCase.UPPER:        return sentence.toUpperCase();
            case TextCase.CAPITALIZE:   return sentence.split(/ /).map(word => StringUtils.capitalize(word)).join(' ');
            case TextCase.SENTENCE:     return StringUtils.capitalize(sentence);
            default:                    throw new Error('Unsupported case: ' + textCase);
        }
    },

    /**
     * Determines whether this subject string matches the specified {@link TextCase}.
     *
     * @param {string} subject  the subject string to process
     * @param {TextCase} textCase  a {@link TextCase} constant
     * @return {boolean} true if the subject string matches the specified {@link TextCase} constant;
     * false otherwise
     */
    isCase: (subject, textCase) => {
        switch (textCase) {
            case TextCase.LOWER:        return subject === subject.toLowerCase();
            case TextCase.UPPER:        return subject === subject.toUpperCase();
            case TextCase.CAPITALIZE:   return subject === subject.split(/ /).map(word => StringUtils.capitalize(word)).join(' ');
            case TextCase.SENTENCE:     return subject === StringUtils.capitalize(subject);
            default:                    throw new Error('Unsupported case: ' + textCase);
        }
    },

    /**
     * Returns the {@link TextCase} matching the specified subject string.
     *
     * @param {string} subject  the subject string to process
     * @return {TextCase} the {@link TextCase} associated to this subject string; or null if the
     * subject string formatting does not match any known {@link TextCase}.
     */
    getCase: (subject) => {
        return [TextCase.LOWER, TextCase.UPPER, TextCase.CAPITALIZE, TextCase.SENTENCE]
            .find(e => StringUtils.isCase(subject, e));
    },

    /**
     * Capitalizes a single word.
     *
     * @param {string} word  the word to capitalize
     * @return {string} the capitalized word
     */
    capitalize: (word) => {
        return word.charAt(0).toUpperCase() + word.toLowerCase().substring(1, word.length);
    },

    max: (a, b) => {
        return (a.length > b.length) ? a : b;
    },

    cut: (subject, indices, removeCount = 0) => {
        let prevIndex = -removeCount;
        const parts = [];
        indices.forEach(index => {
            if (index > subject.length) {
                throw new Error('Index ' + index + ' out of bounds: (0, ' + subject.length + ') for input string: ' + subject);
            }
            const part = subject.substring(prevIndex + removeCount, index);
            parts.push(part);
            prevIndex = index;
        });
        const lastIndex = ArrayUtils.getLast(indices);
        const lastPart = (lastIndex === subject.length) ? '' : subject.substring(lastIndex + removeCount);
        parts.push(lastPart);
        return parts;
    },

    /**
     * Returns the indices of every occurrence of the specified pattern in this subject
     * string.
     *
     * @param {string} subject  the array to process
     * @param {string|RegExp} pattern  the pattern to look for
     * return the indices of every occurrence of the specified element in this array.
     */
    indicesOf: function(subject, pattern) {
        const indices = [];
        let i = -1;
        while ((i = this.indexOf(subject, pattern, i + 1)) > -1) {
            indices.push(i);
        }
        return indices;
    },

    /**
     * Returns the index of the first occurrence of the specified pattern in this subject
     * string, starting from startPos.
     *
     * @param {string} subject  the array to process
     * @param {string|RegExp} pattern  the pattern to look for
     * return the indices of every occurrence of the specified element in this array.
     * @param {number} offset  the index to start from
     * @return {number}
     */
    indexOf: (subject, pattern, offset = 0) => {
        const i = subject.substring(offset).search(pattern);
        return (i < 0) ? i : (i + offset);
    },

    /**
     * Matches a string and returns an {@link Occurrence} if any is matched; null otherwise.
     *
     * @param {string} subject  the subject string to process
     * @param {string|RegExp} pattern  the pattern to look for
     * @param {number=} [offset]  the index to start from
     * @return {Occurrence|null}
     */
    match: (subject, pattern, offset = 0) => {
        const r = subject.substring(offset).match(pattern);
        return (r === null) ? null : new Occurrence(r[0], r.index + offset);
    },

    pluralize: (word, n) => {
        return (n > 1) ? word + 's' : word;
    },

    /**
     * Applies the specified alignment to this subject string.
     *
     * @param {string} text  the text to align
     * @param {number} n  the available space to use for aligning the text
     * @param {Alignment} alignment  the {@link Alignment} to apply to this string
     * @return {string} the aligned text
     */
    alignText: (text, n, alignment) => {
        if (alignment === Alignment.CENTER) {
            const left = Math.round(n / 2);
            const right = n - left;
            const leftSpaces = StringUtils.repeat(" ", left);
            const rightSpaces = StringUtils.repeat(" ", right);
            return leftSpaces + text + rightSpaces;
        }
        const spaces = StringUtils.repeat(" ", n);
        return (alignment === Alignment.LEFT) ? text + spaces : spaces + text;
    },

    /**
     * Executes the specified function once for each character in this string.
     *
     * @param {string} subject  the subject string to apply the forEach function to
     * @param {function(string, number)} fn  the function to apply to each character in this string
     */
    forEach: (subject, fn) => {
        for (let i = 0; i < subject.length; i++) {
            fn(subject.charAt(i), i);
        }
    },

    /**
     * Returns true if this character is a linefeed, carriage return, or both.
     *
     * @param {string} char  the character or sequence of characters to process
     * @return {boolean}
     */
    isLinefeed: (char) => {
        return Values.either(char, '\n', '\r', '\r\n');
    },

    /**
     * Returns the index of the first non-whitespace character in this string
     * from the specified direction.
     *
     * @param subject  the subject string to process
     * @param {boolean=} [backward]  if false or not specified, returns the first
     * non-whitespace character; otherwise returns the last non-whitespace character
     * @return {number}
     */
    indexOfFirstNonWhitespaceCharacter: (subject, backward) => {
        if (backward) {
            for (let i = subject.length - 1; i >= 0; i--) {
                if (subject.charAt(i) !== ' ') {
                    return i + 1;
                }
            }
        } else {
            for (let i = 0; i < subject.length; i++) {
                if (subject.charAt(i) !== ' ') {
                    return i;
                }
            }
        }
        return -1;
    },

    /**
     * Checks if this subject string contains only characters that can be parsed as a number.
     * This method supports float and integer values.
     *
     * @param {string|number} subject  the subject string to process
     * @return {boolean} true if this string can be parsed as a float or integer; false otherwise
     */
    isNumeric: (subject) => {
        return !isNaN(parseFloat(subject));
    }

};
