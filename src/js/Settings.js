/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Settings = {

  INITIAL_ROWS: 2,
  INITIAL_COLUMNS: 2,
  HISTORY_CAPACITY: 1000,
  MESSAGE_DURATION: 2500,
  MESSAGE_POOL_CAPACITY: 5,
  DEFAULT_FORMAT: Format.PLAIN_ASCII,
  DEFAULT_CSV_SEPARATOR: ',',
  DEFAULT_LATEX_PLACEHOLDER: '#', // whitespace characters are also commonly used
  SHOW_LINE_NUMBERS: true,
  ALPHABET: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  PREFILL_EMPTY_CELLS_ALLOW: true,
  PREFILL_EMPTY_CELLS_BIAS: 60,
  SYNTAX_HIGHLIGHTING: true,
  DEFAULT_LAYOUT_ORIENTATION: Axis.X,
  LAYOUT_BREAKPOINT: ScreenSize.M,
  /** If no theme is specified, uses the OS preferred theme (if supported by browser version) */
  THEME: null,
  /** The fallback theme to use if the browser version does not provide access to the OS preferred theme */
  FALLBACK_THEME: Themes.LIGHT

};
