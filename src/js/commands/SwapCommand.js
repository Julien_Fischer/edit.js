/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Swaps two rows or two columns in the specified {@link EditorTable}.
 *
 * @param {EditorTable} table  the target {@link EditorTable} for this operation
 * @param {number} i1  the index of the first element to swap
 * @param {number} i2  the index of the other element to swap
 * @param {Axis} axis  the {@link Axis} of the swap operation
 */
const SwapCommand = function(table, i1, i2, axis) {

    this.execute = () => {
        swap();
    }

    this.undo = () => {
        swap();
    }

    const swap = () => {
        if (axis === Axis.X) {
            table.swapColumns(i1, i2);
        } else {
            table.swapRows(i1, i2);
        }
    }

}
