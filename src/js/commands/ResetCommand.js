/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Resets the specified {@link EditorTable} to its initial state.
 * If no dimension is specified, the table is reset to its initial dimension.
 *
 * @param {EditorTable} table  the target {@link EditorTable} for this operation
 * @param {TableModel} model  the {@link TableModel} before the table is reset
 * @param {number} w  the new table width
 * @param {number} h  the new table height
 */
const ResetCommand = function(table, model, w, h) {

    this.execute = () => {
        table.reset(w, h);
    }

    this.undo = () => {
        const data = model.getData();
        const rows = data.length;
        const cols = data[0].length;
        table.reset(cols, rows);
        table.forEachCoordinate((x, y) => {
            const datum = data[y][x];
            table.set(x, y, datum, false);
        });
    }

}
