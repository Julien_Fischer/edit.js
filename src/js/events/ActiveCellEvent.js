/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This event is emitted each time a table cell is activated, via a system or
 * user-initiated event.
 *
 * @param logicalCoordinate  the logical coordinates at which the event occurred
 * @param physicalCoordinate  the physical coordinates at which the event occurred, in pixels
 */
const ActiveCellEvent = function(logicalCoordinate, physicalCoordinate) {

    this.getLogicalCoordinate = () => {
        return logicalCoordinate;
    }

    this.getPhysicalCoordinate = () => {
        return physicalCoordinate;
    }

}
