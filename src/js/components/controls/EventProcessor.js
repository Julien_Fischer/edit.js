/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Instances of this class are responsible for registering and triggering actions based
 * on events given as input.
 *
 * @param actions  an optional list of {@link Action} objects
 *
 * @see Action
 */
const EventProcessor = function(...actions) {

    const actionList = [];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Determines whether an action registered by this processor must be triggered
     * by this event.
     *
     * @param evt  the event to process
     * @return true if an action registered by this processor must be triggered
     * by this event.
     */
    this.supports = (evt) => {
        return (this.getAction(evt) !== null);
    }

    /**
     * Returns the action which must be triggered by this event is any is found.
     *
     * @param evt  the event to process
     * @return return an {@link Action} object or {@code null} if no registered action
     * supports the specified event
     */
    this.getAction = (evt) => {
        const matches = actionList.filter(action => action.supports(evt));
        return (matches.length > 0) ? matches[0] : null;
    }

    /**
     * Returns the list of actions registered by this event processor.
     *
     * @return a read-only array of actions registered by this event processor
     */
    this.getActions = () => {
        return [...actionList];
    }

    /**
     * Registers the specified actions to this event processor.
     *
     * @param actions  the {@link Action} to register
     */
    this.register = (...actions) => {
        actionList.push(...actions);
    }

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    const init = () => {
        this.register(...actions);
    }

    init();

}
