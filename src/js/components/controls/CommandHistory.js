/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Instances of this class store commands which can be undone and
 * redone depending of their order of insertion.
 *
 * @param {number} c  the maximum capacity
 */
const CommandHistory = function(c) {

    const DEFAULT_START_INDEX = -1;

    const capacity = c;
    let entries = [];
    let cursor;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.save = (command) => {
        if (this.isFull()) {
            removeOldestEntry();
        }
        if (cursor < entries.length - 1) {
            removeUndoneEntries();
        }
        entries.push(command);
        cursor = entries.length - 1;
        printElements('save');
    }

    this.undo = () => {
        if (canUndo()) {
            entries[cursor].undo();
            cursor--;
        }
        printElements('undo');
    }

    this.redo = () => {
        if (canRedo()) {
            cursor++;
            entries[cursor].execute();
        }
        printElements('redo');
    }

    this.isEmpty = () => {
        return this.getSize() === 0;
    }

    this.isFull = () => {
        return this.getSize() === capacity;
    }

    this.clear = () => {
        entries = [];
        cursor = DEFAULT_START_INDEX;
    }

    this.getSize = () => {
        return entries.length;
    }

    this.getCapacity = () => {
        return capacity;
    }

    this.toString = () => {
        let str = '';
        entries.forEach((entry, i) => {
            const c = (i === cursor) ? '-> ' : '   ';
            str += '\n' + c + entry.constructor.name.replace('Command', '');
        });
        return (str === '') ? '\nNo entries' : str;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const removeUndoneEntries = () => {
        const n = entries.length - 1;
        const r = n - (cursor + 1);
        const x = n - r;
        entries = entries.slice(0, x);
    }

    const removeOldestEntry = () => {
        entries.shift();
    }

    const canRedo = () => {
        return cursor < entries.length - 1 && !this.isEmpty();
    }

    const canUndo = () => {
        return cursor > DEFAULT_START_INDEX && !this.isEmpty();
    }

    const printElements = (action) => {
        Logger.debug('history', action.toUpperCase() + ' ACTION', this.toString());
    }

};
