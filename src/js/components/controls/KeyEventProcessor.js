/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A utility class providing methods for processing events which describe a user
 * interaction with the keyboard
 */
const KeyEventProcessor = {

    /**
     * Determines whether the specified keys include the key pressed
     * in this event.
     *
     * @param evt  the key event to process
     * @param keys  the keys to test the event key against
     * @return true if on of the specified keys is pressed; false otherwise
     */
    isEither: (evt, ...keys) => {
        return (keys.indexOf(evt.key) > -1);
    },

    /**
     * Determines whether an arrow key is pressed.
     *
     * @param evt  the key event to process
     * @return true if an arrow key is pressed; false otherwise
     */
    isArrowKey: (evt) => {
        return (KeyEventProcessor.isVertical(evt) || KeyEventProcessor.isHorizontal(evt));
    },

    /**
     * Determines whether a vertical arrow key is pressed.
     *
     * @param evt  the key event to process
     * @return true if an either {@link Key#UP} or {@link Key#DOWN} is pressed;
     * false otherwise
     */
    isVertical: (evt) => {
        return KeyEventProcessor.isEither(evt, Key.UP, Key.DOWN);
    },

    /**
     * Determines whether an horizontal arrow key is pressed.
     *
     * @param evt  the key event to process
     * @return true if an either {@link Key#LEFT} or {@link Key#RIGHT} is pressed;
     * false otherwise
     */
    isHorizontal: (evt) => {
        return KeyEventProcessor.isEither(evt, Key.LEFT, Key.RIGHT);
    },

    /**
     * Converts the specified arrow key to a direction.
     *
     * @param key  the {@link Key} constant to process
     * @return a {@link Vector} object
     */
    toDirection: (key) => {
        switch (key) {
            case Key.UP:    return Direction.UP;
            case Key.LEFT:  return Direction.LEFT;
            case Key.DOWN:  return Direction.DOWN;
            case Key.RIGHT: return Direction.RIGHT;
        }
    },

    /**
     * Returns the coordinate of the mouse pointer on the screen at the time this event
     * was dispatched.
     *
     * @param evt  the event to fetch the cursor coordinates from
     * @return a {@link Vector} object
     */
    toPhysicalCoordinate: (evt) => {
        return new Vector(evt.clientX, evt.clientY);
    },

    /**
     * Returns the modifier keys associated to this event.
     *
     * @param evt  the event to process
     * @return an array of {@link Modifier} keys
     */
    getModifiers: (evt) => {
        const list = [];
        if (evt.ctrlKey) list.push(Modifier.CTRL);
        if (evt.altKey) list.push(Modifier.ALT);
        if (evt.shiftKey) list.push(Modifier.SHIFT);
        return list;
    }

}
