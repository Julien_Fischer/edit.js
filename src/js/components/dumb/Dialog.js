/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Dialog = function() {

    Component.call(this);

    const dialogListeners = [];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.addDialogListener = (listener) => {
        dialogListeners.push(listener);
    }

    this.toggle = () => {
        this.setOpen(!this.isVisible());
    }

    // This method should always be final
    this.setOpen = (open) => {
        this.setVisible(open);
        if (open) {
            this.open();
        } else {
            this.close();
        }
        dialogListeners.forEach(listener => listener.onOpenStateChange(this, open));
    }

    // This method should be overridden by subtypes which require specialized behavior on open
    this.open = () => { }

    // This method should be overridden by subtypes which require specialized behavior on close
    this.close = () => { }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        this.addClass('dialog')
            .setVisible(false);
    }

    init();

}
