/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A generic component controlling a graphic representation on the screen.
 * This class can be either used as-is or extended to provide additional
 * functionality.
 * The UI points to an html document node which may or may not be
 * appended to the DOM when {@link Component.getUI} is called.
 *
 * @param {string} tagName  the type of document node to instantiate (div by default)
 * @see Component.getUI
 */
const Component = function(tagName = 'div') {

    let ui;
    let componentListeners = [];

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Adds the specified {@link Component} as a child of this component.
     *
     * @param {Component} component  a {@link Component} instance
     * @return this object
     */
    this.add = (component) => {
        this.addNode(component.getUI());
        return this;
    };

    /**
     * Adds the specified document node as a child of this component ui.
     *
     * @param {Element} node  a document node
     * @return this object
     */
    this.addNode = (node) => {
        ui.appendChild(node);
        return this;
    };

    /**
     * Removes all nodes inside this element.
     * @return this object
     */
    this.clearNodes = () => {
        ui.innerHTML = '';
        return this;
    }

    /**
     * Returns the document node associated to this component.
     *
     * @return a document node
     */
    this.getUI = () => {
        return ui;
    }

    /**
     * Sets the document node associated to this component.
     * This method does not modify the DOM.
     *
     * @param {HTMLElement} node  a document node
     * @return this object
     */
    this.setUI = (node) => {
        ui = node;
        return this;
    }

    /**
     * Sets the id attribute of this DOM element.
     *
     * @param {string} id  a string
     * @return this object
     */
    this.setId = (id) => {
        ui.id = id;
        return this;
    }

    /**
     * Returns the id attribute of this DOM element.
     *
     * @return a string
     */
    this.getId = () => {
        return ui.id;
    }

    /**
     * Determines whether this component has this css class.
     *
     * @param {string} cssClass  the css class to lookup
     * @return true if this class is already present in this component class list;
     * false otherwise.
     */
    this.hasClass = (cssClass) => {
        return ui.classList.contains(cssClass);
    }

    /**
     * Removes the specified css class from this component class list if
     * it is already present; else removes the specified css class.
     *
     * @param {string} cssClass  the css class to toggle
     * @param {boolean=} [force]  optional parameter. If true, adds the css class. If
     *               false, removes it
     * return true if the class was added; false if it was removed
     */
    this.toggleClass = (cssClass, force) => {
        return ui.classList.toggle(cssClass, force);
    }

    /**
     * Adds the specified css classes to this DOM element.
     *
     * @param {...*} classes  an array of strings
     * @return this object
     */
    this.addClass = (...classes) => {
        for (let c of classes) {
            ui.classList.add(c);
        }
        return this;
    }

    /**
     * Removes this css class to this DOM element
     *
     * @param {string} cssClass  the css class to remove
     */
    this.removeClass = (cssClass) => {
        ui.classList.remove(cssClass);
    }

    /**
     * Sets the value of the css property identified by this key to {@code value}
     *
     * @param {string} key  the name of the css property to modify
     * @param {string|number} value  the value to set this property to
     * @return this object
     */
    this.putStyle = (key, value) => {
        ui.style[key] = !isNaN(value) ? '' + value : value;
        return this;
    }

    /**
     * Resets the value of the css property identified by this key
     *
     * @param {string} key  the name of the css property to remove
     * @return {string} the value of the removed property
     */
    this.removeStyle = (key) => {
        const style = this.getStyle(key);
        ui.style.removeProperty(key);
        return style;
    }

    /**
     * Returns the value of the css property identified by this key
     *
     * @param {string} key  the name of the css property to lookup
     * @return {string} the value of this property
     */
    this.getStyle = (key) => {
        return ui.style[key];
    }

    /**
     * Defines the text to be displayed in this component tooltip.
     *
     * @param {string} tip  the tip to display on mouse over
     * @return this object
     */
    this.setTip = (tip) => {
        ui.title = tip;
        return this;
    }

    /**
     * Defines the rendered text content of this component.
     *
     * @param {number|string} text  the text to render
     * @return this object
     */
    this.setText = (text) => {
        ui.innerText = text;
        return this;
    }

    /**
     * Defines the markup contained within this component.
     *
     * @param {string} html  the HTML or XML markup to render by this component
     * @return this object
     */
    this.setHtml = (html) => {
        ui.innerHTML = html;
        return this;
    }

    /**
     * Defines this component visibility.
     *
     * @param {boolean} visible  if true, makes this component visible; if false, makes this
     * component disappear on screen as well as from the document flow
     * @return this object
     */
    this.setVisible = (visible) => {
        this.toggleClass('hidden', !visible);
        dispatchVisibilityChangeEvent(visible);
        return this;
    }

    /**
     * Determines whether this object is currently visible.
     *
     * @return true if this object is visible; false otherwise
     */
    this.isVisible = () => {
        return !this.hasClass('hidden');
    }

    /**
     * Moves this component to the specified coordinate.
     *
     * @param {Vector} vector  the coordinates where to move this component to
     */
    this.moveTo = (vector) => {
        this
            .putStyle('left', vector.getX() + 'px')
            .putStyle('top',  vector.getY() + 'px');
    }

    /**
     * Returns the number of pixels that the upper left corner of this component
     * is offset to the left and to the top within its parent node.
     *
     * @return {Vector}  the offset of this component within its parent, in pixels
     */
    this.getOffset = () => {
        return new Vector(ui.offsetLeft, ui.offsetTop);
    }

    /**
     * Returns this component total dimension, including its borders.
     *
     * @return {Vector}  the total dimension of this object
     */
    this.getDimension = () => { return new Vector(this.getWidth(), this.getHeight()); }

    /**
     * Returns the length of this component on the specified {@link Axis}.
     *
     * @param {Axis} axis  the axis of the length to return
     * @return {number}
     */
    this.getSize = (axis) => { return (axis === Axis.X) ? this.getWidth() : this.getHeight(); }

    /**
     * Returns the width of this component.
     *
     * @return {number}  the total width of this component, including its top and bottom borders
     */
    this.getWidth = () => { return ui.offsetWidth; }

    /**
     * Returns the height of this component.
     *
     * @return {number}  the total width of this component, including its left and right borders
     */
    this.getHeight = () => { return ui.offsetHeight; }

    /**
     * Adds the specified event listener to this DOM element
     *
     * @param evt  the event to subscribe to
     * @param fn  the callback to execute each time this event is dispatched
     * @param options  the options to apply
     * @return this object
     */
    this.addListener = (evt, fn, options) => {
        ui.addEventListener(evt, fn, options);
        return this;
    }

    /**
     * Registers this listener to be notified when this component size, location,
     * or visibility changes.
     *
     * @param componentListener  the class that is interested in processing this
     *                           component events.
     */
    this.addComponentListener = (componentListener) => {
        componentListeners.push(componentListener);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const dispatchVisibilityChangeEvent = (visible) => {
        componentListeners.forEach(l => l.onVisibilityChanged(this, visible));
    }

    const init = () => {
        this.setUI(document.createElement(tagName));
    }

    init();

}
