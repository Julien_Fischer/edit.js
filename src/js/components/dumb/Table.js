/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A read-only, graphical representation of a 2d array of strings, which supports
 * an optional list of headers.
 *
 * @param {string[][]} data  the data to be displayed by this table
 * @param {string[]=} [headers]  an optional list of headers
 * @param {function(string, number, number, string): Element=} [cellFormatter]  an
 * optional function responsible for drawing the cell content
 */
const Table = function(data, headers = [], cellFormatter) {

    Component.call(this, 'table');

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const getHeader = (x) => {
        return (x >= headers.length) ? null : headers[x];
    }

    const createHeader = (headers) => {
        const thead = document.createElement('thead');
        const headerRow = document.createElement('tr');
        headers.forEach(cell => {
            const node = document.createElement('th');
            node.innerText = cell;
            headerRow.appendChild(node);
        });
        thead.appendChild(headerRow);
        return thead;
    }

    const createBody = (data) => {
        const tbody = document.createElement('tbody');
        data.forEach((row, y) => {
            const rowNode = document.createElement('tr');
            row.forEach((cell, x) => {
                const cellNode = document.createElement('td');
                cellNode.appendChild(cellFormatter(cell, x, y, getHeader(x)));
                rowNode.appendChild(cellNode);
            });
            tbody.appendChild(rowNode);
        })
        return tbody;
    }

    const createDefaultCellFormatter = () => {
        return (cell, x, y, header) => document.createTextNode(cell);
    }

    const init = () => {
        if (cellFormatter == null) {
            cellFormatter = createDefaultCellFormatter();
        }
        if (headers.length > 0) {
            this.addNode(createHeader(headers));
        }
        this.addNode(createBody(data))
            .addClass('table-read-only');
    }

    init();

}
