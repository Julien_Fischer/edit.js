/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} combines a button and a dropdown list.
 * This implementation is built on top of an HTML select element.
 *
 * @param title  the title of this component
 * @param enumObject  the enum object to be rendered by this component
 * @param defaultValue  the default value for this component
 * @param tip  a textual description to display on mouse over
 * @param cssClasses  provides additional styles to this component
 */
const Combobox = function (title, enumObject, defaultValue, tip, cssClasses = []) {

    Component.call(this);

    const CHAR_LIMIT = 32;

    // Config
    const values = Object.values(enumObject);
    let placeholder;
    let callback;
    let itemFormatter;
    // UI
    let htmlSelect;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getValue = function() {
        const selectedIndex = htmlSelect.selectedIndex;
        if (selectedIndex < 0) {
            return defaultValue;
        }
        const selectedElement = htmlSelect.options[selectedIndex];
        const key = selectedElement.value;
        if (key === placeholder) {
            return defaultValue;
        }
        return Enums.lookup(enumObject, key);
    };

    this.setValue = function(newValue) {
        for (let i = 0; i < values.length; i++) {
            const enumValue = values[i];
            if (enumValue === newValue) {
                htmlSelect.selectedIndex = i;
                notifyListeners();
                return;
            }
        }
        throw new Error(newValue + ' is not a constant of enum ' + enumObject);
    };

    this.reset = function() {
        if (defaultValue !== null) {
            this.setValue(defaultValue);
        } else {
            htmlSelect.selectedIndex = 0;
        }
    };

    this.registerCallback = function(then) {
        callback = then;
        return this;
    };

    this.withItemFormatter = function(formatter) {
        itemFormatter = formatter;
        displayOptions();
        return this;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const displayOptions = () => {
        htmlSelect.innerHTML = '';
        values.splice(0, values.length);
        ObjectUtils.forEach(enumObject, (key, value) => addOption(key, value));
        if (defaultValue != null) {
            this.setValue(defaultValue);
        }
    };

    const addOption = (key, value) => {
        const formatted = itemFormatter(value);
        const valueToDisplay = formatted.substr(0, CHAR_LIMIT);
        const suffix = valueToDisplay.length < value.displayName.length ? '...' : '';
        values.push(value);
        const option = new Component('option')
            .setText(valueToDisplay + suffix)
            .setTip(formatted);
        option.getUI().value = key;
        htmlSelect.appendChild(option.getUI());
    }

    const defaultItemFormatter = (item) => {
        const prefix = StringUtils.isBlank(item.type) ? '' : '[' + item.type + '] ';
        return prefix + item.displayName;
    }

    const notifyListeners = () => {
        if (callback != null) {
            callback(this.getValue());
        }
    };

    const createSelectNode = (title, desc) => {
        placeholder = '-- ' + title + ' --';
        const titleOption = new Component('option')
            .setText(placeholder);
        return new Component('select')
            .addClass('clickable')
            .addListener('change', notifyListeners, false)
            .add(titleOption)
            .setTip(desc)
            .getUI();
    };

    const createCaption = (title) => {
        const captionLabel = new Component()
            .setText(title)
            .addClass('form-element-caption-label', 'label-center');
        return new Component()
            .addClass('form-element-caption')
            .add(captionLabel);
    };

    const init = (title, desc) => {
        itemFormatter = defaultItemFormatter;
        const caption = createCaption(title);
        htmlSelect = createSelectNode(title, desc);
        const wrapper = new Component()
            .addClass('form-element-wrapper')
            .add(caption)
            .addNode(htmlSelect);
        this.add(wrapper)
            .addClass(...cssClasses, 'form-element', 'clickable');
        displayOptions();
    };

    init(title, tip);

}
