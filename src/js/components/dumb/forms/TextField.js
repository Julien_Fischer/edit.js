/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A {@link Component} that allows the editing of a single line of text.
 * This implementation is built on top of an HTML input element.
 *
 * @param title  the title of this component
 * @param defaultVal  the default value for this component
 * @param desc  a textual description to display on mouse over
 */
const TextField = function(title, defaultVal, desc) {

    Component.call(this);

    const defaultValue = defaultVal;
    const description = desc;
    let callback;
    let htmlInput;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.setValue = (val) => {
        htmlInput.value = val;
        notifyListeners();
    };

    this.getValue = () => {
        return htmlInput.value;
    };

    this.setValue = (val) => {
        htmlInput.value = val;
    }

    this.reset = function() {
        this.setValue((defaultValue !== null) ? defaultValue : '');
    };

    this.registerCallback = function(then) {
        callback = then;
        htmlInput.addEventListener('input', callback, false);
        return this;
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const notifyListeners = () => {
        if (callback != null) {
            callback(this.getValue());
        }
    };

    const createHtmlCaption = (title) => {
        const label = new Component()
            .setText(title)
            .addClass('form-element-caption-label', 'label-center');
        return new Component()
            .addClass('form-element-caption')
            .add(label)
            .getUI();
    };

    const createHtmlInput = () => {
        htmlInput = new Component('input')
            .addClass('clickable')
            .addClass('textfield-input')
            .setTip(description)
            .getUI();
        htmlInput.type = 'text';
        htmlInput.placeholder = title;
        if (defaultValue != null) {
            htmlInput.value = defaultValue;
        }
        return htmlInput;
    };

    const init = () => {
        const htmlCaption = createHtmlCaption(title);
        htmlInput = createHtmlInput();
        const wrapper = new Component()
            .addClass('form-element-wrapper')
            .addClass('form-element')
            .addNode(htmlCaption)
            .addNode(htmlInput);
        this.add(wrapper);
    };

    init();

};
