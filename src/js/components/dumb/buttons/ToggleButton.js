/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A two-state button intended to be used in conjunction with a {@link ButtonGroup}.
 *
 * @param {string} text  the text of the button
 * @param {function(Event): void} callback  the callback to execute when this button is activated
 * @param {boolean=} [tab]  if true, styles the toggle buttons as tabs
 * @param {any=} [value]  an optional value represented by this button
 */
const ToggleButton = function(text, callback, tab = false, value) {

    Component.call(this, 'button');

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.setActive = (active) => {
        if (active) {
            this.addClass('active');
        } else {
            this.removeClass('active');
        }
    }

    this.getValue = () => {
        return value;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        const t = tab ? '-tab' : '';
        const classes = ['clickable', 'toggle-button' + t];
        this.addClass(...classes)
            .addListener('click', callback, false)
            .setText(text);
    }

    init();

}
