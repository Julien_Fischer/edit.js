/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} is the base implementation of a push-button.
 *
 * @param {string} text  the button text
 * @param {function(Event): void} callback  the callback to execute on button click
 * @param {string=} [tip]  an optional tip to display on mouse over
 * @param {ButtonStyle} [style]  an optional button style to apply
 * @param {ButtonType} [type]  an optional button type to apply
 */
const Button = function(text = "", callback, tip = '', style = ButtonStyle.FILLED, type = ButtonType.NEUTRAL) {

    Component.call(this, 'button');

    let svgIcon;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.withIcon = (icon, rotation) => {
        if (icon == null) {
            return this;
        }
        this.clearNodes();
        svgIcon = new Icon(icon, rotation);
        if (text !== '') {
            svgIcon.putStyle('marginRight', '10px');
        }
        this.add(svgIcon);
        this.getUI().innerHTML += text;
        return this;
    };

    this.setText = (text) => {
        this.getUI().innerText = text;
        if (svgIcon != null) {
            this.add(svgIcon);
        }
        return this;
    }

    this.setEnabled = (enable) => {
        this.toggleClass('disabled', !enable);
        this.getUI().disabled = !enable;
        return this;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        if (tip != null) {
            this.setTip(tip);
        }
        if (style !== ButtonStyle.DEFAULT) {
            this.addClass('style-' + style);
            this.addClass(type);
        }
        this.setText(text)
            .addClass('clickable')
            .addListener('click', callback, false);
    };

    init();

}
