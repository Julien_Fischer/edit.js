/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * This {@link Component} provides a mutable table which notifies its listeners
 * when its model changes.
 *
 * @param {number} cols  the number of columns to initialize this table with
 * @param {number} rows  the number of rows to initialize this table with
 */
const EditorTable = function(cols, rows) {

    Component.call(this, 'table');

    const HIGHLIGHTED = 'highlighted';

    // Model
    let model = createModel(cols, rows);
    let alignments = [];
    // Listeners
    const structureChangeListeners = [];
    const cellEventListeners = [];
    const keyEventListeners = [];
    // Cache
    /** A {@link Matrix} of HTMLElements */
    let inputMatrix = new Matrix(cols, rows);
    let htmlTableRows = [];
    let columnNames = [];
    let columnNameCells = [];
    let rowNameCells = [];
    let htmlColumnNamesRow;
    let lastCursor;
    let cursor = new Vector(0, 0);

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.importModel = (tableModel) => {
        const data = tableModel.getData();
        const alignments = tableModel.getAlignments();
        const height = data.length;
        const width = data[0].length;
        this.reset(width, height);
        data.forEach((row, y) =>
            row.forEach((cell, x) => {
                this.set(x, y, cell, false);
                if (alignments.length > x) {
                    this.alignColumn(x, alignments[x]);
                }
            }))
        ;
    };

    // Called when an input is added to the input matrix
    this.onCellValueChange = (update) => {
        Logger.verbose('Table', ' - onCellValueChange', update.toString());
        const x = update.getX();
        const y = update.getY();
        const htmlInput = update.getNewValue();
        if (htmlInput != null) {
            const htmlCell = htmlTableRows[y].insertCell(x + 1);
            htmlCell.appendChild(htmlInput);
            htmlCell.addEventListener('click', () => htmlInput.focus(), false);
            htmlInput.addEventListener('focus', () => dispatchActiveCellEvent(htmlInput), false);
        }
    }

    this.onTableStructureChange = () => {
        dispatchStructureChangeEvent();
    }

    this.insertRow = (y = -1, row = null) => {
        const htmlRow = insertHtmlRow(computeDomIndex(y));
        ArrayUtils.insert(htmlTableRows, y, htmlRow);
        insertRowNameCell(htmlRow, y);
        updateRowNameCells();
        const cellArray = toCellArray(row, Axis.X);
        model.insertRow(y, cellArray);
        inputMatrix.insertRow(y);
        cellArray.forEach((cell, x) => createHtmlCell(x, y));
        updateInputs();
        if (y > -1) {
            this.focus(cursor.getX(), y);
        }
        dispatchStructureChangeEvent();
    }

    this.insertColumn = (x = -1, column = null) => {
        ArrayUtils.insert(alignments, x, Alignment.LEFT);
        generateColumnNames();
        insertColumnNameCell(x);
        updateColumnNameCells();
        const cellArray = toCellArray(column, Axis.Y);
        model.insertColumn(x, cellArray);
        inputMatrix.insertColumn(x);
        cellArray.forEach((cell, y) => createHtmlCell(x, y));
        updateInputs();
        if (x > - 1) {
            this.focus(x, cursor.getY());
        }
        dispatchStructureChangeEvent();
    }

    this.removeRow = (y = -1) => {
        const removed = model.removeRow(y);
        inputMatrix.removeRow(y);
        htmlTableRows.splice(y, 1);
        this.getUI().deleteRow(computeDomIndex(y));
        rowNameCells.splice(y, 1);
        updateRowNameCells();
        updateInputs();
        this.focus(cursor.getX(), y - 1);
        dispatchStructureChangeEvent();
        return removed;
    };

    this.removeColumn = (x = -1) => {
        const removed = model.removeColumn(x);
        inputMatrix.removeColumn(x);
        removed.forEach((cell, y) => {
            const htmlRow = htmlTableRows[y];
            htmlRow.deleteCell(computeDomIndex(x));
        });
        columnNameCells.splice(x, 1);
        htmlColumnNamesRow.deleteCell(computeDomIndex(x));
        updateColumnNameCells();
        updateInputs();
        this.focus(x - 1, cursor.getY());
        dispatchStructureChangeEvent();
        return removed;
    };

    this.swapRows = (a, b) => {
        model.swapRows(a, b);
        this.focus(cursor.getX(), a);
        updateInputs();
        dispatchStructureChangeEvent();
    }

    this.swapColumns = (a, b) => {
        ArrayUtils.swap(alignments, a, b);
        model.swapColumns(a, b);
        this.focus(a, cursor.getY());
        updateInputs();
        dispatchStructureChangeEvent();
    }

    this.duplicateRow = (y) => {
        const targetIndex = y + 1;
        const row = this.getRow(y);
        const copy = [...row];
        this.insertRow(targetIndex, copy);
        dispatchStructureChangeEvent();
    }

    this.duplicateColumn = (x) => {
        const targetIndex = x + 1;
        const column = this.getColumn(x);
        const copy = [...column];
        this.insertColumn(targetIndex, copy);
        dispatchStructureChangeEvent();
    }

    /**
     * Copies all cells from row a into row b
     *
     * @param y1  the index of the row to copy
     * @param y2  the index of the target row
     * @return the copied row
     */
    this.copyRow = (y1, y2) => {
        const row = this.getRow(y1);
        row.forEach((cell, x) => setInternal(x, y2, cell));
        return row;
    }

    /**
     * Copies all cells from column a into column  b
     *
     * @param x1  the index of the column to copy
     * @param x2  the index of the target column
     * @return the copied column
     */
    this.copyColumn = (x1, x2) => {
        const column = this.getColumn(x1);
        column.forEach((cell, y) => setInternal(x2, y, cell));
        return column;
    }

    this.set = (x, y, value, notify = true) => {
        const oldValue = setInternal(x, y, value);
        if (notify) {
            dispatchCellValueChangeEvent(new CellUpdate(x, y, oldValue, value));
        }
        dispatchStructureChangeEvent();
        this.focus(x, y);
    }

    this.get = (x, y) => {
        return getCell(x, y).getValue();
    }

    this.getRow = (index) => {
        return toModelData().getRow(index);
    }

    this.getColumn = (index) => {
        return toModelData().getColumn(index);
    }

    this.getHeight = () => { return model.getHeight(); }

    this.getWidth = () => { return model.getWidth(); }

    this.getDimension = () => { return model.getDimension(); }

    this.getLength = (axis) => { return model.getLength(axis); }

    /**
     * Constructs a copy of the backing matrix.
     * This method returns a new {@link TableModel} which can be mutated
     * without affecting the original table model.
     *
     * @return a {@link TableModel} object
     */
    this.getModel = () => {
        return new TableModel(alignments, toModelData().toArray());
    };

    /**
     * Returns the alignment of the column at index x.
     *
     * @param x  the index of the column
     * @return {Alignment} an {@link Alignment} constant
     */
    this.getAlignment = (x) => {
        return alignments[x];
    }

    /**
     * Clears the table cells content.
     * This method does not modify the structure of the table.
     */
    this.clear = () => {
        model.clear(() => new Cell());
        inputMatrix.forEachCell(input => input.value = "");
        dispatchStructureChangeEvent();
    };

    /**
     * Resets the table to its initial state.
     * This method modifies the structure of the table.
     *
     * @param {number} h  the desired number of rows
     * @param {number} w  the desired number of columns
     */
    this.reset = function (h = cols, w = rows) {
        this.clearNodes();
        htmlTableRows = [];
        alignments = [];
        empty(h, w);
        dispatchStructureChangeEvent();
    };

    /**
     * Determines whether this table is blank.
     *
     * @return true if the table contains only empty strings; false otherwise
     */
    this.isBlank = () => {
        return model.isBlank();
    };

    this.contains = (coordinate) => {
        return model.contains(coordinate);
    }

    this.getColumns = () => {
        return model.getColumns();
    }

    this.forEachColumn = (callback) => {
        toModelData().forEachColumn(callback);
    }

    this.forEachRow = (callback) => {
        toModelData().forEachRow(callback);
    }

    this.forEachCell = (callback) => {
        toModelData().forEachCell(callback);
    }

    this.forEachCoordinate = (callback) => {
        model.forEachCoordinate(callback);
    }

    this.fill = (value) => {
        model.fill(new Cell(value));
        inputMatrix.forEachCell(input => input.value = value);
    }

    this.fillColumn = (index, value) => {
        model.fillColumn(index, new Cell(value));
        inputMatrix.getColumn(index)
            .forEach(cell => cell.value = value);
    }

    this.fillRow = (index, value) => {
        model.fillRow(index, new Cell(value));
        inputMatrix.getRow(index)
            .forEach(cell => cell.value = value);
    }

    this.addStructureChangeListener = (listener) => {
        structureChangeListeners.push(listener);
    }

    this.addCellEventListener = (listener) => {
        cellEventListeners.push(listener);
    }

    this.addKeyEventListener = (listener) => {
        keyEventListeners.push(listener);
    }

    this.focus = (x = 0, y = 0) => {
        let newX = f(x, Axis.X);
        let newY = f(y, Axis.Y);
        inputMatrix.get(newX, newY).focus();
        highlightRowAndColumnNames(newX, newY);
        function f(value, axis) {
            let newVal = (axis === Axis.X) ? computeColIndex(value) : computeRowIndex(value);
            if (newVal < 0) {
                newVal = 0;
            }
            return newVal;
        }
    }

    this.select = (coordinate) => {
        this.focus(coordinate.getX(), coordinate.getY());
        inputMatrix.forEachCell(cell => cell.classList.remove(HIGHLIGHTED));
    }

    this.highlightColumn = (index, active) => {
        highlight(index, active, Axis.Y);
    }

    this.highlightRow = (index, active) => {
        highlight(index, active, Axis.X);
    }

    this.alignColumn = (x, alignment) => {
        alignments[x] = alignment;
        alignColumnInternal(x, alignment);
    }

    this.alignRow = (y, alignment) => {
        inputMatrix.getRow(y)
            .forEach((cell, x) => this.alignCell(x, y, alignment));
    }

    this.alignCell = (x, y, alignment) => {
        getCell(x, y).setAlignment(alignment);
        inputMatrix.get(x, y).style.textAlign = alignment.displayName;
    }

    /**
     * Applies this {@link Alignment} to the header identified by the specified
     * {@link Axis}.
     *
     * @param {Axis} axis  the target axis
     * @param {Alignment} alignment  the alignment to apply
     */
    this.alignHeader = (axis, alignment) => {
        if (axis === Axis.X) {
            this.resetVerticalHeader();
            this.alignRow(0, alignment);
        } else {
            this.resetHorizontalHeader();
            alignColumnInternal(0, alignment);
        }
    }

    this.resetHorizontalHeader = () => {
        this.getRow(0)
            .forEach((cell, x) => this.alignCell(x, 0, alignments[x]));
    }

    this.resetVerticalHeader = () => {
        alignColumnInternal(0, alignments[0]);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const createHtmlCell = (x, y) => {
        const colIndex = computeColIndex(x);
        const rowIndex = computeRowIndex(y);
        const input = createHtmlInput(colIndex, rowIndex);
        inputMatrix.set(x, y, input);
    }

    const createHtmlInput = (x, y) => {
        const input = new Component('input')
            .addClass('spreadsheet-cell')
            .setId(x + '-' + y)
            .putStyle('padding', '5px 10px')
            .putStyle('textAlign', alignments[x].displayName)
            .addListener('focus', e => onHtmlInputFocus(e))
            .addListener('input', e => onHtmlInputValueChange(e), false)
            .addListener('keydown', e => delegateKeyEvent(e), false)
            .getUI();
        input.type = 'text';
        input.size = '10';
        return input;
    }

    const empty = (cols, rows) => {
        columnNameCells = [];
        rowNameCells = [];
        columnNames = SpreadSheetUtils.columnNames(cols);
        htmlColumnNamesRow = insertHtmlRow();
        const selectAllCell = htmlColumnNamesRow.insertCell();
        selectAllCell.classList.add('corner-cell');
        model = createModel(cols, rows);
        model.addStructureChangeListener(this);
        inputMatrix = new Matrix(cols, rows);
        inputMatrix.forEachColumn((col, x) => {
            insertColumnNameCell(x);
            alignments.push(Alignment.LEFT);
        });
        inputMatrix.forEachCoordinate((x, y) =>
            inputMatrix.set(x, y, createHtmlInput(x, y))
        );
        inputMatrix.addCellValueChangeListener(this);
        inputMatrix.forEachRow((row, y) => {
            const htmlRow = insertHtmlRow();
            insertRowNameCell(htmlRow, y);
            htmlTableRows.push(htmlRow);
            row.forEach((cell, x) => createHtmlCell(x, y));
        });
        updateInputs();
        this.focus();
    };

    const insertHtmlRow = (y = -1) => {
        return this.getUI().insertRow(y);
    }

    const insertRowNameCell = (htmlRow, index) => {
        const cell = htmlRow.insertCell();
        cell.innerText = index + 1;
        cell.classList.add('row-name');
        ArrayUtils.insert(rowNameCells, index, cell);
    }

    const insertColumnNameCell = (index) => {
        const cell = htmlColumnNamesRow.insertCell(computeDomIndex(index));
        cell.innerText = columnNames[index];
        cell.classList.add('column-name');
        ArrayUtils.insert(columnNameCells, index, cell);
        return cell;
    }

    const highlight = (index, active, axis) => {
        inputMatrix.forEachCell(input => input.classList.remove(HIGHLIGHTED));
        const cells = (axis === Axis.X) ? inputMatrix.getRow(index) : inputMatrix.getColumn(index);
        cells.forEach(input => {
            if (active) {
                input.classList.add(HIGHLIGHTED);
            }
        });
    }

    const updateInputs = () => {
        inputMatrix.forEachCoordinate((x, y) => {
            inputMatrix.get(x,y).id = x + '-' + y;
            inputMatrix.get(x,y).value = getValue(x, y);
        });
    }

    const updateRowNameCells = () => {
        rowNameCells.forEach((cell, y) => cell.innerText = y + 1);
    }

    const updateColumnNameCells = () => {
        columnNameCells.forEach((cell, x) => {
            cell.innerText = columnNames[x]
        });
    }

    const generateColumnNames = () => {
        columnNames = SpreadSheetUtils.columnNames(this.getWidth() + 1);
    }

    // Computes the DOM index for row name and column name cells
    const computeDomIndex = (index) => {
        return (index === -1) ? -1 : index + 1;
    }

    const onHtmlInputFocus = (e) => {
        lastCursor = cursor;
        cursor = toCoordinate(e);
        highlightRowAndColumnNames(cursor.getX(), cursor.getY());
    }

    const onHtmlInputValueChange = (e) => {
        Logger.verbose('Table', ' - onHtmlInputValueChange')
        const c = toCoordinate(e);
        const x = c.getX();
        const y = c.getY();
        const oldValue = getValue(x, y);
        const newValue = e.target.value;
        setValue(x, y, newValue);
        dispatchCellValueChangeEvent(new CellUpdate(x, y, oldValue, newValue));
        // cell user-friendly coordinate: (columnNames[x], y + 1)
    }

    const dispatchStructureChangeEvent = () => {
        structureChangeListeners.forEach(l =>
            l.onTableStructureChange(this.getModel())
        );
    }

    const dispatchCellValueChangeEvent = (cellUpdate) => {
        cellEventListeners.forEach(l => l.onCellValueChange(cellUpdate));
    }

    const dispatchActiveCellEvent = (input) => {
        const event = new ActiveCellEvent(parseCoordinate(input.id), toPhysicalCoordinate(input));
        cellEventListeners.forEach(l => l.onCellActive(event));
    }

    const delegateKeyEvent = (evt) => {
        keyEventListeners.forEach(listener => listener.processKeyEvent(evt, toCoordinate(evt)));
    }

    const highlightRowAndColumnNames = (x, y) => {
        const cssClass = 'active';
        if (lastCursor != null) {
            const cells = [];
            pushIfNotNull(cells, rowNameCells, lastCursor.getY());
            pushIfNotNull(cells, columnNameCells, lastCursor.getX());
            cells.forEach(cell => cell.classList.remove(cssClass));
        }
        rowNameCells[y].classList.add(cssClass);
        columnNameCells[x].classList.add(cssClass);
        function pushIfNotNull(target, source, index) {
            if (source[index] != null) {
                target.push(source[index]);
            }
        }
    }

    const setInternal = (x, y, value) => {
        const rowIndex = computeRowIndex(y);
        const colIndex = computeColIndex(x);
        const oldValue = getValue(colIndex, rowIndex);
        setValue(colIndex, rowIndex, value);
        inputMatrix.get(colIndex, rowIndex).value = value;
        return oldValue;
    }

    const setValue = (x, y, value) => {
        getCell(x, y).setValue(value);
    }

    const getValue = (x, y) => {
        return getCell(x, y).getValue();
    }

    const getCell = (x, y) => {
        return model.get(x, y);
    }

    const alignColumnInternal = (index, alignment) => {
        this.getColumn(index)
            .forEach((cell, y) => this.alignCell(index, y, alignment));
    }

    const toCellArray = (arr, axis) => {
        if (arr == null) {
            return ArrayUtils.range(0, this.getLength(axis)).map(ignored => new Cell());
        }
        return arr.map(value => new Cell(value));
    }

    /** A {@link Matrix} of {@link Cell} */
    function createModel(w, h) {
        return new Matrix(w, h).map(ignored => new Cell());
    }

    /**
     * Returns the logical coordinates of the cell which originated this event.
     *
     * @param evt  the event to process
     */
    const toCoordinate = (evt) => {
        return parseCoordinate(evt.target.id);
    }

    /**
     * Returns the physical coordinates (in pixels) of the cell which originated
     * this event.
     *
     * @param htmlInput  the cell which originated this event
     */
    const toPhysicalCoordinate = (htmlInput) => {
        const rect = htmlInput.getBoundingClientRect();
        return new Vector(rect.left, rect.top);
    }

    const parseCoordinate = (subject) => {
        const c = subject
            .split("-")
            .map(e => parseInt(e));
        return new Vector(c[0], c[1]);
    }

    const toModelData = () => {
        return model.map(cell => cell.getValue());
    }

    const computeRowIndex = (index) => {
        return computeIndex(index, Axis.Y);
    }

    const computeColIndex = (index) => {
        return computeIndex(index, Axis.X);
    }

    const computeIndex = (index, axis) => {
        return (index < 0) ? this.getLength(axis) + index : index;
    }

    const init = () => {
        empty(cols, rows);
    }

    init();

};
