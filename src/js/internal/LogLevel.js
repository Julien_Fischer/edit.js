/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * Lists the log levels supported by the {@link Logger}.
 *
 * Note that DEBUG, VERBOSE and TRACE levels are automatically disabled if
 * {@link editjs#PRODUCTION} is true.
 *
 * @see Logger
 */
const LogLevel = Object.freeze({

    /** For errors which cause the current activity to fail */
    ERROR:   {name: 'ERROR',   severity: 4},

    /** Suitable for tracking the flow of the application */
    INFO:    {name: 'INFO',    severity: 3},

    /** Suitable for investigation during development. */
    DEBUG:   {name: 'DEBUG',   severity: 2},

    /** Suitable for tracking the low-level flow of the application */
    VERBOSE: {name: 'VERBOSE', severity: 1},

    /** Will dump the method call stack trace each time a message is called with this log level */
    TRACE:   {name: 'TRACE',   severity: 0}

});
