/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const TestRunner = function() {

    const listeners = [];
    // State
    let metrics;
    // Cache
    let testsToRun;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.addRunnerListener = (listener) => {
        listeners.push(listener);
    }

    /**
     * Executes the specified unit tests with specified settings.
     *
     * @param unitTestsList  an array of {@link TestSet}
     * @param settings  the settings to apply when running the tests.
     * {@code settings.gui} will display the results as an interactive web-page.
     * {@code settings.debug} will stop tests execution if an exception is thrown
     */
    this.execute = (unitTestsList, settings) => {
        setConfiguration(unitTestsList, settings);
        dispatchRunnerStartEvent(settings, metrics);
        testsToRun.forEach(unitTests => runTestSet(unitTests, settings.debug));
        dispatchRunnerEndEvent(metrics);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const setConfiguration = (unitTestsList, settings) => {
        Logger.setLevel(settings.logLevel);
        Logger.setProduction(false);
        if (settings.gui) {
            this.addRunnerListener(new Dashboard());
        }
        testsToRun = unitTestsList;
        metrics = new Metrics(getTotalCount());
    }

    const runTestSet = (unitTests, debug) => {
        const className = unitTests.getSimpleName();
        let i = 1;
        const separator = StringUtils.repeat('-', 35);
        log(separator);
        log('Testing class: ' + unitTests.getName());
        log(separator);
        dispatchTestSetStartEvent(className, unitTests.getPackageName());
        unitTests.forEachTest((testName, packageName, testMethod, context) => {
            const testNumber = i + '/' + unitTests.getCount();
            runTest(context, testMethod, testName, packageName, testNumber, debug);
            i++;
        });
        dispatchTestSetCompleteEvent(className);
    };

    /**
     * Runs this test method using the specified context.
     *
     * @param {Object.<any, any>} context  the test context which may be used to store and retrieve
     * data for the lifetime or the {@link TestSet} set
     * @param {function} testMethod  the test method to run
     * @param {string} testName  the test method name
     * @param {string} packageName  the test method package
     * @param {number} testNumber  the test number
     * @param {boolean} debug  true if debug mode, false otherwise
     */
    const runTest = (context, testMethod, testName, packageName, testNumber, debug) => {
        const separator = ' - ';
        log(testNumber + separator + 'Running test: ', testName);
        const startTime = Date.now();
        let error = null;
        try {
            testMethod(context);
        } catch (err) {
            error = err;
        }
        const success = (error === null);
        const duration = Date.now() - startTime;
        const result = new TestResult(testName, packageName, error, duration);
        const testStatus = success ? 'Passed' : 'Failed';
        metrics.update(result);
        dispatchTestCompleteEvent(testMethod, result);
        info(testNumber + separator + testStatus);
        log('****');
        if (!success && debug) {
            Logger.verbose(metrics.getTotalCount())
            dispatchRunnerEndEvent(metrics);
            throw error;
        }
    }

    const getTotalCount = () => {
        return testsToRun
            .map(set => set.getCount())
            .reduce((prev, next) => prev + next, 0);
    }

    const dispatchRunnerStartEvent = (settings, metrics) => {
        listeners.forEach(l => l.onRunnerStart(settings, metrics));
    }

    const dispatchTestCompleteEvent = (testName, test, result) => {
        listeners.forEach(l => l.onTestComplete(testName, test, result));
    };

    const dispatchTestSetStartEvent = (className, packageName) => {
        listeners.forEach(l => l.onTestSetStart(className, packageName));
    }

    const dispatchTestSetCompleteEvent = (className) => {
        listeners.forEach(l => l.onTestSetComplete(className));
    }

    const dispatchRunnerEndEvent = (metrics) => {
        listeners.forEach(l => l.onRunnerEnd(metrics));
    }

    const log = (...data) => {
        console.log(...data);
    }

    const info = (...data) => {
        console.info(...data);
    }

}
