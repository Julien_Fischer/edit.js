/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

function assertThrows(callback) {
    try {
        callback();
    } catch (error) {
        return;
    }
    fail('assertThrows', 'Exception not thrown when an error was expected for this test');
}

function assertTrue(actual) {
    if (!actual) {
        fail('assertTrue', 'Received ' + actual + ' when true was expected');
    }
}

function assertFalse(actual) {
    if (actual) {
        fail('assertFalse', 'Received ' + actual + ' when false was expected');
    }
}

function assertNull(actual) {
    if (actual != null) {
        fail('assertNull', 'Received ' + actual + ' when null was expected');
    }
}

function assertNotNull(actual) {
    if (actual == null) {
        fail('assertNotNull', 'Received ' + actual + ' when not null was expected');
    }
}

function assertArrayEquals(expected, actual) {
    if (!isArray(expected) || !isArray(actual)) {
        fail('assertArrayEquals', 'Specified element is not an array');
    }
    if (expected.length !== actual.length) {
        const message = 'Arrays differ in length ' +
            '(' + actual.length + ' instead of ' + expected.length + '): ' +
            '\nexpected=' + expected + '\nactual=  ' + actual;
        fail('assertArrayEquals', message);
    }
    for (let i = 0; i < expected.length; i++) {
        if (JSON.stringify(expected[i]) !== JSON.stringify(actual[i])) {
            fail('assertArrayEquals', 'Element differ at index ' + i + '. ' + expected[i] + " !== " + actual[i]);
        }
    }
}

function fail(methodName, message) {
    const msg = methodName + ' failed: ' + message;
    Logger.verbose(msg);
    throw new Error(msg);
}

function assertNotEquals(expected, actual) {
    try {
        assertEquals(expected, actual);
        fail('assertNotEquals', 'Elements are equal');
    } catch (error) {
        // Elements are not equal; nothing to do here
    }
}

/**
 * Note:
 * If the parameters are objects, only the public members will be compared
 * (i.e. members declared with {@code this} keyword). Other members will
 * not be automatically visible by this method.
 *
 * @param expected
 * @param actual
 * @param msg
 */
function assertEquals(expected, actual, msg = '') {
    if (
        (expected == null && actual != null) ||
        (expected != null && actual == null)
    )  {
        fail('assertEquals', 'null value when comparing expected = ' + expected + ' and actual: ' + actual);
    }
    if (typeof expected !== typeof actual) {
        fail('assertEquals', 'elements differ in type: expected = ' + expected + ' and actual: ' + actual);
    }
    if (expected == null) {
        return;
    }

    if (isArray(expected) || isArray(actual)) {
        assertArrayEquals(expected, actual);
        return;
    }

    switch (typeof expected) {
        case 'object':
            assertObjectEquals(expected, actual);
            return;
        case 'function':
            assertValueEquals('' + expected, '' + actual);
            return;
    }

    assertValueEquals(expected, actual);

    function assertValueEquals(expected, actual) {
        if (expected !== actual) {
            fail('assertEquals', expected + ' !== ' + actual + '\n' + msg);
        }
    }
}

function assertObjectEquals(expected, actual) {
    if (isFunction(expected['equals'])) {
        if (!expected.equals(actual)) {
            fail('assertEquals', expected + ' !== ' + actual);
        }
        return;
    }
    for (const key in expected) {
        const message = 'elements differ with property named "' + key + '". ' +
            '\nExpected:\n' + key + ' = ' + expected[key] + '\nActual:\n' + key + ' = ' + actual[key]
        assertEquals(expected[key], actual[key], message);
    }
}

