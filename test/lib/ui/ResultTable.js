/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A {@link Component} rendering the results of a {@link TestSet} set.
 *
 * @param {string} title  the table title
 * @param {string} packageName  the name of the test package
 */
const ResultTable = function(title, packageName) {

    Component.call(this);

    const DOWN_ARROW = '˅';
    const UP_ARROW = '˄';

    let listeners = [];
    let expandedStateListener = [];
    // State
    const elements = [];
    let expanded;
    let successCount = 0;
    let errorCount = 0;
    // UI
    let titleContainer;
    let detailContainer;
    let toggleArrow;
    let toggleLabel;
    let htmlTable;
    let htmlTableBody;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.toggle = () => {
        this.setExpanded(!expanded);
        expandedStateListener.forEach(e => e.onExpandedStateChange(expanded));
    }

    this.setExpanded = (expand) => {
        htmlTable.style.display = expand ? 'table' : 'none';
        toggleLabel.setText(      expand ? 'Fold' : 'Expand');
        toggleArrow.setText(      expand ? UP_ARROW : DOWN_ARROW);
        expanded = expand;
    }

    this.isExpanded = () => {
        return expanded;
    }

    this.append = (test, testResult) => {
        const row = htmlTableBody.insertRow();
        const nameCell = row.insertCell();
        const durationCell = row.insertCell();
        const statusCell = row.insertCell();
        const errorCell = row.insertCell();
        nameCell.innerText = testResult.getName()
        durationCell.innerText = testResult.getDuration();
        statusCell.innerText = testResult.hasFailed() ? 'Error' : 'Success';
        statusCell.style.color = testResult.hasFailed() ? 'red' : 'green';
        errorCell.style.color = testResult.hasFailed() ? 'red' : 'green';
        const preformatted = new Component('pre')
            .putStyle('fontFamily', 'monospace, monospace')
            .setText(testResult.getError() || 'None');
        errorCell.appendChild(preformatted.getUI());
        row.addEventListener('click', () => onRowClick(test, testResult), false);
        elements.push({ui: row, result: testResult});
        updateDetail(testResult);
    };

    this.filterByName = (name) => {
        this.filter(e => e.result.getName().toLowerCase().match(name));
    }

    this.filterByStatus = (success) => {
        this.filter(e => retain(e, success));
    }

    this.filter = (predicate) => {
        const filtered = elements.filter(predicate);
        if (filtered.length === 0) {
            this.setVisible(false);
        } else {
            this.setVisible(true);
            elements.forEach(e => {
                if (predicate(e)) {
                    e.ui.classList.remove('hidden');
                } else {
                    e.ui.classList.add('hidden');
                }
            });
        }
    }

    this.addRowClickListener = (listener) => {
        listeners.push(listener);
    }

    this.addExpandedStateListener = (listener) => {
        expandedStateListener.push(listener);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const updateDetail = (result) => {
        if (result.hasFailed()) {
            errorCount++;
        } else {
            successCount++;
        }
        const tag = new Tag(packageName, TagType.PLAIN, 'package-name')
            .addClass('package-' + packageName)
        const titleContainer = new Component('span')
            .putStyle('marginLeft', '10px')
            .setText(title);
        const html = ' -'
            + ' Passed: ' + StyleUtils.colorizeSuccess(successCount, 'metrics-value')
            + ' Failed: ' + StyleUtils.colorizeError(errorCount, 'metrics-value');
        const detailWrapper = new Component('span')
            .setHtml(html);
        detailContainer
            .clearNodes()
            .add(tag)
            .add(titleContainer)
            .add(detailWrapper);
    }

    const retain = (e, success) => {
        switch (success) {
            case true:
                return !e.result.hasFailed();
            case false:
                return e.result.hasFailed();
            default:
                return true;
        }
    }

    const init = () => {
        toggleLabel = new Component('span')
            .addClass('table-toggle-label');
        toggleArrow = new Component('span')
            .addClass('table-toggle-arrow')
            .setText('Expand ' + DOWN_ARROW);
        const expandBtn = new Component()
            .addClass('table-toggle-button')
            .add(toggleLabel)
            .add(toggleArrow);
        detailContainer = new Component()
            .addClass('table-detail')
            .setText(title + ' tests');
        titleContainer = new Component()
            .addListener('click', this.toggle, false)
            .addClass('table-title')
            .add(detailContainer)
            .add(expandBtn);

        htmlTable = document.createElement('TABLE');
        const header = htmlTable.createTHead();
        const headingsRow = header.insertRow();
        insertHeaderCell(headingsRow, 'Test', 70);
        insertHeaderCell(headingsRow, 'ms');
        insertHeaderCell(headingsRow, 'Status');
        insertHeaderCell(headingsRow, 'Error', -1, true);
        htmlTableBody = document.createElement('TBODY');
        htmlTable.appendChild(htmlTableBody);
        headingsRow.classList.add('header');
        this.add(titleContainer);
        this.addNode(htmlTable);
        this.addClass('result-table');
        this.setExpanded(false);
    };

    const insertHeaderCell = (container, text, width = -1, truncate) => {
        const cell = document.createElement('TH');
        cell.innerText = text;
        if (width > 0) {
            cell.style.width = width + '%';
        }
        if (truncate) {
            cell.classList.add('truncate');
        }
        container.appendChild(cell);
    }

    const onRowClick = (test, result) => {
        listeners.forEach(l => l.onRowClick(test, result));
    }

    init();

};
