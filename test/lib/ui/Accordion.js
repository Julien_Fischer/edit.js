/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A {@link Component} containing a list of elements which can be folded or expanded
 * either one at a time or in bulk.
 */
const Accordion = function() {

    Component.call(this);

    // State
    const elements = [];
    // Config
    let codeRequestCallback = () => {};

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.setExpanded = (expand) => {
        elements.forEach(e => e.setExpanded(expand));
    }

    this.onExpandedStateChange = (expanded) => {
        if (expanded) {
            const foldedCount = elements.filter(e => !e.isExpanded()).length;
            if (foldedCount === 0) {
                this.setExpanded(true);
            }
        } else {
            const expandedCount = elements.filter(e => e.isExpanded()).length;
            if (expandedCount === 0) {
                this.setExpanded(false);
            }
        }
    }

    this.append = (title, packageName) => {
        const resultTable = new ResultTable(title, packageName);
        resultTable.addRowClickListener(this);
        resultTable.addExpandedStateListener(this);
        elements.push(resultTable);
        this.add(resultTable);
    };

    this.filterByStatus = (success) => {
        elements.forEach(e => e.filterByStatus(success));
    }

    this.filterByName = (name) => {
        const lowerCased = name.toLowerCase();
        elements.forEach(e => e.filterByName(lowerCased));
    }

    this.onRowClick = (test, result) => {
        codeRequestCallback(test, result);
    }

    this.onCodeRequest = (callback) => {
        codeRequestCallback = callback;
        return this;
    }

    this.getLastElement = () => {
        return ArrayUtils.getLast(elements);
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        this.addClass('accordion');
    };

    init();

};
