/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const StyleUtils = {

    colorize: (subject, ...classes) => {
        return `<span class="code ${classes.join(' ')}">${subject}</span>`;
    },

    colorizeSuccess: (count, cssClass = '') => {
        return StyleUtils.colorize(count, StyleUtils.computeSuccessStatus(count), cssClass);
    },

    colorizeError: (count, cssClass = '') => {
        return StyleUtils.colorize(count, StyleUtils.computeErrorStatus(count), cssClass);
    },

    colorizeWarning: (count, cssClass = '') => {
        return StyleUtils.colorize(count, StyleUtils.computeWarnStatus(count), cssClass);
    },

    computeSuccessStatus: (count) => {
        return StyleUtils.computeStatus(count, Status.SUCCESS, Status.ERROR);
    },

    computeErrorStatus: (count) => {
        return StyleUtils.computeStatus(count, Status.ERROR, Status.SUCCESS);
    },

    computeWarnStatus: (count) => {
        return StyleUtils.computeStatus(count, Status.WARN, Status.SUCCESS);
    },

    computeStatus: (count, status, fallback) => {
        return (count > 0) ? status: fallback;
    }

}
