/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const CodeViewer = function() {

    Component.call(this);

    let snippet;
    let titleBar;
    let button;
    let statusContainer;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.close = () => {
        this.addClass('closed');
    };

    this.display = (test, result) => {
        const escaped = StringUtils.escapeHTMLChars(test.toString());
        const reg = /\{(.*[\s\S]*)\}/;
        const code = escaped.match(reg)[1];
        snippet.display(code);
        const status = new Component()
            .addClass('code-viewer-status')
            .addClass((result.hasFailed()) ? 'error' : 'success')
            .setText(result.hasFailed() ? 'Failed' : 'Passed');
        titleBar.setText(result.getName());
        statusContainer.clearNodes();
        statusContainer.add(status);
        this.removeClass('closed');
    };

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        snippet = new Snippet();
        titleBar = new Component()
            .addClass('code-viewer-title');
        button = new CloseButton('close', this.close);
        statusContainer = new Component()
            .addClass('code-viewer-status-container')
        const topBar = new Component()
            .addClass('code-viewer-topbar')
            .add(titleBar)
            .add(statusContainer)
            .add(button);
        this.addClass('code-viewer')
            .addClass('bottom')
            .addClass('closed')
            .add(topBar)
            .add(snippet);
        GUI.dockable(topBar.getUI(), this);
        document.addEventListener('keyup', e => {
            if (e.key === Key.ESCAPE) {
                this.close();
            }
        }, false);
    }

    init();

};
