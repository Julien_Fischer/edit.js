/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Donut = function(thickness = 40, size = 80) {

    Component.call(this);

    // Constants cache
    const NAMESPACE = 'http://www.w3.org/2000/svg';
    const ONE_HUNDRED = (size - thickness) * 3;
    const TWENTY_FIVE = ONE_HUNDRED / 4;
    const RADIUS = ONE_HUNDRED / (2 * Math.PI);
    // UI
    let svg;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.getNamespace = () => { return NAMESPACE; }

    this.getOneHundred = () => { return ONE_HUNDRED; }

    this.getTwentyFive = () => { return TWENTY_FIVE; }

    this.getRadius = () => { return RADIUS; }

    this.getThickness = () => { return thickness; }

    this.getSize = () => { return size; }

    this.getSvg = () => { return svg; }

    this.computeRatio = (value) => { return (value / 100 * this.getOneHundred()); }

    this.computeDashArray = (value) => { return value + ' ' + (this.getOneHundred() - value); }

    this.computeDashOffset = (value) => { return value + this.getTwentyFive(); }

    this.createSvgElement = () => {
        const svg = document.createElementNS(NAMESPACE, "svg");
        svg.setAttribute('width', '' + size);
        svg.setAttribute('height', '' + size);
        return svg;
    }

    this.createCircle = (x, y, radius, color, dashArray, dashOffset) => {
        const circle = document.createElementNS(this.getNamespace(), 'circle');
        circle.setAttribute('cx', x + radius + this.getThickness() / 2);
        circle.setAttribute('cy', y + radius + this.getThickness() / 2);
        circle.setAttribute('r', radius);
        circle.setAttribute('fill', 'transparent');
        circle.setAttribute('stroke', color);
        circle.setAttribute('stroke-width', '' + this.getThickness());
        circle.setAttribute('stroke-dasharray', dashArray);
        circle.setAttribute('stroke-dashoffset', '' + dashOffset);
        return circle;
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const init = () => {
        svg = this.createSvgElement();
        this.addNode(svg);
    }

    init();

}
