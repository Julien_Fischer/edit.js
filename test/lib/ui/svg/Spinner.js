/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Spinner = function(thickness = 15, size = 150) {

    Donut.call(this, thickness, size);

    // UI
    let svgCircle;
    let progress;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Updates the spinner circle completion based on the specified value.
     *
     * @param value  an integer between 0 (inclusive) and 100 (inclusive)
     */
    this.update = (value) => {
        const ratio = this.computeRatio(value);
        const dashArray = this.computeDashArray(ratio);
        const dashOffset = this.computeDashOffset(ratio);
        svgCircle.setAttribute('stroke-dasharray', dashArray);
        svgCircle.setAttribute('stroke-dashoffset', '' + dashOffset);
        progress.setText(value.toFixed(1) + '%');
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const createSpinner = () => {
        svgCircle = this.createCircle(0, 0, this.getRadius(), '#00aeff', 0, 0);
        return svgCircle;
    }

    const createProgress = () => {
        return new Component()
            .addClass('progress');
    }

    const init = () => {
        progress = createProgress();
        this.getSvg().appendChild(createSpinner());
        this.update(0);
        this.addClass('spinner')
            .putStyle('marginTop', -this.getSize() / 2 + 'px')
            .putStyle('marginLeft', -this.getSize() / 2 + 'px')
            .add(progress);
    }

    init();

}
