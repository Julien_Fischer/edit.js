/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A {@link Component} containing the details of the test results and the global metrics.
 * Also provides controls for filtering the test results.
 */
const Dashboard = function() {

    Component.call(this);

    // Dependencies
    let metricsContainer;
    let controls;
    let codeViewer;
    let accordion;
    let loadingScreen;
    let spinner;
    // Cache
    let metricsCache;
    let executed = 0;
    let hasError = false; // no error

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    this.onRunnerStart = (settings, metrics) => {
        metricsCache = metrics;
        loadingScreen.setVisible(true);
    }

    this.onTestSetStart = (className, packageName) => {
        accordion.append(className, packageName);
    };

    this.onTestComplete = (test, result) => {
        const resultTable = accordion.getLastElement();
        resultTable.append(test, result);
        executed++;
        const progress = executed / metricsCache.getTotalCount() * 100;
        spinner.update(progress);
        storeResultState(result.hasFailed());
    };

    this.onTestSetComplete = (className) => {
        // nothing to do yet
    };

    this.onRunnerEnd = (metrics) => {
        metricsContainer.update(metrics);
        updateControls(metrics);
        loadingScreen.setVisible(false);
        updateFavicon();
    }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const storeResultState = (failed) => {
        if (!hasError) {
            hasError = failed;
        }
    }

    const updateFavicon = () => {
        const link = document.querySelector("link[rel~='icon']");
        const status = hasError ? '-error' : '-success';
        if (link) {
            link.href = '../src/assets/branding/favicon-tests' + status + '.png';
        }
    }

    const updateControls = (metrics) => {
        controls.setMetrics(metrics);
    }

    const createLoadingScreen = () => {
        spinner = new Spinner();
        loadingScreen = new Component()
            .addClass('loading-screen')
            .add(spinner);
        return loadingScreen;
    }

    const createDockArea = () => {
        return new Component()
            .setId('dock-area')
            .addClass('dockable');
    }

    const createCodeViewer = () => {
        codeViewer = new CodeViewer();
        return codeViewer;
    }

    const init = () => {
        metricsContainer = new MetricsContainer();
        accordion = new Accordion()
            .onCodeRequest((test, result) => codeViewer.display(test, result));
        controls = new DashboardControls()
            .onFilterByStatus(success => accordion.filterByStatus(success))
            .onFilterByName(name => accordion.filterByName(name))
            .onExpand(expand => accordion.setExpanded(expand));
        const summary = new Component()
            .addClass('summary')
            .add(metricsContainer)
            .add(controls);
        this.setUI(document.getElementById('dashboard'))
            .add(createLoadingScreen())
            .add(createDockArea())
            .add(summary)
            .add(accordion)
            .add(createCodeViewer());
    }

    init();

}
