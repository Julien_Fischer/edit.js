/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const Metrics = function(total) {

    let totalTime = 0;
    let totalSuccessCount = 0;
    let totalErrorCount = 0;
    let totalRemainingCount = total;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    /**
     * Updates the metrics using this {@link TestResult}.
     *
     * @param result  a {@link TestResult} instance
     */
    this.update = (result) => {
        totalTime += result.getDuration();
        if (result.hasFailed()) {
            totalErrorCount++;
        } else {
            totalSuccessCount++;
        }
        if (totalRemainingCount === 0) {
            throw new Error('Invalid metrics update request: total remaining count is zero');
        }
        totalRemainingCount--;
    }

    this.getTotalSuccessCount = () => { return totalSuccessCount; }

    this.getTotalErrorCount = () => { return totalErrorCount; }

    this.getTotalRemainingCount = () => { return totalRemainingCount; }

    this.getTotalExecutedCount = () => { return totalSuccessCount + totalErrorCount; }

    this.getTotalCount = () => { return total ; }

    this.getTotalTime = () => { return totalTime; }

    this.toString = () => {
        return 'Metrics:' +
            '\ntotalTime: ' + totalTime +
            '\ntotalSuccessCount: ' + totalSuccessCount +
            '\ntotalErrorCount: ' + totalErrorCount +
            '\ntotalRemainingCount: ' + totalRemainingCount;
    }

}
