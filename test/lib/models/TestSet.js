/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/**
 * A set of unit tests supporting fixture and cleanup methods.
 *
 * @param {string} name  the name of the class to test.
 *                       If the name is dot-separated, the left side specifies the package name,
 *                       while the right side specifies the test name
 */
const TestSet = function(name) {

    // This context is available for each unit test or fixture method in this set
    // to store or retrieve data
    const context = {};

    let setupMethod = () => {};
    let fixtureMethod = () => {};
    let cleanupMethod = () => {};
    let testsToRun = {};

    let simpleName;
    let packageName;

    //////////////////////////////////////////////////////////////////
    // API
    //////////////////////////////////////////////////////////////////

    // Client-facing methods

    /**
     * Registers a setup method which will be called only once, before any unit in this set
     * test is executed.
     *
     * @param fn  the setup method to register
     * @return this object
     */
    this.beforeAll = (fn) => {
        setupMethod = fn;
        return this;
    }

    /**
     * Registers a setup method which will be called before every unit test in this set
     * is executed.
     *
     * @param fn  the fixture method to register
     * @return this object
     */
    this.beforeEach = (fn) => {
        fixtureMethod = fn;
        return this;
    }

    /**
     * Registers a cleanup method which will be called after every unit test in this set
     * has been executed.
     *
     * @param fn  the fixture method to register
     * @return this object
     */
    this.afterAll = (fn) => {
        cleanupMethod = fn;
        return this;
    }

    /**
     * Expects an object with functions as public members
     * (e.g. an object literal or an instance with public members).
     *
     * @param tests  the tests to run
     * @return this object
     */
    this.run = (tests) => {
        testsToRun = tests;
        return this;
    }

    // Library-facing methods
    // These methods should not be called or overridden by clients

    this.forEachTest = (fn) => {
        callBeforeAll();
        ObjectUtils.forEach(testsToRun, (testName, testMethod) => {
            callBeforeEach();
            fn(testName, this.getPackageName(), testMethod, context);
        });
        callAfterAll();
    }

    this.getName = () => { return name; }

    this.getSimpleName = () => { return simpleName; }

    this.getPackageName = () => { return packageName; }

    this.getCount = () => {
        return Object.values(testsToRun).length;
    }

    this.getSetupMethod = () => { return setupMethod; }

    this.getFixtureMethod = () => { return fixtureMethod; }

    //////////////////////////////////////////////////////////////////
    // Internal
    //////////////////////////////////////////////////////////////////

    const callBeforeAll = () => { setupMethod(context); }

    const callBeforeEach = () => { fixtureMethod(context); }

    const callAfterAll = () => { cleanupMethod(); }

    const init = () => {
        const split = name.split(/\./);
        if (split.length > 1) {
            packageName = split[0];
            simpleName = split[1];
        } else {
            simpleName = name;
        }
    }

    init();

}
