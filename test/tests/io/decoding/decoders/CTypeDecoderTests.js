/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.CTypeDecoder')

    .beforeAll(ctx => {
        ctx.javaDecoder = new CTypeDecoder(Format.JAVA);
        ctx.cSharpDecoder = new CTypeDecoder(Format.C_SHARP);
        ctx.phpDecoder = new CTypeDecoder(Format.PHP);
        ctx.jsDecoder = new CTypeDecoder(Format.JAVASCRIPT);
        ctx.pythonDecoder = new CTypeDecoder(Format.PYTHON);
        ctx.data = [
            ['A', 'B', 'C'],
            ['D', 'E', 'F']
        ];
        ctx.javaPrettifiedArray = `
            String[][] arr = {
            
                {"A", "B", "C"},
                
                {"D", "E", "F"}
            };
        `;
        ctx.javaMinifiedArray = 'String[][] arr = {{"A", "B", "C"}, {"D", "E", "F"}};';
        ctx.javaMinifiedList = 'List<List<String>> arr = List.of(List.of("A", "B", "C"), List.of("D", "E", "F"));';
        ctx.javaPrettifiedList = `
            List<List<String>> arr = List.of(
                List.of("A", "B", "C"),
                List.of("D", "E", "F")
            );
        `;
        ctx.cSharpPrettified2dArray = `
            string[,] arr = {
                {"A", "B", "C"},
                {"D", "E", "F"}
            };
        `;
        ctx.cSharpPrettifiedJaggedArray = `
            string[][] arr = {
                new string[] {"A", "B", "C"},
                new string[] {"D", "E", "F"}
            };
        `;
        ctx.data2 = [
            ['A', 'B', 'C'],
            ['D', 'E', 'F'],
            ['G', 'H', 'I']
        ];
        ctx.phpPrettified2dArray = `
            $arr = array(
                array("A", "B", "C"),
                array("D", "E", "F"),
                array("G", "H", "I")
            );
        `;
        ctx.phpPrettifiedAssociativeArray = `
            expected = [
                $arr = array(
                    array(
                        "A" => "D",
                        "B" => "E",
                        "C" => "F"
                    ),
                    array(
                        "A" => "G",
                        "B" => "H",
                        "C" => "I"
                    )
                );
            ]
        `;
        ctx.javaScriptPrettifiedArray = `
            [
                ['A', 'B', 'C'],
                ['D', 'E', 'F'],
                ['G', 'H', 'I']
            ];
        `;
        ctx.javaScriptPrettifiedAssociativeArray = `
            [
                {
                    A: 'D',
                    B: 'E',
                    C: 'F'
                },
                {
                    A: 'G',
                    B: 'H',
                    C: 'I'
                }
            ];
        `;
        ctx.pythonPrettifiedArray = `
            arr = [
                ['A', 'B', 'C'],
                ['D', 'E', 'F'],
                ['G', 'H', 'I']
            ]
        `;
        ctx.pythonPrettifiedAssociativeArray = `
            arr = [
                {
                    'A': 'D',
                    'B': 'E',
                    'C': 'F'
                },
                {
                    'A': 'G',
                    'B': 'H',
                    'C': 'I'
                }
            ]
        `;
    })

    .run({
        supports_whenJava2dArray_shouldReturnFormatJava: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.javaPrettifiedArray);
            // Then
            assertEquals(Format.JAVA, actual);
        },

        supports_whenJava2dList_shouldReturnFormatJava: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.javaPrettifiedList);
            // Then
            assertEquals(Format.JAVA, actual);
        },

        supports_whenCSharp2dArray_shouldReturnFormatCSharp: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.cSharpPrettified2dArray);
            // Then
            assertEquals(Format.C_SHARP, actual);
        },

        supports_whenCSharpJaggedArray_shouldReturnFormatCSharp: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.cSharpPrettifiedJaggedArray);
            // Then
            assertEquals(Format.C_SHARP, actual);
        },

        supports_whenPhp2dArray_shouldReturnFormatPhp: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.phpPrettified2dArray);
            // Then
            assertEquals(Format.PHP, actual);
        },

        supports_whenPhpAssociativeArrays_shouldReturnFormatPhp: (ctx) => {
            // When
            const actual = ctx.javaDecoder.supports(ctx.phpPrettifiedAssociativeArray);
            // Then
            assertEquals(Format.PHP, actual);
        },

        supports_whenJs2dArray_shouldReturnFormatJavaScript: (ctx) => {
            // When
            const actual = ctx.jsDecoder.supports(ctx.javaScriptPrettifiedArray);
            // Then
            assertEquals(Format.JAVASCRIPT, actual);
        },

        supports_whenJsAssociativeArrays_shouldReturnFormatJavaScript: (ctx) => {
            // When
            const actual = ctx.jsDecoder.supports(ctx.javaScriptPrettifiedAssociativeArray);
            // Then
            assertEquals(Format.JAVASCRIPT, actual);
        },

        supports_whenInvalidJavaFormat_shouldReturnNull: (ctx) => {
            // Given
            const input = `
                List<List<>> arr = List.of(
                    List.of("A", "B", "C"),
                    List.of("D", "E", "F")
                );
            `;
            // When
            const actual = ctx.javaDecoder.supports(input);
            // Then
            assertNull(actual);
        },

        decode_whenJavaAndPrettifiedArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.javaDecoder.decode(ctx.javaPrettifiedArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.ARRAY_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenJavaAndMinifiedArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.javaDecoder.decode(ctx.javaMinifiedArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.ARRAY_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.MINIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenJavaAndPrettifiedList_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.javaDecoder.decode(ctx.javaPrettifiedList);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.LIST_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenJavaAndMinifiedList_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.javaDecoder.decode(ctx.javaMinifiedList);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.LIST_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.MINIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenJavaAndArrayAndContainsHTMLCharacters_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const input = `
                String[][] arr = {
                    {"Aerzer", "<a>B", "C"},
                    {"D", "E", "F"}
                };
            `;
            const data = [
                ['Aerzer', '<a>B', 'C'],
                ['D',      'E',    'F']
            ];
            const actual = ctx.javaDecoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.ARRAY_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(data, model.getData());
        },

        decode_whenJavaAndListAndContainsHTMLCharacters_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const input = `
                List<List<String>> arr = List.of(
                    List.of("Aerzer", "<a>B", "C"),
                    List.of("D", "E", "F")
                );
            `;
            const data = [
                ['Aerzer', '<a>B', 'C'],
                ['D',      'E',    'F']
            ];
            const actual = ctx.javaDecoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JavaFormat.LIST_2D, metadata.getJavaFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(data, model.getData());
        },

        decode_whenCSharpAndPrettified2dArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.cSharpDecoder.decode(ctx.cSharpPrettified2dArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(CSharpFormat.ARRAY_2D, metadata.getCSharpFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenCSharpAndPrettifiedJaggedArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.cSharpDecoder.decode(ctx.cSharpPrettifiedJaggedArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(CSharpFormat.ARRAY_JAGGED, metadata.getCSharpFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data, model.getData());
        },

        decode_whenPhpAndPrettified2Array_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.phpDecoder.decode(ctx.phpPrettified2dArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(PhpFormat.ARRAY_2D, metadata.getPhpFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

        decode_whenPhpAndPrettifiedAssociativeArrays_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.phpDecoder.decode(ctx.phpPrettifiedAssociativeArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(PhpFormat.ASSOCIATIVE_2D, metadata.getPhpFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

        decode_whenJavaScriptAndPrettified2dArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.jsDecoder.decode(ctx.javaScriptPrettifiedArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JsonFormat.ARRAY_2D, metadata.getJsonFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

        decode_whenJavaScriptAndPrettifiedAssociativeArrays_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.jsDecoder.decode(ctx.javaScriptPrettifiedAssociativeArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(JsonFormat.ASSOCIATIVE_2D, metadata.getJsonFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

        /* PYTHON */

        decode_whenPythonAndPrettified2dArray_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.pythonDecoder.decode(ctx.pythonPrettifiedArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(PythonFormat.ARRAY_2D, metadata.getPythonFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

        decode_whenPythonAndPrettifiedArrayOfDictionaries_shouldReturnCorrectTableModel: (ctx) => {
            // When
            const actual = ctx.pythonDecoder.decode(ctx.pythonPrettifiedAssociativeArray);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(PythonFormat.ASSOCIATIVE_2D, metadata.getPythonFormat());
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertArrayEquals(ctx.data2, model.getData());
        },

    })
