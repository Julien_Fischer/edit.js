/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.MediaWikiDecoder')

    .beforeAll(ctx => {
        ctx.decoder = new MediaWikiDecoder();
    })

    .run({
        decode_whenMinifyAndFirstRowAsHeader_shouldReturnCorrectModelAndMetadata: (ctx) => {
            // Given
            const expected = [
                ['a', 'b'],
                ['c', 'd'],
                ['e', 'f']
            ];
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' |-\n' +
                ' ! a !! b\n' +
                ' |-\n' +
                ' | c || d\n' +
                ' |-\n' +
                ' | e || f\n' +
                ' |}\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.MINIFY, metadata.getOutputPolicy());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertArrayEquals(expected, model.getData());
        },

        decode_whenPrettifyAndFirstRowAsHeader_shouldReturnCorrectModelAndMetadata: (ctx) => {
            // Given
            const expected = [
                ['A', 'B'],
                ['c', 'd'],
                ['e', 'f']
            ];
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' |-\n' +
                ' ! A\n' +
                ' ! B\n' +
                ' |-\n' +
                ' | c\n' +
                ' | d\n' +
                ' |-\n' +
                ' | e\n' +
                ' | f\n' +
                ' |}\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertEquals(HeaderOrientation.FIRST_ROW, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertArrayEquals(expected, model.getData());
        },

        decode_whenMinifyAndFirstColumnAsHeader_shouldReturnCorrectModelAndMetadata: (ctx) => {
            // Given
            const expected = [
                ['a', 'b', 'c'],
                ['d', 'e', 'f'],
                ['g', 'h', 'i']
            ];
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' |-\n' +
                ' ! a\n' +
                ' | b || c\n' +
                ' |-\n' +
                ' ! d\n' +
                ' | e || f\n' +
                ' |-\n' +
                ' ! g\n' +
                ' | h || i\n' +
                ' |}\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.MINIFY, metadata.getOutputPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertArrayEquals(expected, model.getData());
        },

        decode_whenMinifyAndNoHeader_shouldReturnCorrectModelAndMetadata: (ctx) => {
            // Given
            const expected = [
                ['a', 'b'],
                ['c', 'd'],
                ['e', 'f']
            ];
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' |-\n' +
                ' | a || b\n' +
                ' |-\n' +
                ' | c || d\n' +
                ' |-\n' +
                ' | e || f\n' +
                ' |}\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.MINIFY, metadata.getOutputPolicy());
            assertEquals(HeaderOrientation.NO_HEADER, metadata.getHeaderOrientation());
            assertArrayEquals(expected, model.getData());
        },

        decode_whenPrettifyAndMissingCells_shouldDetectCorrectOutputPolicy: (ctx) => {
            // Given
            const input = `
                {| class="wikitable"
                |-
                ! a
                ! b
                ! 
                |-
                | AA
                | 
                | 
                |-
                | 
                | BBB
                | 
                |}
            `;
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
        },

        decode_whenPrettifyAndFirstColumnAsHeader_shouldReturnCorrectModelAndMetadata: (ctx) => {
            // Given
            const expected = [
                ['A', 'b'],
                ['C', 'd'],
                ['E', 'f']
            ];
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' |-\n' +
                ' ! A\n' +
                ' | b\n' +
                ' |-\n' +
                ' ! C\n' +
                ' | d\n' +
                ' |-\n' +
                ' ! E\n' +
                ' | f\n' +
                ' |}\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            const metadata = actual.getMetadata();
            assertEquals(OutputPolicy.PRETTIFY, metadata.getOutputPolicy());
            assertEquals(HeaderOrientation.FIRST_COLUMN, metadata.getHeaderOrientation());
            assertEquals(TextCase.UPPER, metadata.getHeaderCase());
            assertArrayEquals(expected, model.getData());
        },

        supports_whenInputMediaWiki_shouldReturnFormatMediaWiki: (ctx) => {
            // Given
            const input = '\n' +
                ' {| class="wikitable"\n' +
                ' ! a\n' +
                ' ! b\n' +
                ' |-\n' +
                ' | c\n' +
                ' | d\n' +
                ' |-\n' +
                ' | e\n' +
                ' | f\n' +
                ' |}\n' +
                '';
            // Then
            assertEquals(Format.MEDIAWIKI, ctx.decoder.supports(input));
        },

        supports_whenInputNotMediaWiki_shouldReturnNull: (ctx) => {
            // Given
            const input = 'Some string';
            // Then
            assertNull(ctx.decoder.supports(input));
        },

        decode_shouldDetectColumnAlignments: (ctx) => {
            // Given
            const expected = [Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT];
            const input = '' +
                '{| class="wikitable"\n' +
                '|-\n' +
                '! style="text-align: left;"| Cake\n' +
                '! Potato\n' +
                '! 8,333.00\n' +
                '|-\n' +
                '| style="text-align: left;"| Orange\n' +
                '| Apple\n' +
                '| 12,333.00\n' +
                '|-\n' +
                '| Bread\n' +
                '| style="text-align:center;"| Pie\n' +
                '| 500.00\n' +
                '|- style="font-style: italic; color: green;"\n' +
                '| Butter || Ice cream\n' +
                '| style="text-align:right;"| 1.00\n' +
                '|}\n' +
                '\n';
            // When
            const actual = ctx.decoder.decode(input);
            // Then
            const model = actual.getTableModel();
            assertArrayEquals(expected, model.getAlignments());
        }

    });

