/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.ConfluenceDecoder')

    .beforeAll(ctx => {
        ctx.header1 = 'header1';
        ctx.header2 = 'header1';
        ctx.cell1   = 'cell1';
        ctx.cell2   = 'cell2';
        ctx.data = [
            [ctx.header1, ctx.header2],
            [ctx.cell1,   ctx.cell2]
        ];
    })

    .run({
        decode_whenFormatConfluence_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = '\n' +
                '\n' +
                ' || ' + ctx.header1 + ' || ' + ctx.header2 + ' ||\n' +
                ' | ' + ctx.cell1 + '    | ' + ctx.cell2 + '    |\n' +
                '\n';
            const decoder = new ConfluenceDecoder();
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(ctx.data, actual.getData());
        },

        decode_whenFormatCucumber_shouldReturnCorrectTableModel: (ctx) => {
            // Given
            const input = '\n' +
                '\n' +
                ' | ' + ctx.header1 + ' | ' + ctx.header2 + ' |\n' +
                ' | ' + ctx.cell1 + '   | ' + ctx.cell2 + '   |\n' +
                '\n';
            const decoder = new ConfluenceDecoder();
            // When
            const actual = decoder.decode(input).getTableModel();
            // Then
            assertArrayEquals(ctx.data, actual.getData());
        },

        decode_whenFormatConfluenceAndOutputPolicyMinify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '' +
                '||Hello||world||\n' +
                '|a1|a2|';
            // When
            const decoder = new ConfluenceDecoder(Format.CONFLUENCE_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenFormatConfluenceAndOutputPolicyPrettify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '\n' +
                ' || Hello || world ||\n' +
                ' |  a1    |  a2     |\n';
            // When
            const decoder = new ConfluenceDecoder(Format.CONFLUENCE_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenFormatCucumberAndOutputPolicyMinify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '' +
                '|Hello|world|\n' +
                '|a1|a2|';
            // When
            const decoder = new ConfluenceDecoder(Format.CUCUMBER_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenFormatCucumberAndOutputPolicyPrettify_shouldDetectOutputPolicy: () => {
            // Given
            const input = '\n' +
                ' | Hello | world |\n' +
                ' | a1    | a2    |\n';
            // When
            const decoder = new ConfluenceDecoder(Format.CUCUMBER_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual.getMetadata().getOutputPolicy());
        },

        decode_whenFormatCucumberAndUppercaseHeaders_shouldUpperHeaderCase: () => {
            // Given
            const input = '\n' +
                ' | HELLO | WORLD |\n' +
                ' | a1    | a2    |\n';
            // When
            const decoder = new ConfluenceDecoder(Format.CUCUMBER_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(TextCase.UPPER, actual.getMetadata().getHeaderCase());
        },

        decode_whenFormatCucumberAndCapitalizeHeaders_shouldCapitalizeHeaderCase: () => {
            // Given
            const input = '\n' +
                ' | Hello | World |\n' +
                ' | a1    | a2    |\n';
            // When
            const decoder = new ConfluenceDecoder(Format.CUCUMBER_MARKDOWN);
            const actual = decoder.decode(input);
            // Then
            assertEquals(TextCase.CAPITALIZE, actual.getMetadata().getHeaderCase());
        }
    });
