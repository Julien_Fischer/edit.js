/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('decoders.Decoders')

    .run({
        removeBlankCells_shouldRemoveAllBlankCells: () => {
            // Given
            const expected = [
                ' +----------+----------+',
                ' | header 1 | header 2 |',
                ' +----------+----------+',
                ' | cell 1   | cell 2   |',
                ' +----------+----------+'
            ];
            const emptyRows = [
                '',
                ''
            ];
            const rows = emptyRows.concat(expected);
            rows.push('');
            // When
            const actual = Decoders.removeBlankCells(rows);
            // Then
            assertArrayEquals(expected, actual);
        },

        removeBlankCells_whenClip_shouldRemoveAllBlankCellsBeforeAndAfter: () => {
            // Given
            const expected = [
                ['Header1', 'Header2'],
                ['Cell A',  'Cell B' ],
                ['  ',      'Cell D' ]
            ];
            const input = [
                ['', 'Header1', 'Header2', ''],
                ['', 'Cell A',  'Cell B',  ''],
                ['', '  ',      'Cell D',  '']
            ];
            // When
            const actual = Decoders.trimMatrix(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        removeEmptyCells_shouldRemoveAllEmptyRows: () => {
            // Given
            const expected = [
                'Foo',
                'Bar',
            ];
            const rows = [
                '',
                '',
                'Foo',
                '',
                'Bar',
                '',
            ];
            // When
            const actual = Decoders.removeEmptyCells(rows);
            // Then
            assertArrayEquals(expected, actual);
        },

        trimArray_shouldRemoveAllSpacesBeforeAndAfterEachString: () => {
            // Given
            const expected = [
                '+----------+----------+',
                '| header 1 | header 2 |',
                '+----------+----------+',
                '| cell 1   | cell 2   |',
                '+----------+----------+'
            ];
            const rows = [
                ' +----------+----------+  ',
                ' | header 1 | header 2 |  ',
                ' +----------+----------+  ',
                ' | cell 1   | cell 2   |  ',
                ' +----------+----------+  '
            ];
            // When
            const actual = Decoders.trimArray(rows);
            // Then
            assertArrayEquals(expected, actual);
        },

        trimMatrix_whenClip1_shouldRemoveAllSpacesBeforeAndAfterFirstAndLastNonEmptyCells: () => {
            // Given
            const expected = [
                ['5','6'],
                ['9','A'],
                ['D','E'],
            ];
            const arr = [
                ['','', '', ''],
                ['','5','6',''],
                ['','9','A',''],
                ['','D','E',''],
                ['','' ,'' ,'']
            ];
            // When
            const actual = Decoders.trimMatrix(arr, true);
            // Then
            assertArrayEquals(expected, actual);
        },

        trimMatrix_whenClip2_shouldRemoveAllSpacesBeforeAndAfterFirstAndLastNonEmptyCells: () => {
            // Given
            const expected = [
                ['5','6'],
                ['9','A'],
                ['D','E'],
            ];
            const arr = [
                ['','','','', '', '',''],
                ['','','','', '', '',''],
                ['','','','5','6','',''],
                ['','','','9','A','',''],
                ['','','','D','E','',''],
                ['','','','' ,'' ,'',''],
                ['','','','' ,'' ,'',''],
            ];
            // When
            const actual = Decoders.trimMatrix(arr, true);
            // Then
            assertArrayEquals(expected, actual);
        },

        detectOutputPolicy_whenPrettify_shouldReturnPrettify: () => {
            // Given
            const rows = [
                ['foo', 'b  \n'],
                ['c  ', 'bar\n']
            ];
            // When
            const actual = Decoders.detectOutputPolicy(rows);
            // Then
            assertEquals(OutputPolicy.PRETTIFY, actual);
        },

        detectOutputPolicy_whenMinify_shouldReturnMinify: () => {
            // Given
            const rows = [
                ['foo', 'b\n'],
                ['c', 'd\n']
            ];
            // When
            const actual = Decoders.detectOutputPolicy(rows);
            // Then
            assertEquals(OutputPolicy.MINIFY, actual);
        },

        formatCell_whenFirstRowAsHeaderAndUppercase_shouldUppercaseValue: () => {
            // Given
            const input = 'Cell value';
            const expected = input.toUpperCase();
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setHeaderCase(TextCase.UPPER);
            // When
            const actual = Decoders.formatHeaderCell(3, 0, input, metadata);
            // Then
            assertEquals(expected, actual);
        },

        formatCell_whenFirstColumnAsHeaderAndUppercase_shouldUppercaseValue: () => {
            // Given
            const input = 'Cell value';
            const expected = input.toUpperCase();
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN)
                .setHeaderCase(TextCase.UPPER);
            // When
            const actual = Decoders.formatHeaderCell(0, 5, input, metadata);
            // Then
            assertEquals(expected, actual);
        }
    });
