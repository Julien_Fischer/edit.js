/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.SqlHighlighter')

    .beforeAll(ctx => {
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                CREATE TABLE table_name (
                    Col A VARCHAR(255),
                    VARCHAR(255),
                    Col C VARCHAR(255),
                    VARCHAR(255);
                );

                INSERT INTO table_name
                    (Col A, , Col C, )
                VALUES
                    ('AA', 'BB', '', ''),
                    ('AAA', '', 'CCC', '');
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">CREATE</span> <span class=\"token keyword\">TABLE</span> table_name <span class=\"token delimiter\">(</span>",
                "                    Col A <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    Col C <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "                <span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "",
                "                <span class=\"token keyword\">INSERT</span> <span class=\"token keyword\">INTO</span> table_name",
                "                    <span class=\"token delimiter\">(</span>Col A<span class=\"token delimiter\">,</span> <span class=\"token delimiter\">,</span> Col C<span class=\"token delimiter\">,</span> <span class=\"token delimiter\">)</span>",
                "                <span class=\"token keyword\">VALUES</span>",
                "                    <span class=\"token delimiter\">(</span><span class=\"token string\">&#039;AA&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;BB&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">(</span><span class=\"token string\">&#039;AAA&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;CCC&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new SqlHighlighter();
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        highlight_whenInvalidInputAndContainsEscapedHtmlEntities_shouldNotBreakSyntaxHighlighting: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                CREATE TABLE table_name (
                    <a href="#">Link</a> VARCHAR(255),
                                         VARCHAR(255),
                    Col C                VARCHAR(255),
                                         VARCHAR(255);
                );
                
                INSERT INTO table_name
                    (<a href="#">Link</a>, , Col C, )
                VALUES
                    ('AA', 'BB', '', ''),
                    ('AAA', '', 'CCC', '<a href=\\"#\\">Link 2</a>');
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">CREATE</span> <span class=\"token keyword\">TABLE</span> table_name <span class=\"token delimiter\">(</span>",
                "                    &lt;a href=<span class=\"token string\">&quot;#&quot;</span>&gt;Link&lt;/a&gt; <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                                         <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    Col C                <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                                         <span class=\"token keyword\">VARCHAR</span><span class=\"token delimiter\">(</span><span class=\"token number\">255</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "                <span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "                ",
                "                <span class=\"token keyword\">INSERT</span> <span class=\"token keyword\">INTO</span> table_name",
                "                    <span class=\"token delimiter\">(</span>&lt;a href=<span class=\"token string\">&quot;#&quot;</span>&gt;Link&lt;/a&gt;<span class=\"token delimiter\">,</span> <span class=\"token delimiter\">,</span> Col C<span class=\"token delimiter\">,</span> <span class=\"token delimiter\">)</span>",
                "                <span class=\"token keyword\">VALUES</span>",
                "                    <span class=\"token delimiter\">(</span><span class=\"token string\">&#039;AA&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;BB&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">,</span>",
                "                    <span class=\"token delimiter\">(</span><span class=\"token string\">&#039;AAA&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;CCC&#039;</span><span class=\"token delimiter\">,</span> <span class=\"token string\">&#039;&lt;a href=\\&quot;#\\&quot;&gt;Link 2&lt;/a&gt;&#039;</span><span class=\"token delimiter\">)</span><span class=\"token delimiter\">;</span>",
                "            "
            ];
            const highlighter = new SqlHighlighter();
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
