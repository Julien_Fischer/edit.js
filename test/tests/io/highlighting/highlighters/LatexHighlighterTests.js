/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.LatexHighlighter')

    .beforeAll(ctx => {
        ctx.mockEncoderOutput = (subject) => {
            return StringUtils.escapeHTMLChars(subject).split('\n');
        }
    })

    .run({
        highlight_shouldReturnHighlightedSyntax: (ctx) => {
            // Given
            const input = ctx.mockEncoderOutput(`
                \\begin{table}[!ht]
                    \\centering
                    \\caption{Table Caption}
                    \\begin{tabular}{|l|l|l|l|}
                    \\hline
                        A   & B   & C  &   \\\\ \\hline
                        AA  & #   & CC & # \\\\
                        AAA & BBB & #  & # \\\\ \\hline
                    \\end{tabular}
                    \\label{Table Label}
                \\end{table}
            `);
            const expected = [
                "",
                "                <span class=\"token keyword\">\\begin</span><span class=\"token delimiter\">{</span><span class=\"token attribute\">table</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">[</span>!ht<span class=\"token delimiter\">]</span>",
                "                    <span class=\"token keyword\">\\centering</span>",
                "                    <span class=\"token keyword\">\\caption</span><span class=\"token delimiter\">{</span>Table Caption<span class=\"token delimiter\">}</span>",
                "                    <span class=\"token keyword\">\\begin</span><span class=\"token delimiter\">{</span><span class=\"token attribute\">tabular</span><span class=\"token delimiter\">}</span><span class=\"token delimiter\">{</span>|l|l|l|l|<span class=\"token delimiter\">}</span>",
                "                    <span class=\"token keyword\">\\hline</span>",
                "                        <span class=\"token value\">A   </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> B   </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> C  </span><span class=\"token separator\">&amp;</span><span class=\"token value\">   </span>\\\\ <span class=\"token keyword\">\\hline</span>",
                "                        <span class=\"token value\">AA  </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> #   </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> CC </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> # </span>\\\\",
                "                        <span class=\"token value\">AAA </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> BBB </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> #  </span><span class=\"token separator\">&amp;</span><span class=\"token value\"> # </span>\\\\ <span class=\"token keyword\">\\hline</span>",
                "                    <span class=\"token keyword\">\\end</span><span class=\"token delimiter\">{</span><span class=\"token attribute\">tabular</span><span class=\"token delimiter\">}</span>",
                "                    <span class=\"token keyword\">\\label</span><span class=\"token delimiter\">{</span>Table Label<span class=\"token delimiter\">}</span>",
                "                <span class=\"token keyword\">\\end</span><span class=\"token delimiter\">{</span><span class=\"token attribute\">table</span><span class=\"token delimiter\">}</span>",
                "            "
            ];
            const highlighter = new LatexHighlighter();
            // When
            const actual = highlighter.highlight(input);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
