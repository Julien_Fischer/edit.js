/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('highlighters.Highlighter')

    .beforeAll(ctx => {
        ctx.highlighter = new Highlighter();
    })

    .run({
        colorize_WhenNoTokens_shouldReturnInputString: (ctx) => {
            // Given
            const input = 'Hello world!';
            // When
            const actual = ctx.highlighter.colorize(input, []);
            // Then
            assertEquals(input, actual);
        },

        colorize_whenTwoTokens_shouldReturnColorizedString: (ctx) => {
            // Given
            const opening = '<a href="#">';
            const closing = '</a>';
            const input = `This is a ${opening}link${closing} inside a string`;
            const tokens = [
                new Token(opening, 10, TokenType.TAG),
                new Token(closing, 26, TokenType.TAG),
            ];
            const expected = '' +
                'This is a ' +
                '<span class="token tag"><a href="#"></span>' +
                'link' +
                '<span class="token tag"></a></span>' +
                ' inside a string';
            // When
            const actual = ctx.highlighter.colorize(input, tokens);
            // Then
            assertEquals(expected, actual);
        },

        getEscapedHtmlEntity_whenNoEscapedHtmlEntityAtSpecifiedIndex_shouldReturnNull: (ctx) => {
            // Given
            const input = `Hello world!`;
            // When
            const actual = ctx.highlighter.getEscapedHtmlEntity(input, 5);
            // Then
            assertNull(actual);
        },

        getEscapedHtmlEntity_whenEscapedHtmlEntityAtSpecifiedIndex_shouldReturnEscapedHtmlEntity: (ctx) => {
            // Given
            const entity = HtmlEntities.QUOTE_DOUBLE.escaped;
            const input = `Hello ${entity} world!`;
            // When
            const actual = ctx.highlighter.getEscapedHtmlEntity(input, 6);
            // Then
            assertEquals(entity, actual);
        },

        isEscapedHtmlDelimiterEntity_whenTrue_shouldReturnTrue: (ctx) => {
            // Given
            const opening = HtmlEntities.LESS_THAN.escaped;
            const closing = HtmlEntities.GREATER_THAN.escaped;
            // Then
            assertTrue(ctx.highlighter.isEscapedHtmlDelimiterEntity(opening));
            assertTrue(ctx.highlighter.isEscapedHtmlDelimiterEntity(closing));
        },

        isEscapedHtmlDelimiterEntity_whenFalse_shouldReturnFalse: (ctx) => {
            // Given
            const entity = HtmlEntities.QUOTE_DOUBLE.escaped;
            // Then
            assertFalse(ctx.highlighter.isEscapedHtmlDelimiterEntity(entity));
        },

        isBackSlashed_whenPreviousCharacterIsBackslash_shouldReturnTrue: (ctx) => {
            // Given
            const input = 'Hello \\world!';
            // Then
            assertTrue(ctx.highlighter.isBackSlashed(input, 7))
        },

        isBackSlashed_whenPreviousCharacterNotBackslash_shouldReturnFalse: (ctx) => {
            // Given
            const input = 'Hello  world!';
            // Then
            assertFalse(ctx.highlighter.isBackSlashed(input, 7))
        },

        isBackSlashed_whenFirstCharacter_shouldReturnFalse: (ctx) => {
            // Given
            const input = 'Hello \'world!';
            // Then
            assertFalse(ctx.highlighter.isBackSlashed(input, 0))
        },

        getIndexOfNextNonWhitespaceCharacter_whenNotFound_shouldReturnMinusOne: (ctx) => {
            // Given
            const input = '     ';
            // Then
            assertEquals(-1, ctx.highlighter.getIndexOfNextNonWhitespaceCharacter(input, 0))
        },

        getIndexOfNextNonWhitespaceCharacter_whenOffsetZero_shouldReturnIndexOfFirstMatch: (ctx) => {
            // Given
            const input = '   Foo    Bar   Baz';
            // Then
            assertEquals(3, ctx.highlighter.getIndexOfNextNonWhitespaceCharacter(input, 0))
        },

        getIndexOfNextNonWhitespaceCharacter_whenIndexAfterFirstMatch_shouldReturnIndexOfSecondMatch: (ctx) => {
            // Given
            const input = '   Foo    Bar   Baz';
            // Then
            assertEquals(10, ctx.highlighter.getIndexOfNextNonWhitespaceCharacter(input, 6))
        },

        getIndexOfNextCharacter_whenCharacterLiteral_shouldReturnFirstMatch: (ctx) => {
            // Given
            const input = '   Foo    Bar   Baz';
            // Then
            assertEquals(3, ctx.highlighter.getIndexOfNextCharacter(input, 0, 'F'))
        },

        getIndexOfNextCharacter_whenFunctionPredicate_shouldReturnFirstMatch: (ctx) => {
            // Given
            const input = '   Foo    Bar   Baz';
            // Then
            assertEquals(10, ctx.highlighter.getIndexOfNextCharacter(input, 0, char => char === 'B'))
        },

        getIndexOfNextCharacter_whenNotFound_shouldReturnMinusOne: (ctx) => {
            // Given
            const input = 'Hello world!';
            // Then
            assertEquals(-1, ctx.highlighter.getIndexOfNextCharacter(input, 0, 'z'))
        },

        getIndexOfNextCharacter_whenNotFoundAndDefaultValueSpecified_shouldReturnDefaultValue: (ctx) => {
            // Given
            const input = 'Hello world!';
            // Then
            assertEquals(-99, ctx.highlighter.getIndexOfNextCharacter(input, 0, 'z', input.length,-99))
        }
    })
