/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.CTypeEncoder')

    .beforeAll(ctx => {
        ctx.markupData = [
            ['<a>Link</a>', 'B', 'C'],
            ['D',           'E', 'F']
        ];
        ctx.data = [
            ['A', 'B', 'C'],
            ['D', 'E', 'F'],
            ['G', 'H', 'I']
        ];
    })

    .run({
        /* JAVA */
        encode_whenJavaAndMinifyAnd2dArray_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                'String[][] arr = {{"<a>Link</a>", "B", "C"}, {"D", "E", "F"}};'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVA)
                .setJavaFormat(JavaFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaAndPrettifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                'String[][] arr = {',
                '    {"<a>Link</a>", "B", "C"},',
                '    {"D", "E", "F"}',
                '};'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVA)
                .setJavaFormat(JavaFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaAndMinifyAnd2dList_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                'List<List<String>> arr = List.of(List.of("<a>Link</a>", "B", "C"), List.of("D", "E", "F"));'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVA)
                .setJavaFormat(JavaFormat.LIST_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaAndPrettifyAnd2dList_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                'List<List<String>> arr = List.of(',
                '    List.of("<a>Link</a>", "B", "C"),',
                '    List.of("D", "E", "F")',
                ');'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVA)
                .setJavaFormat(JavaFormat.LIST_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        /* C SHARP */
        encode_whenCSharpAndMinifyAndJaggedArray_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                'string[][] arr = {new string[] {"<a>Link</a>", "B", "C"}, new string[] {"D", "E", "F"}};',
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.C_SHARP)
                .setCSharpFormat(CSharpFormat.ARRAY_JAGGED)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenCSharpAndPrettifyAndJaggedArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                'string[][] arr = {',
                '    new string[] {"<a>Link</a>", "B", "C"},',
                '    new string[] {"D", "E", "F"}',
                '};'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.C_SHARP)
                .setCSharpFormat(CSharpFormat.ARRAY_JAGGED)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenCSharpAndMinifyAnd2dArray_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                'string[,] arr = {{"<a>Link</a>", "B", "C"}, {"D", "E", "F"}};'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.C_SHARP)
                .setCSharpFormat(CSharpFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenCSharpAndPrettifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                'string[,] arr = {',
                '    {"<a>Link</a>", "B", "C"},',
                '    {"D", "E", "F"}',
                '};'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.C_SHARP)
                .setCSharpFormat(CSharpFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.markupData);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        /* PHP */

        encode_whenPhpAndMinifyAnd2dArray_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '$arr = array(array("A", "B", "C"), array("D", "E", "F"), array("G", "H", "I"));'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PHP)
                .setPhpFormat(PhpFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPhpAndPrettifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '$arr = array(',
                '    array("A", "B", "C"),',
                '    array("D", "E", "F"),',
                '    array("G", "H", "I")',
                ');'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PHP)
                .setPhpFormat(PhpFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPhpAndMinifyAndAssociativeArrays_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '$arr = array(array("A" => "D", "B" => "E", "C" => "F"), array("A" => "G", "B" => "H", "C" => "I"));'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PHP)
                .setPhpFormat(PhpFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPhpAndPrettifyAndAssociativeArrays_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '$arr = array(',
                '    array(',
                '        "A" => "D",',
                '        "B" => "E",',
                '        "C" => "F"',
                '    ),',
                '    array(',
                '        "A" => "G",',
                '        "B" => "H",',
                '        "C" => "I"',
                '    )',
                ');'
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PHP)
                .setPhpFormat(PhpFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        /* JAVASCRIPT */

        encode_whenJavaScriptAndPrettifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "const arr = [",
                "    ['A', 'B', 'C'],",
                "    ['D', 'E', 'F'],",
                "    ['G', 'H', 'I']",
                "];"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVASCRIPT)
                .setJsonFormat(JsonFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaScriptAndMinifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "const arr = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']];"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVASCRIPT)
                .setJsonFormat(JsonFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaScriptAndPrettifyAndAssociativeArrays_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "const arr = [",
                "    {",
                "        A: 'D',",
                "        B: 'E',",
                "        C: 'F'",
                "    },",
                "    {",
                "        A: 'G',",
                "        B: 'H',",
                "        C: 'I'",
                "    }",
                "];"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVASCRIPT)
                .setJsonFormat(JsonFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenJavaScriptAndMinifyAndAssociativeArrays_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "const arr = [{A: 'D', B: 'E', C: 'F'}, {A: 'G', B: 'H', C: 'I'}];"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.JAVASCRIPT)
                .setJsonFormat(JsonFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        /* PYTHON */

        encode_whenPythonAndPrettifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "arr = [",
                "    ['A', 'B', 'C'],",
                "    ['D', 'E', 'F'],",
                "    ['G', 'H', 'I']",
                "]"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PYTHON)
                .setPythonFormat(PythonFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPythonAndMinifyAnd2dArray_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "arr = [['A', 'B', 'C'], ['D', 'E', 'F'], ['G', 'H', 'I']]"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PYTHON)
                .setPythonFormat(PythonFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPythonAndPrettifyAndAssociativeArrays_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "arr = [",
                "    {",
                "        'A': 'D',",
                "        'B': 'E',",
                "        'C': 'F'",
                "    },",
                "    {",
                "        'A': 'G',",
                "        'B': 'H',",
                "        'C': 'I'",
                "    }",
                "]"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PYTHON)
                .setPythonFormat(PythonFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPythonAndMinifyAndAssociativeArrays_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                "arr = [{'A': 'D', 'B': 'E', 'C': 'F'}, {'A': 'G', 'B': 'H', 'C': 'I'}]"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PYTHON)
                .setPythonFormat(PythonFormat.ASSOCIATIVE_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPythonAndContainsQuotes_shouldEscapeQuotes: (ctx) => {
            // Given
            const input = [
                ["A'", "'B\\'"],
                ["C", "D"]
            ];
            const expected = [
                "arr = [['A\\'', '\\'B\\''], ['C', 'D']]"
            ];
            const metadata = new TableMetadata()
                .setFormat(Format.PYTHON)
                .setPythonFormat(PythonFormat.ARRAY_2D)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new CTypeEncoder(metadata);
            const model = new TableModel([], input);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
