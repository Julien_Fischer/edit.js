/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.SqlEncoder')

    .run({
        encode_shouldSerializeTableModel: () => {
            // Given
            const header1 = 'COLUMN_NAME_1',
                header2 = 'SHORT',
                header3 = 'COL_NAME_3',
                header4 = 'VERY_LONG_COLUMN_NAME',
                cell1 = 'Value1',
                cell2 = 'Val2',
                cell3 = 'Value3',
                cell4 = 'Value4',
                cell5 = 'Value5',
                cell6 = 'Value6',
                cell7 = 'Value7',
                cell8 = 'Value8';
            const expected = [
                'CREATE TABLE table_name (',
                '    ' + header1 + '         VARCHAR(255),',
                '    ' + header2 + '                 VARCHAR(255),',
                '    ' + header3 + '            VARCHAR(255),',
                '    ' + header4 + ' VARCHAR(255);',
                ');',
                '',
                'INSERT INTO table_name',
                '    (' + header1 + ', ' + header2 + ', ' + header3 + ', ' + header4 + ')',
                'VALUES',
                '    (\'' + cell1 + '\', \'' + cell2 + '\', \'' + cell3 + '\', \'' + cell4 + '\'),',
                '    (\'' + cell5 + '\', \'' + cell6 + '\', \'' + cell7 + '\', \'' + cell8 + '\');'
            ];
            const data = [
                [header1, header2, header3, header4],
                [cell1, cell2, cell3, cell4],
                [cell5, cell6, cell7, cell8]
            ];
            const encoder = new SqlEncoder(new TableMetadata());
            const tableModel = new TableModel([], data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(actual, expected);
        }
    });
