/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.BBCodeEncoder')

    .beforeAll(ctx => {
        ctx.data = [
            ['^', 'F', 'T'],
            ['F', '0', '0'],
            ['T', '0', '1']
        ];
    })

    .run({
        encode_whenPrettify_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '[table]',
                '    [tr]',
                '        [th]^[/th]',
                '        [th]F[/th]',
                '        [th]T[/th]',
                '    [/tr]',
                '    [tr]',
                '        [td]F[/td]',
                '        [td]0[/td]',
                '        [td]0[/td]',
                '    [/tr]',
                '    [tr]',
                '        [td]T[/td]',
                '        [td]0[/td]',
                '        [td]1[/td]',
                '    [/tr]',
                '[/table]'
            ];
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new BBCodeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinify_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '[table][tr][td]^[/td][td]F[/td][td]T[/td][/tr][tr][td]F[/td][td]0[/td][td]0[/td][/tr][tr][td]T[/td][td]0[/td][td]1[/td][/tr][/table]'
            ];
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.NO_HEADER)
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new BBCodeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinifyAndContainsHTMLCharacters_shouldGenerateMinifiedOutput: (ctx) => {
            // Given
            const expected = [
                '[table][tr][td]<a>link</a>[/td][td]Some text[/td][td]#tag[/td][/tr][/table]'
            ];
            const data = [
                ['<a>link</a>', 'Some text', '#tag']
            ];
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.NO_HEADER)
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new BBCodeEncoder(metadata);
            const model = new TableModel([], data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenPrettifyAndHeaderLowerCase_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '[table]',
                '    [tr]',
                '        [th]^[/th]',
                '        [th]f[/th]',
                '        [th]t[/th]',
                '    [/tr]',
                '    [tr]',
                '        [td]F[/td]',
                '        [td]0[/td]',
                '        [td]0[/td]',
                '    [/tr]',
                '    [tr]',
                '        [td]T[/td]',
                '        [td]0[/td]',
                '        [td]1[/td]',
                '    [/tr]',
                '[/table]'
            ];
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setHeaderCase(TextCase.LOWER)
                .setOutputPolicy(OutputPolicy.PRETTIFY);
            const encoder = new BBCodeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        },

        encode_whenMinifyAndHeaderLowerCase_shouldGeneratePrettifiedOutput: (ctx) => {
            // Given
            const expected = [
                '[table][tr][th]^[/th][th]f[/th][th]t[/th][/tr][tr][td]F[/td][td]0[/td][td]0[/td][/tr][tr][td]T[/td][td]0[/td][td]1[/td][/tr][/table]'
            ];
            const metadata = new TableMetadata()
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW)
                .setHeaderCase(TextCase.LOWER)
                .setOutputPolicy(OutputPolicy.MINIFY);
            const encoder = new BBCodeEncoder(metadata);
            const model = new TableModel([], ctx.data);
            // When
            const actual = encoder.encode(model);
            // Then
            assertArrayEquals(expected, actual);
        }
    })
