/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('encoders.MediaWikiEncoder')

    .beforeAll(context => {
        context.alignments = ArrayUtils.repeat(Alignment.LEFT, 2);
        context.data = [
            ['a', 'b'],
            ['c', 'd']
        ];
    })

    .run({
        encode_whenMinifyAndFirstRowAsHeader_shouldGenerateMinifiedTableWithFirstRowAsHeader: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '! a !! b',
                '|-',
                '| c || d',
                '|}'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_ROW);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_whenMinifyAndFirstColumnAsHeader_shouldGenerateMinifiedTableWithFirstColumnAsHeader: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '! a',
                '| b || c',
                '|-',
                '! d',
                '| e || f',
                '|-',
                '! g',
                '| h || i',
                '|}'
            ];
            const input = [
                ['a', 'b', 'c'],
                ['d', 'e', 'f'],
                ['g', 'h', 'i']
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel([Alignment.LEFT, Alignment.LEFT, Alignment.LEFT], input);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_whenMinifyAndNoHeader_shouldGenerateMinifiedTableWithoutHeader: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '| a || b',
                '|-',
                '| c || d',
                '|}'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.MINIFY)
                .setHeaderOrientation(HeaderOrientation.NO_HEADER);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_whenPrettifyAndNoHeader_shouldGeneratePrettifiedTableWithoutHeader: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '| a',
                '| b',
                '|-',
                '| c',
                '| d',
                '|}'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.NO_HEADER);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_whenPrettifyAndFirstRowAsHeaderHeader_shouldGeneratePrettifiedTableWithFirstRowAsHeader: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '! a',
                '| b',
                '|-',
                '! c',
                '| d',
                '|}'
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_shouldComplyToSpecifiedColumnAlignments: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '! a',
                '| style="text-align: center;" | b',
                '| style="text-align: right;" | c',
                '|-',
                '! d',
                '| style="text-align: center;" | e',
                '| style="text-align: right;" | f',
                '|}'
            ];
            const input = [
                ['a', 'b', 'c'],
                ['d', 'e', 'f']
            ];
            const metadata = new TableMetadata()
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel([Alignment.LEFT, Alignment.CENTER, Alignment.RIGHT], input);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        },

        encode_whenHeaderUppercase_shouldPrintHeaderAsUppercase: (ctx) => {
            // Given
            const expected = [
                '{| class="wikitable"',
                '|-',
                '! A',
                '| b',
                '|-',
                '! C',
                '| d',
                '|}'
            ];
            const metadata = new TableMetadata()
                .setHeaderCase(TextCase.UPPER)
                .setOutputPolicy(OutputPolicy.PRETTIFY)
                .setHeaderOrientation(HeaderOrientation.FIRST_COLUMN);
            const encoder = new MediaWikiEncoder(metadata);
            const tableModel = new TableModel(ctx.alignments, ctx.data);
            // When
            const actual = encoder.encode(tableModel);
            // Then
            assertArrayEquals(expected, actual)
        }

    });
