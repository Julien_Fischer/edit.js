/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('models.Rectangle')

    .beforeAll(ctx => {
        ctx.x = 1;
        ctx.y = 2;
        ctx.width = 13;
        ctx.height = 25;
        ctx.rect = new Rectangle(ctx.x, ctx.y, ctx.width, ctx.height);
    })

    .run({
        getCoordinate_shouldReturnLocationOfTopLeftVertex: (ctx) => {
            const expected = new Vector(ctx.x, ctx.y);
            assertEquals(expected, ctx.rect.getCoordinate());
        },

        getEndCoordinate_shouldReturnLocationOfBottomRightVertex: (ctx) => {
            const expected = new Vector(ctx.x + ctx.width, ctx.y + ctx.height);
            assertEquals(expected, ctx.rect.getEndCoordinate());
        },

        getDimension_shouldReturnCorrectDimension: (ctx) => {
            const expected = new Vector(ctx.width, ctx.height);
            assertEquals(expected, ctx.rect.getDimension());
        },

        equals_whenEqual_shouldReturnTrue: (ctx) => {
            const rect = new Rectangle(ctx.x, ctx.y, ctx.width, ctx.height);
            assertEquals(rect, ctx.rect);
        },

        equals_whenNotEqual_shouldReturnFalse: (ctx) => {
            const rect = new Rectangle(ctx.x, ctx.y + 1, ctx.width, ctx.height);
            assertNotEquals(rect, ctx.rect);
        },

        containsCoordinate_whenCoordinateWithinBounds_shouldReturnTrue: (ctx) => {
            const coordinate = new Vector(ctx.x + 1, ctx.y + 3);
            assertTrue(ctx.rect.containsCoordinate(coordinate));
        },

        containsCoordinate_whenCoordinateNotWithinBounds_shouldReturnFalse: (ctx) => {
            const coordinate = new Vector(ctx.x - 1, ctx.y + 3);
            assertFalse(ctx.rect.containsCoordinate(coordinate));
        },

        containsRectangle_whenRectangleWithinBounds_shouldReturnTrue: (ctx) => {
            const rect = new Rectangle(ctx.x, ctx.y + 1, ctx.width - 1, ctx.height - 2);
            assertTrue(ctx.rect.containsRectangle(rect));
        },

        containsRectangle_whenRectangleNotWithinBounds_shouldReturnFalse: (ctx) => {
            const rect = new Rectangle(ctx.x, ctx.y - 1, ctx.width, ctx.height);
            assertFalse(ctx.rect.containsRectangle(rect));
        }
    });
