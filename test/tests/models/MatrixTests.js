/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('models.Matrix')

    .run({
        insertRow_shouldInsertEmptyRowAtSpecifiedIndex: () => {
            // Given
            let h = 3;
            const matrix = new Matrix(2, h);
            matrix.fill(0);
            // When
            matrix.insertRow();
            assertArrayEquals([null, null], matrix.getRow(h));
            matrix.fill(0);
            // When
            matrix.insertRow(1);
            assertArrayEquals([null, null], matrix.getRow(1));
            matrix.fill(0);
            // When
            matrix.insertRow(3);
            assertArrayEquals([null, null], matrix.getRow(3));
        },

        removeRow_shouldRemoveRowAtSpecifiedIndex: () => {
            // Given
            let h = 3;
            const matrix = new Matrix(2, h);
            // When
            matrix.removeRow();
            // Then
            assertEquals(h - 1, matrix.getHeight());
        },

        removeColumn_shouldRemoveColumnAtSpecifiedIndex: () => {
            // Given
            let w = 3;
            const matrix = new Matrix(w, 2);
            // When
            matrix.removeColumn();
            // Then
            assertEquals(w - 1, matrix.getWidth());
        },

        reset_whenParameters_shouldGenerateEmptyMatrixWithSpecifiedDimension: () => {
            // Given
            const expected = [
                [null, null, null],
                [null, null, null]
            ];
            const matrix = new Matrix(0, 0);
            // When
            matrix.reset(3, 2);
            // Then
            assertArrayEquals(expected, matrix.toArray());
        },

        fill_shouldOnlyFillCellMatchingPredicate: () => {
            const predicate = (cell) => cell == null;
            const expected = [
                [1, 0, 1],
                [1, 0, 1],
                [1, 0, 1]
            ];
            const matrix = new Matrix(3, 3);
            matrix.fillColumn(1, 0);
            // When
            matrix.fill(1, predicate);
            // Then
            assertArrayEquals(expected, matrix.toArray());
        },

        fill_whenValueProvider_shouldFillMatrixWithGeneratedValue: () => {
            const expected = [
                ['null_0-0', 'a_1-0', 'null_2-0'],
                ['null_0-1', 'a_1-1', 'null_2-1'],
                ['null_0-2', 'a_1-2', 'null_2-2']
            ];
            const provider = (cell, x, y) => `${cell}_${x}-${y}`;
            const matrix = new Matrix(3, 3);
            matrix.fillColumn(1, 'a');
            // When
            matrix.fill(provider);
            // Then
            assertArrayEquals(expected, matrix.toArray());
        },

        forEachRow_shouldExecuteSpecifiedCallbackForEachRow: () => {
            // Given
            const matrix = new Matrix(2, 3);
            // When
            matrix.fill(0);
            // Then
            matrix.forEachRow(row => assertArrayEquals([0, 0], row));
        },

        forEachColumn_shouldExecuteSpecifiedCallbackForEachColumn: () => {
            // Given
            const matrix = new Matrix(2, 3);
            // When
            matrix.fill(1);
            // Then
            matrix.forEachColumn(col => assertArrayEquals([1, 1, 1], col));
        },

        forEachCoordinate_shouldExecuteSpecifiedCallbackForEachCoordinate: () => {
            // Given
            const expected = [
                [0, 1, 2],
                [3, 4, 5]
            ];
            const matrix = new Matrix(3, 2);
            for (let y = 0; y < expected.length; y++) {
                for (let x = 0; x < expected[y].length; x++) {
                    const datum = expected[y][x];
                    matrix.set(x, y, datum)
                }
            }
            // Then
            matrix.forEachCoordinate((x, y) => {
                assertEquals(expected[y][x], matrix.get(x, y))
            });
        },

        get_whenWithinBounds_shouldReturnValueAtSpecifiedCoordinate: () => {
            // Given
            const expected = 'HELLO WORLD';
            const x = 1, y = 2;
            const matrix = new Matrix(2, 3);
            matrix.set(x, y, expected);
            // When
            const actual = matrix.get(x, y);
            // Then
            assertEquals(expected, actual)
        },

        get_whenOutsideBounds_shouldThrowException: () => {
            // Given
            const x = 3, y = 2;
            const matrix = new Matrix(x - 1, y);
            // Then
            assertThrows(() => matrix.get(x, y));
        },

        set_whenOutsideBounds_shouldThrowException: () => {
            // Given
            const x = 3, y = 2;
            const matrix = new Matrix(x - 1, y);
            // Then
            assertThrows(() => matrix.set(x, y, 'a'));
        },

        clear_shouldEmptyEachCell: () => {
            // Given
            const matrix = new Matrix(4, 5);
            matrix.fill(1);
            // When
            matrix.clear();
            // Then
            matrix.forEachCell(cell => assertNull(cell));
        },

        reset_whenNoParameters_shouldResetMatrixToItsInitialState: () => {
            // Given
            const w = 4, h = 5;
            const matrix = new Matrix(w, h);
            matrix.insertRow();
            matrix.insertColumn();
            matrix.fill(1);
            // When
            matrix.reset();
            // Then
            assertEquals(w + 1, matrix.getWidth());
            assertEquals(h + 1, matrix.getHeight());
            matrix.forEachCell(cell => assertNull(cell));
        },

        insertColumn_shouldIncrementWidthByOne: () => {
            // Given
            const w = 3;
            const matrix = new Matrix(w, 2);
            // When
            matrix.insertColumn();
            // Then
            assertEquals(w + 1, matrix.getWidth());
        },

        insertColumn_shouldInsertColumnWithSpecifiedElements: () => {
            // Given
            const expected = [
                [0, 1, 2,  9],
                [3, 4, 5, 10],
                [6, 7, 8, 11]
            ];
            const matrix = new Matrix().of([
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ]);
            // When
            matrix.insertColumn(-1, [9, 10, 11]);
            // Then
            const actual = matrix.toArray();
            assertArrayEquals(expected, actual);
        },

        insertRow_shouldInsertRowWithSpecifiedElements: () => {
            // Given
            const expected = [
                [0,  1,  2],
                [3,  4,  5],
                [6,  7,  8],
                [9, 10, 11]
            ];
            const matrix = new Matrix().of([
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ]);
            // When
            matrix.insertRow(-1, [9, 10, 11]);
            // Then
            const actual = matrix.toArray();
            assertArrayEquals(expected, actual);
        },

        insertRow_shouldIncrementHeightByOne: () => {
            // Given
            const h = 3;
            const matrix = new Matrix(2, h);
            // When
            matrix.insertRow();
            // Then
            assertEquals(h + 1, matrix.getHeight());
        },

        isBlank_whenEachCellNull_shouldReturnTrue: () => {
            const matrix = new Matrix(4, 5);
            assertTrue(matrix.isBlank());
            matrix.fill(1);
            assertFalse(matrix.isBlank());
            matrix.clear();
            assertTrue(matrix.isBlank());
        },

        isBlank_whenEachCellEmpty_shouldReturnTrue: () => {
            const matrix = new Matrix(4, 5);
            matrix.fill(new Cell());
            assertTrue(matrix.isBlank());
            matrix.forEachCell(cell => cell.setValue(1));
            assertFalse(matrix.isBlank());
            matrix.clear();
            assertTrue(matrix.isBlank());
        },

        assertTrue_whenCoordinateInside_shouldReturnTrue: () => {
            // Given
            const x = 2, y = 3;
            const coordinate = new Vector(x, y);
            const matrix = new Matrix(x + 1, y + 1);
            // When
            const contains = matrix.contains(coordinate);
            // Then
            assertTrue(contains);
        },

        assertTrue_whenCoordinateOutside_shouldReturnFalse: () => {
            // Given
            const x = 2, y = 3;
            const coordinate = new Vector(x, y);
            const matrix = new Matrix(x - 1, y + 1);
            // When
            const contains = matrix.contains(coordinate);
            // Then
            assertFalse(contains);
        },

        getRow_shouldReturnRowAtSpecifiedIndex: () => {
            // Given
            const index = 2;
            const matrix = new Matrix(3, index + 1);
            matrix.fillRow(index, 1);
            // When
            const row = matrix.getRow(index);
            // Then
            assertArrayEquals([1, 1, 1], row);
        },

        getColumn_shouldReturnColumnAtSpecifiedIndex: () => {
            // Given
            const index = 2;
            const matrix = new Matrix(index + 1, 3);
            matrix.fillColumn(index, 1);
            // When
            const column = matrix.getColumn(index);
            // Then
            assertArrayEquals([1, 1, 1], column);
        },

        swapRows_shouldSwapRowsAtSpecifiedIndex: () => {
            // Given
            const a = 1, b = 3;
            const matrix = new Matrix(2, 5);
            matrix.fillRow(a, 'a');
            matrix.fillRow(b, 'b');
            // When
            matrix.swapRows(a, b);
            // Then
            assertArrayEquals(['b', 'b'], matrix.getRow(a));
            assertArrayEquals(['a', 'a'], matrix.getRow(b));
        },

        swapColumns_shouldSwapColumnsAtSpecifiedIndex: () => {
            // Given
            const a = 1, b = 3;
            const matrix = new Matrix(5, 2);
            matrix.fillColumn(a, 'a');
            matrix.fillColumn(b, 'b');
            // When
            matrix.swapColumns(a, b);
            // Then
            assertArrayEquals(['b', 'b'], matrix.getColumn(a));
            assertArrayEquals(['a', 'a'], matrix.getColumn(b));
        },

        set_shouldNotifyCellValueChangeListeners: () => {
            // Given
            const expected = new CellUpdate(0, 0, null, 'a');
            let notified = false;
            let valueChange;
            const cellValueChangeListener = {
                onCellValueChange: (change) => {
                    notified = true;
                    valueChange = change;
                }
            };
            const matrix = new Matrix(2, 2);
            matrix.addCellValueChangeListener(cellValueChangeListener);
            // When
            matrix.set(expected.getX(), expected.getY(), expected.getNewValue());
            // Then
            assertTrue(notified);
            assertNotNull(valueChange);
            assertEquals(expected.getY(), valueChange.getY());
            assertEquals(expected.getX(), valueChange.getX());
            assertEquals(expected.getNewValue(), valueChange.getNewValue());
            assertEquals(expected.getPreviousValue(), valueChange.getPreviousValue());
        },

        insert_shouldNotifyStructureChangeListeners: () => {
            // Given
            const expected = [
                [null, null],
                [null, null]
            ];
            let notified = false;
            let tableModel;
            const structureChangeListener = {
                onTableStructureChange: (model) => {
                    notified = true;
                    tableModel = model;
                }
            };
            const matrix = new Matrix(1, 1);
            matrix.addStructureChangeListener(structureChangeListener);
            // When
            matrix.insertRow();
            // Then
            assertTrue(notified);
            notified = false;
            // When
            matrix.insertColumn();
            // Then
            assertTrue(notified);
            notified = false;
            matrix.fill(0);
            // When
            matrix.reset();
            // Then
            assertTrue(notified);
            assertArrayEquals(expected, tableModel);
        },

        transpose_shouldSwitchDataFromRowsToColumns: () => {
            // Given
            const input = [
                [0, 1, 2],
                [3, 4, 5]
            ];
            const expected = [
                [0, 3],
                [1, 4],
                [2, 5]
            ];
            const matrix = new Matrix().of(input);
            // When
            matrix.transpose();
            // Then
            assertArrayEquals(expected, matrix.toArray());
        },

        transpose_shouldBeIdempotent: () => {
            // Given
            const input = [
                [0, 1, 2],
                [3, 4, 5]
            ];
            const matrix = new Matrix().of(input);
            // When
            matrix.transpose();
            matrix.transpose();
            // Then
            assertArrayEquals(input, matrix.toArray());
        },

        of_shouldInitializeMatrixFromSpecifiedArray: () => {
            // Given
            const input = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ];
            // When
            const matrix = new Matrix().of(input);
            // Then
            assertArrayEquals(input, matrix.toArray());
        },

        getDimension_whenInitializedFromArray_shouldReturnMatrixDimension: () => {
            // Given
            const input = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ];
            // When
            const matrix = new Matrix().of(input);
            // Then
            assertEquals(input[0].length, matrix.getWidth());
            assertEquals(input.length, matrix.getHeight());
        },

        getDimension_whenInitializedEmpty_shouldReturnMatrixDimension: () => {
            // Given
            const width = 3;
            const height = 4;
            const matrix = new Matrix(width, height);
            // Then
            assertEquals(width, matrix.getWidth());
            assertEquals(height, matrix.getHeight());
        },

        clip_whenNotWithinBounds_shouldThrowError: () => {
            // Given
            const width = 10;
            const height = 20;
            const matrix = new Matrix(width, height);
            const rect = new Rectangle(1, 1, width, height);
            // Then
            assertThrows(() => matrix.clip(rect));
        },

        clip_whenWithinBounds_shouldReturnClippedMatrix: () => {
            // Given
            const expected = [
                ['5','6'],
                ['9','A'],
                ['D','E'],
            ];
            const arr = [
                ['0','1','2','3'],
                ['4','5','6','7'],
                ['8','9','A','B'],
                ['C','D','E','F'],
                ['G','H','I','J']
            ];
            const width = arr[0].length;
            const height = arr.length;
            const matrix = new Matrix().of(arr);
            const rect = new Rectangle(1, 1, width - 2, height - 2);
            // When
            const actual = matrix.clip(rect);
            // Then
            assertEquals(expected, actual.toArray());
        },

        clip_whenWithinBounds_shouldReturnClippedMatrix2: () => {
            // Given
            const expected = new Matrix().of([
                ['Header1', 'Header2'],
                ['Cell A',  'Cell B' ],
                ['  ',      'Cell D' ]
            ]);
            const input = [
                ['', 'Header1', 'Header2', ''],
                ['', 'Cell A',  'Cell B',  ''],
                ['', '  ',      'Cell D',  '']
            ];
            const matrix = new Matrix().of(input);
            const rectangle = new Rectangle(1, 0, 2, 3);
            // When
            const actual = matrix.clip(rectangle);
            // Then
            assertEquals(expected, actual);
        },

        equals_whenNotEqual_shouldReturnFalse: () => {
            // Given
            const data1 = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ];
            const data2 = [
                [0, 1, 2],
                [3, 0, 5],
                [6, 7, 8]
            ];
            const a = new Matrix().of(data1);
            const b = new Matrix().of(data2);
            // Then
            assertNotEquals(a, b);
            assertNotEquals(a, null);
        },

        equals_whenEqual_shouldReturnTrue: () => {
            // Given
            const data1 = [
                [0, 1, 2],
                [3, 4, 5],
                [6, 7, 8]
            ];
            const a = new Matrix().of(data1);
            const b = new Matrix().of(data1);
            // Then
            assertEquals(a, b);
        },

        map_whenMatrixOfIntegers_: () => {
            // Given
            const input = [
                [1, 2],
                [4, 8]
            ];
            const expected = [
                [2, 4],
                [8, 16]
            ];
            const matrix = new Matrix().of(input);
            // When
            const actual = matrix.map(cell => cell * 2);
            // Then
            assertArrayEquals(expected, actual.toArray());
        },

        map_whenMatrixOfStrings: () => {
            // Given
            const input = [
                ['a', 'b', 'c'],
                ['d', 'e', 'f']
            ];
            const expected = [
                ['aa', 'bb', 'cc'],
                ['dd', 'ee', 'ff']
            ];
            const matrix = new Matrix().of(input);
            // When
            const actual = matrix.map(cell => cell + cell);
            // Then
            assertArrayEquals(expected, actual.toArray());
        },

        toArray_shouldConvertMatrixTo2dArray: () => {
            // Given
            const expected = [
                ['a', 'b', 'c'],
                ['d', 'e', 'f']
            ];
            const matrix = new Matrix().of(expected);
            // Then
            assertArrayEquals(expected, matrix.toArray());
        }
    });
