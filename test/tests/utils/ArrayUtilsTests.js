/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.ArrayUtils')

    .run({
        contains_whenElementInArray_shouldReturnTrue: () => {
            // Given
            const value = 1;
            const arr = [0, value, 2, 3];
            // Then
            assertTrue(ArrayUtils.contains(arr, value))
        },

        indicesOf_whenThreeMatches_shouldReturnIndexOfEveryMatch: () => {
            // Given
            const subject = 'hello world!'.split('');
            const pattern = 'l';
            const expected = [2, 3, 9];
            // When
            const actual = ArrayUtils.indicesOf(subject, pattern);
            // Then
            assertArrayEquals(expected, actual);
        },

        indicesOf_whenNoMatch_shouldReturnEmptyArray: () => {
            // Given
            const subject = 'hello world!'.split('');
            const pattern = 'z';
            const expected = [];
            // When
            const actual = ArrayUtils.indicesOf(subject, pattern);
            // Then
            assertArrayEquals(expected, actual);
        },

        getLast_whenArrayNotEmpty_shouldReturnLastElement: () => {
            assertEquals(3, ArrayUtils.getLast([1, 2, 3]));
        },

        getLast_whenArrayEmptyAndDefaultValueIsSpecified_shouldReturnDefaultValue: () => {
            assertEquals(-1, ArrayUtils.getLast([], -1));
        },

        getLast_whenArrayEmptyAndDefaultValueNotSpecified_shouldThrowError: () => {
            assertThrows(() => ArrayUtils.getLast([]));
        },

        swap_shouldSwapElementsAtSpecifiedIndices: () => {
            // Given
            const expected = [5, 6, 7, 8];
            const actual = [5, 8, 7, 6];
            // When
            ArrayUtils.swap(actual, 1, 3);
            // Then
            assertArrayEquals(expected, actual);
        },

        insert_whenIndexPositive_shouldInsertElementAtSpecifiedIndex: () => {
            // Given
            const expected = [5, 6, 7, 8];
            const actual = [5, 6, 8];
            // When
            ArrayUtils.insert(actual, 2, 7);
            // Then
            assertArrayEquals(expected, actual);
        },

        insert_whenIndexZero_shouldInsertElementAtFirstPosition: () => {
            // Given
            const expected = [7, 5, 6, 8];
            const actual = [5, 6, 8];
            // When
            ArrayUtils.insert(actual, 0, 7);
            // Then
            assertArrayEquals(expected, actual);
        },

        insert_whenIndexMinusOne_shouldInsertElementAtLastIndex: () => {
            // Given
            const expected = [5, 6, 8, 7];
            const actual = [5, 6, 8];
            // When
            ArrayUtils.insert(actual, -1, 7);
            // Then
            assertArrayEquals(expected, actual);
        },

        convert2dArrayToArrayOfObjects: () => {
            // Given
            const expected = [
                {
                    "a": "1",
                    "b": "2",
                    "c": "3"
                },
                {
                    "a": "4",
                    "b": "5",
                    "c": "6"
                }
            ];
            const input = [
                ["a", "b", "c"],
                ["1", "2", "3"],
                ["4", "5", "6"]
            ];
            // When
            const actual = ArrayUtils.convert2dArrayToArrayOfObjects(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        convertArrayOfObjectsTo2dArray: () => {
            // Given
            const expected = [
                ["a", "b", "c"],
                ["1", "2", "3"],
                ["4", "5", "6"]
            ];
            const input = [
                {
                    "a": "1",
                    "b": "2",
                    "c": "3"
                },
                {
                    "a": "4",
                    "b": "5",
                    "c": "6"
                }
            ];
            // When
            const actual = ArrayUtils.convertArrayOfObjectsTo2dArray(input);
            // Then
            assertArrayEquals(expected, actual);
        },

        equalsIgnoreOrder_whenEqual_shouldReturnTrue: () => {
            // Given
            const a = [0, 1, 2, 3, 4];
            const b = [4, 2, 3, 0, 1];
            // Then
            assertTrue(ArrayUtils.equalsIgnoreOrder(a, b));
        },

        equalsIgnoreOrder_whenNotEqual_shouldReturnFalse: () => {
            // Given
            const a = [0, 1, 2, 3, -4];
            const b = [4, 2, 3, 0, 1];
            // Then
            assertFalse(ArrayUtils.equalsIgnoreOrder(a, b));
        },

        repeat_whenThreeTimes_shouldReturnArrayWithThreeTimesSpecifiedElement: () => {
            // Given
            const n = 3;
            const value = Alignment.LEFT;
            const expected = [value, value, value];
            // When
            const actual = ArrayUtils.repeat(value, n);
            // Then
            assertArrayEquals(expected, actual);
        },

        range_when5to10_shouldReturnArrayWithIntegersFrom5to9: () => {
            // Given
            const expected = [5, 6, 7, 8, 9];
            // When
            const actual = ArrayUtils.range(5, 10);
            // Then
            assertArrayEquals(expected, actual);
        },

        range_when0to0_shouldReturnEmptyArray: () => {
            // Given
            const expected = [];
            // When
            const actual = ArrayUtils.range(0, 0);
            // Then
            assertArrayEquals(expected, actual);
        },

        mostFrequent_whenArrayOfPrimitives_shouldReturnMostFrequentPrimitive: () => {
            // Given
            const expected = 1;
            const input = [0, expected, 2, expected, 0, expected, expected, 3, expected, 5, expected];
            // When
            const actual = ArrayUtils.mostFrequent(input);
            // Then
            assertEquals(expected, actual);
        },

        mostFrequent_whenHashCodeNotImplemented_shouldThrowError: () => {
            // Given
            const expected = {};
            const input = [expected, {}, expected, {}, {}];
            // Then
            assertThrows(() => ArrayUtils.mostFrequent(input));
        },

        mostFrequent_whenArrayOfObjects_shouldReturnMostFrequentObject: () => {
            // Given
            const expected = Alignment.RIGHT;
            const input = [Alignment.LEFT, expected, Alignment.CENTER, expected, Alignment.CENTER, expected];
            // When
            const actual = ArrayUtils.mostFrequent(input);
            // Then
            assertEquals(expected, actual);
        },

        mostFrequent_whenArrayEmpty_shouldReturnNull: () => {
            // Given
            const input = [];
            // When
            const actual = ArrayUtils.mostFrequent(input);
            // Then
            assertNull(actual);
        },

        isEmpty_whenArrayEmpty_shouldReturnTrue: () => {
            assertTrue(ArrayUtils.isEmpty([]));
        },

        isEmpty_whenArrayNotEmpty_shouldReturnFalse: () => {
            assertFalse(ArrayUtils.isEmpty([1]));
        },

        countOccurrences_whenNone_shouldReturnZero: () => {
            // Given
            const arr = ['hello', 'world', 'foo', 'bar'];
            const pattern = /z/;
            // When
            const actual = ArrayUtils.countOccurrences(arr, pattern);
            // Then
            assertEquals(0, actual);
        },

        countOccurrences_whenEmptyArray_shouldReturnZero: () => {
            // Given
            const arr = [];
            const pattern = /o/;
            // When
            const actual = ArrayUtils.countOccurrences(arr, pattern);
            // Then
            assertEquals(0, actual);
        },

        countOccurrences_whenThree_shouldReturnThree: () => {
            // Given
            const arr = ['hello', 'world', 'foo', 'bar'];
            const pattern = /o/;
            // When
            const actual = ArrayUtils.countOccurrences(arr, pattern);
            // Then
            assertEquals(3, actual);
        },

        indexOfMatch_whenZero_shouldReturnMinusOne: () => {
            // Given
            const arr = ['hello', 'world', 'foo', 'bar', 'world'];
            const pattern = /z/;
            // When
            const actual = ArrayUtils.indexOfMatch(arr, pattern);
            // Then
            assertEquals(-1, actual);
        },

        indexOfMatch_whenOne_shouldReturnOne: () => {
            // Given
            const arr = ['hello', 'world', 'foo', 'bar', 'world'];
            const pattern = /orl/;
            // When
            const actual = ArrayUtils.indexOfMatch(arr, pattern);
            // Then
            assertEquals(1, actual);
        }
    });


