/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.ObjectUtils')

    .beforeAll((context) => {
        context.input = {'key1': 1, 'key2': 2, 'key3': 3, 'key4': 4};
    })

    .run({
        map_whenMultiplyByTwo_shouldReturnEveryElementsWithValueMultipliedByTwo: (ctx) => {
            // Given
            const expected = {'key1': 2, 'key2': 4, 'key3': 6, 'key4': 8};
            // When
            const actual = ObjectUtils.map(ctx.input, (key, value) => value * 2);
            // Then
            assertObjectEquals(expected, actual);
        },

        forEach_shouldApplyFunctionToEveryObjectElement: (ctx) => {
            // Given
            const expected = ctx.input;
            const actual = {};
            // When
            ObjectUtils.forEach(ctx.input, (key, value) => actual[key] = value);
            // Then
            assertObjectEquals(expected, actual);
        },

        filter_whenGreaterThanTwo_shouldReturnElementsWithValueGreaterThanTwo: (ctx) => {
            // Given
            const expected = {'key3': 3, 'key4': 4};
            // When
            const actual = ObjectUtils.filter(ctx.input, (key, value) => value > 2);
            // Then
            assertObjectEquals(expected, actual);
        },

        findUnique_whenOneMatch_shouldReturnMatchedElement: (ctx) => {
            // Given
            const expected = {'key2': 2};
            // When
            const actual = ObjectUtils.findUnique(ctx.input, (key, value) => value === 2);
            // Then
            assertObjectEquals(expected, actual);
        },

        findUnique_whenMoreThanOneMatch_shouldThrowError: (ctx) => {
            assertThrows(() => ObjectUtils.findUnique(ctx.input, (key, value) => value > 2));
        },

        findUnique_whenNoMatch_shouldReturnNull: (ctx) => {
            // When
            const actual = ObjectUtils.findUnique(ctx.input, (key, value) => value > 99);
            // Then
            assertNull(actual);
        },

        anyMatch_whenNoMatch_shouldReturnFalse: (ctx) => {
            // When
            const actual = ObjectUtils.some(ctx.input, (key, value) => value > 99);
            // Then
            assertFalse(actual);
        },

        anyMatch_whenAtLeastOneMatch_shouldReturnTrue: (ctx) => {
            // When
            const actual = ObjectUtils.some(ctx.input, (key, value) => value > 2);
            // Then
            assertTrue(actual);
        },

        allMatch_whenAtLeastOneElementDoesNotMatch_shouldReturnFalse: (ctx) => {
            // When
            const actual = ObjectUtils.allMatch(ctx.input, (key, value) => value > 2);
            // Then
            assertFalse(actual);
        },

        allMatch_whenAllElementsMatch_shouldReturnTrue: (ctx) => {
            // When
            const actual = ObjectUtils.allMatch(ctx.input, (key, value) => value > 0);
            // Then
            assertTrue(actual);
        },

        length_whenTwo_shouldReturnTwo: (ctx) => {
            // Given
            const input2 = {'key3': 3, 'key4': 4};
            // When
            const inputLength = ObjectUtils.length(ctx.input);
            const input2Length = ObjectUtils.length(input2);
            // Then
            assertEquals(4, inputLength);
            assertEquals(2, input2Length);
        }
    })
