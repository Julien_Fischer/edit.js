/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.MatrixUtils')

    .run({
        getSmallestElement_shouldReturnSmallestElement: () => {
            // Given
            const expected = '1234';
            const arr = ['123', '1', expected, '12'];
            // When
            const actual = MatrixUtils.getLongestElement(arr);
            // Then
            assertEquals(expected, actual);
        },

        getLongestElement_shouldReturnLongestElement: () => {
            // Given
            const expected = '1';
            const arr = ['123', expected, '1234', '12'];
            // When
            const actual = MatrixUtils.getSmallestElement(arr);
            // Then
            assertEquals(expected, actual);
        },

        getColumn_shouldReturnValuesAtSameIndexOnXAxis: () => {
            // Given
            const matrix = [
                [0, 3, 6],
                [1, 4, 7],
                [2, 5, 8]
            ];
            // When
            const actual = MatrixUtils.getColumn(matrix, 1);
            // Then
            assertArrayEquals([3, 4, 5], actual);
        },

        getLargestRow_shouldReturnRowWithHighestNumberOfElements: () => {
            // Given
            const expected = [1, 4, 7, 10];
            const matrix = [
                [0, 1],
                expected,
                [2, 5, 8]
            ];
            // When
            const actual = MatrixUtils.getLargestRow(matrix, 1);
            // Then
            assertArrayEquals(expected, actual);
        },

        nullSafe_shouldReplaceEveryNullElementWithAnEmptyString: () => {
            // Given
            const expected = [
                ['a', '', 'c'],
                ['', '', 'f'],
                ['g', '', '']
            ];
            const matrix = [
                ['a', null, 'c'],
                [null, null, 'f'],
                ['g', null, null]
            ];
            // When
            const actual = MatrixUtils.nullSafe(matrix);
            // Then
            assertArrayEquals(expected, actual);
        },

        getLongestElementPerColumn_shouldReturnLongestElementPerColumn: () => {
            // Given
            const expected = ['hello', 'world', 'baz'];
            const matrix = [
                ['hello', 'a', 'b'],
                ['c', 'world', 'd'],
                ['foo', 'bar', 'baz'],
            ];
            // When
            const actual = MatrixUtils.getLongestElementPerColumn(matrix);
            // Then
            assertArrayEquals(expected, actual);
        },

        sortByLengthDescending_shouldSortArrayByTheLengthOfItsElements: () => {
            // Given
            const expected = [
                [1, 2, 3, 4],
                [1, 2, 3],
                [1, 2],
                [1]
            ];
            const arr = [
                [1, 2, 3, 4],
                [1],
                [1, 2, 3],
                [1, 2]
            ];
            // When
            const actual = MatrixUtils.sortByLengthDescending(arr);
            // Then
            assertArrayEquals(expected, actual);
        },

        rotate_shouldReturnRotateMatrixBy90Degrees: () => {
            // Given
            const matrix = [
                [0, 1, 2, 3],
                [4, 5, 6, 7],
            ];
            const expected = [
                [0, 4],
                [1, 5],
                [2, 6],
                [3, 7],
            ];
            // When
            const actual = MatrixUtils.rotate(matrix);
            // Then
            assertArrayEquals(expected, actual);
        },

        square_shouldEvenRowLengths: () => {
            // Given
            const matrix = [
                [0, 1],
                [0, 1, 2, 3],
                [0]
            ];
            const expected = [
                [0, 1, null, null],
                [0, 1, 2, 3],
                [0, null, null, null]
            ];
            // When
            const actual = MatrixUtils.square(matrix);
            // Then
            assertArrayEquals(expected, actual);
        },

        isBlank_whenEmpty_shouldReturnTrue: () => {
            // Given
            const matrix = [];
            // Then
            assertTrue(MatrixUtils.isBlank(matrix));
        },

        isBlank_whenEmptyAnd2D_shouldReturnTrue: () => {
            // Given
            const matrix = [[]];
            // Then
            assertTrue(MatrixUtils.isBlank(matrix));
        },


        isBlank_whenBlank_shouldReturnTrue: () => {
            // Given
            const matrix = [
                [null],
                []
            ];
            // Then
            assertTrue(MatrixUtils.isBlank(matrix));
        },

        isBlank_whenNotBlank_shouldReturnFalse: () => {
            // Given
            const matrix = [
                ['a', 'b']
            ];
            // Then
            assertFalse(MatrixUtils.isBlank(matrix));
        },

        clone_whenCloneModified_shouldNotMutateOriginalArray: () => {
            // Given
            const input = [
                [1, 2],
                [3, 4]
            ];
            // When
            const clone = MatrixUtils.clone(input);
            clone[0] = [0, 2];
            // Then
            assertNotEquals(input[0][0], clone[0][0]);
            assertNotEquals(input[1][0], clone[1][0]);
            assertEquals(input[0][1], clone[0][1]);
            assertEquals(input[1][1], clone[1][1]);
        }
    });
