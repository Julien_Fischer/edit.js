/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('utils.Enums')

    .run({
        lookup_whenExists_shouldReturnConstant: () => {
            // Given
            const expected = Alignment.LEFT;
            // When
            const actual = Enums.lookup(Alignment, 'LeFt');
            // Then
            assertEquals(expected, actual);
        },

        lookup_whenNotExists_shouldReturnNull: () => {
            // Given
            const key = 'WRONG_KEY';
            // When
            const actual = Enums.lookup(Alignment, key);
            // Then
            assertNull(actual);
        }
    });
