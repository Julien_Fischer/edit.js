/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('controls.Action')

    .run({
        support_whenEventSupported_shouldReturnTrue: () => {
            // Given
            const action = new Action('Action 1', Context.GLOBAL, new KeyStroke(Key.UP, Modifier.SHIFT), () => {});
            // Then
            assertTrue(action.supports({key: Key.UP, shiftKey: true}));
        },
        support_whenEventNotSupported_shouldReturnFalse: () => {
            // Given
            const action = new Action('Action 1', Context.GLOBAL, new KeyStroke(Key.UP, Modifier.SHIFT), () => {});
            // Then
            assertFalse(action.supports({key: Key.UP, altKey: true, ctrlKey: true}));
        }
    });
