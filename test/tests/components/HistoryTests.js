/*
 * Copyright (c) 2021. Julien Fischer
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

Unit.test('components.History')

    .beforeAll((context) => {
         context.mockCommand = () => {
            return {
                execute: () => { console.log('Mock execute()'); },
                undo: () => { console.log('Mock undo()'); },
            }
        }
    })

    .run({
        getCapacity_shouldReturnHistoryCapacity: () => {
            // Given
            const capacity = 99;
            const history = new CommandHistory(capacity);
            // Then
            assertEquals(capacity, history.getCapacity());
        },

        save_shouldStoreSpecifiedCommand: () => {
            // Given
            const history = new CommandHistory(2);
            // When
            history.save(new ResetCommand());
            // Then
            assertEquals(1, history.getSize());
        },

        undo_whenCanUndo_shouldUndoCommand: () => {
            // Given
            let undoCount = 0;
            const history = new CommandHistory(2);
            history.save({
                execute: () => { },
                undo: () => undoCount++
            });
            // When
            history.undo();
            // Then
            assertEquals(1, undoCount);
        },

        getSize_shouldReturnCurrentSize: (ctx) => {
            // Given
            const capacity = 2;
            const history = new CommandHistory(capacity);
            // When
            history.save(ctx.mockCommand());
            // Then
            assertEquals(1, history.getSize());
            assertEquals(capacity, history.getCapacity());
        },

        getSize_whenMoreElementsAddedThanCapacity_shouldReturnCapacity: (ctx) => {
            // Given
            const capacity = 2;
            const history = new CommandHistory(capacity);
            // When
            history.save(ctx.mockCommand());
            history.save(ctx.mockCommand());
            history.save(ctx.mockCommand());
            // Then
            assertEquals(capacity, history.getSize());
        },

        isEmpty_whenHistoryWasCleared_shouldReturnTrue: (ctx) => {
            // Given
            const capacity = 2;
            const history = new CommandHistory(capacity);
            history.save(ctx.mockCommand());
            // When
            history.clear();
            // Then
            assertTrue(history.isEmpty());
        },

        isEmpty_whenHistoryEmpty_shouldReturnTrue: () => {
            // Given
            const history = new CommandHistory(20);
            // Then
            assertTrue(history.isEmpty());
        },

        isEmpty_whenHistoryNotEmpty_shouldReturnFalse: (ctx) => {
            // Given
            const history = new CommandHistory(20);
            history.save(ctx.mockCommand());
            // Then
            assertFalse(history.isEmpty());
        },

        isFull_whenHistoryFull_shouldReturnTrue: (ctx) => {
            // Given
            const history = new CommandHistory(2);
            history.save(ctx.mockCommand());
            history.save(ctx.mockCommand());
            // Then
            assertTrue(history.isFull());
        },

        undo_whenNothingToUndo_shouldDoNothing: () => {
            // Given
            const command = {
                execute: () => { },
                undo: () => undoCount++
            };
            let undoCount = 0;
            let redoCount = 0;
            const history = new CommandHistory(2);
            history.save(command);
            history.save(command);
            // When
            history.undo();
            history.redo();
            history.undo();
            history.undo();
            history.undo();
            history.undo();
            // Then
            assertEquals(3, undoCount);
        },

        redo_whenNothingToRedo_shouldDoNothing: () => {
            // Given
            const command = {
                execute: () => redoCount++,
                undo: () => { }
            };
            let redoCount = 0;
            const history = new CommandHistory(2);
            history.save(command);
            history.save(command);
            // When
            history.undo();
            history.redo();
            history.undo();
            history.undo();
            history.redo();
            history.redo();
            history.redo();
            history.redo();
            // Then
            assertEquals(3, redoCount);
        }
    });
