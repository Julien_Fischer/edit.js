# Change Log


## 2.3.1

#### Features

- This patch provides general improvements to the Encoder, Decoder, and Highlighter APIs

#### Fixed bugs

- Fix C-type languages syntax highlighting for array elements containing an escaped quote


## 2.3.0

#### Features

- Add keyboard shortcuts for import dialog
- Add support for syntax highlighting (epic &1):
  - MediaWiki tables (issue #59)
  - AsciiDoc tables (issue #58)
  - LaTeX tables (issue #56)
  - Confluence tables (issue #55)
  - Cucumber data tables (issue #54)
  - GitHub/GitLab markdown tables (issue #53)
  - CSV (issue #49)
  - SQL create/insert scripts (issue #48)
  - BBCode tables (issue #46)
  - Plain Text tables (issue #45)
  - Java (issue #44)
  - PHP (issue #43)
  - C# (issue #42)
  - Python (issue #41)
  - JavaScript (issue #40)
  - JSON (issue #39)

#### Fixed bugs

- Fix OutputPolicy detection for MediaWiki tables (issue #60)


## 2.2.0

#### Features

- Add an SVG logo and favicons (issue #37)
- Add support for Python format (issue #34)
- Add support for JavaScript format (issue #33)
- Add support for PHP format (issue #32)


## 2.1.0

#### Features

- Add a dark mode (issue #26): dark mode can be toggled on button click and is enabled by default 
  depending on the OS preferred theme.
- Add support for Java format (issue #25)
- Add support for C# format (issue #28)
- Accordion now automatically expands when filtering unit tests by name (Dashboard)
- Package names are now displayed for each set of unit tests to speed up navigation (Dashboard)
- Improve target format combobox scalability and accessibility (issue #29)
- Add icons from FontAwesome to the existing icon set (from IcoMoon)
- Add support for AsciiDoc format (issue #30)

#### Fixed bugs

- CodeViewer location should be reset to its default docking side (issue #31)
- Allow multiple confirmation messages to be displayed at the same time (issue #27)


## 2.0.0

#### Features

- Duplicate rows and columns via the editor (issue #16)
- Implement keyboard shortcuts for editor operations (issue #14)
- Render HTML characters inside cells for every table format (issue #13)
- Add support for borderless tables (issue #12)
- Add support for centered and uppercase headers (issue #19)
- Add support for BBCode table format (issue #20)
- Add support for LaTeX table format (issue #21)
- Editor and Renderer can be interactively resized on mouse drag (issue #23)
- Layout orientation can be switched between horizontal and vertical on button click (issue #23)
- Display row and column names in the editor
- Help dialog accessible at all time with mouse click and keyboard shortcut (Ctrl + Q)
- Simple logging API for development and bug tracking
- v.2 of the internal testing framework:
  - `beforeAll`, `beforeEach` and `afterAll` methods are now supported
  - Each unit test, fixture and clean-up methods have now access to the testing context for read and write operations
  - Improved code viewer syntax highlighting
  - Filter test results by status or by name in Dashboard (issue #17)
  - The code viewer is now dockable on different sides of the screen (issue #18)
  - Unit tests are now automatically registered at declaration using `Unit.test` method
  - More metrics displayed in the Dashboard for easiers debugging and analysis

#### Fixed bugs

- Fixed `FIRST_COLUMN_AS_HEADER` option when `HEADER_ONLY` option is selected.
  This bug was impacting ASCII and Unicode formats (issue #15)


## 1.2.0

#### Features

- Generate and import MediaWiki tables (issue #11)
- Support custom separator for importing and generating CSV tables (issue #9)
- Provide a visual feedback when copy button is clicked (issue #8)
- Replace existing toolbars by a unique, user-friendly toolbar (issue #7)
- Add controls to create an empty table with specified number of rows and columns (issue #6)
- Transpose rows and columns in the editor table (issue #5)
- Add support for generating arrays of objects in JSON format (issue #4)
- Add support for SQL tables (issue #3)


## 1.1.0

#### Features

- Copy paste an existing table to prefill the editor (issue #1)


## 1.0.0

- Initial release

