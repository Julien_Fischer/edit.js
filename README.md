# edit.js

edit.js is an open source editor and table converter. 
It offers a rich set of features that makes it easy to create, import, edit, and convert any type of table in a matter of seconds.

edit.js is typically used for:

- Writing documentation for wikis and programming comments without the hassle
- Static code generation for software development and unit testing
- Data conversion from anything to anything; i.e. data to code, code to markdown, markdown to plain text,
etc... as long as it is a 2D representation of data, edit.js either supports it or will support it in the future

edit.js is free for both commercial and non-commercial use.

The codebase is dependency-free and can be used either standalone or embedded into a website.

![Editor](images/editor_dark.png)


## Features

- **Import existing tables**
  
  Import any type of table with just one click. Modify, convert, or export your work to any other format.

- **Start from scratch**
  
  Create and fill a new table in no-time using the editor and the toolbar options.

- **Convert data, code, markup, plain text...**
  
  Convert your table from any format to any other format. Render options can be tweaked so that
the output always accommodates your needs. Example: CSV <=> JSON, Java <=> LaTeX, etc...

- **Flexible editor**
  
  Insert, remove, duplicate, move, swap rows or columns, and much more.

- **Live reload**
  
  Everything you do is rendered in real time. No wasting time clicking buttons and waiting for the views to reload.
  
- **History**
  
  Every operation can be undone and redone as needed.

- **Syntax highlighting**

  Generated tables are nicely highlighted in the results panel

- **Column alignments**
  
  Each column can be left, center, or right aligned.

- **Contextual options**
  
  Each table format supports a specific set of options which can be customized as needed.
  Minify or prettify output, customize borders, define headers and table orientation, prepend with comments, and much more.

## Supported formats

edit.js currently supports the following formats:

- PLAIN
  - Plain Text
  - Plain Unicode
- MARKDOWN
  - Confluence
  - GitHub, GitLab, StackOverflow...
  - Cucumber
- MARKUP
  - LaTeX
  - AsciiDoc
- DATA
  - JSON
  - CSV
- WEB
  - HTML
  - BBCode
  - MediaWiki
- CODE
  - SQL
  - Java
  - C#
  - JavaScript
  - Python
  - PHP

## Shortcuts

Please refer to the list of [Supported shortcuts](https://gitlab.com/edit.js/edit.js/-/blob/master/docs/shortcuts.md) for more details.

## Security & Privacy

Everything runs locally in your web-browser. No data is sent, processed, or collected by anyone.

## Requirements

Any modern browser supporting ECMAScript 6.

No internet connection required.

## Getting started

To use edit.js: 
1. Download the source code or clone this repository
2. Open the following file in your web-browser: `edit.js\src\index.html`

## License

This software is licensed under Apache version 2.0.

## Contributing

edit.js is an open-source project; community contributions are welcome.

To report a bug or request a new feature, feel free to open an issue or submit a pull request on GitLab.

Please refer to [Contributing to edit.js](https://gitlab.com/edit.js/edit.js/-/blob/master/CONTRIBUTING.md) for more details.


## Current version

2.3.1

## Credits

edit.js logo and favicons were designed using Inkscape.

The UI uses icons from IcoMoon and FontAwesome under CC BY 4.0 license.
