# Shortcuts

This document specifies the list of available shortcuts:

| Operation             | Shortcut             | Context | Version |
| :-------------------- | :------------------- | :------ | :------ |
| Undo                  | CTRL + Z             | Global  | 1.2.0   |
| Redo                  | CTRL + Y             | Global  | 1.2.0   |
| Close dialog          | ESCAPE               | Global  | 1.2.0   |
| Toggle Help           | CTRL + Q             | Global  | 2.0.0   |
| Toggle Import Dialog  | CTRL + I             | Global  | 2.3.0   |
| Toggle Editor Actions | CTRL + E             | Global  | 2.0.0   |
| Insert row            | ENTER                | Editor  | 2.0.0   |
| Insert column         | ALT + ENTER          | Editor  | 2.0.0   |
| Delete row            | CTRL + D             | Editor  | 2.0.0   |
| Delete column         | ALT + D              | Editor  | 2.0.0   |
| Duplicate row         | CTRL + SHIFT + DOWN  | Editor  | 2.0.0   |
| Duplicate column      | CTRL + SHIFT + RIGHT | Editor  | 2.0.0   |
| Move row up           | ALT + SHIFT + UP     | Editor  | 2.0.0   |
| Move row down         | ALT + SHIFT + DOWN   | Editor  | 2.0.0   |
| Move column left      | ALT + SHIFT + LEFT   | Editor  | 2.0.0   |
| Move column right     | ALT + SHIFT + RIGHT  | Editor  | 2.0.0   |
| Next cell up          | ALT + UP             | Editor  | 2.0.0   |
| Next cell left        | ALT + LEFT           | Editor  | 2.0.0   |
| Next cell down        | ALT + DOWN           | Editor  | 2.0.0   |
| Next cell right       | ALT + RIGHT          | Editor  | 2.0.0   |
